/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.tools.activation

import org.osgi.framework.BundleActivator
import org.osgi.framework.BundleContext

object Activator {
  var context : BundleContext = _

  def getContext : BundleContext = {
    context
  }

  //val Logger = new Logger(ID)

}

class Activator extends BundleActivator {

  def contex = Activator.context

  def start(bundleContext : BundleContext) : Unit = {
    Activator.context = bundleContext;
  }

  def stop(bundleContext : BundleContext) : Unit = {
    Activator.context = null;
  }

}
