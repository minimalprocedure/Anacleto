/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.tools

import java.lang.reflect.Field
import java.util.Map
import java.util.HashMap
import com.sun.jna.Library
import com.sun.jna.Native

object Environment {

  trait WinLibC extends Library {
    def _putenv(name : String) : Int
  }
  trait LinuxLibC extends Library {
    def setenv(name : String, value : String, overwrite : Int) : Int
    def unsetenv(name : String) : Int
  }

  object Posix {
    val libc = if (System.getProperty("os.name").equals("Linux"))
      Native.loadLibrary("c", classOf[LinuxLibC])
    else
      Native.loadLibrary("msvcrt", classOf[WinLibC])

    def setenv(name : String, value : String, overwrite : Int) : Int = {
      if (libc.isInstanceOf[LinuxLibC])
        libc.asInstanceOf[LinuxLibC].setenv(name, value, overwrite)
      else
        libc.asInstanceOf[WinLibC]._putenv(name + "=" + value)
    }

    def unsetenv(name : String) : Int = {
      if (libc.isInstanceOf[LinuxLibC])
        libc.asInstanceOf[LinuxLibC].unsetenv(name)
      else
        libc.asInstanceOf[WinLibC]._putenv(name + "=")
    }
  }

  def unsetenv(name : String) : Int = {
    //val map = System.getenv
    getenv.remove(name)
    //val env2 = getwinenv
    getwinenv.remove(name)
    Posix.unsetenv(name)
  }

  def getwinenv : Map[String, String] = {
    try {
      val sc = Class.forName("java.lang.ProcessEnvironment")
      val caseinsensitive = sc.getDeclaredField("theCaseInsensitiveEnvironment")
      caseinsensitive.setAccessible(true)
      caseinsensitive.get(null).asInstanceOf[Map[String, String]]
    } catch {
      case e : Exception => new HashMap[String, String]()
    }
  }

  def getenv : Map[String, String] = {
    try {
      val env = System.getenv
      val cu = env.getClass
      val m = cu.getDeclaredField("m")
      m.setAccessible(true)
      m.get(env).asInstanceOf[Map[String, String]]
    } catch {
      case e : Exception => new HashMap[String, String]()
    }
  }

  def setenv(name : String, value : String, overwrite : Boolean) : Int = {
    if (name.lastIndexOf("=") != -1) {
      throw new IllegalArgumentException("Environment variable cannot contain '='")
      0
    } else {
      val map = getenv
      if (!map.containsKey(name) || overwrite) {
        map.put(name, value)
        //val env2 = getwinenv
        getwinenv.put(name, value)
      }
      Posix.setenv(name, value, if (overwrite) 1 else 0)
    }
  }

}
