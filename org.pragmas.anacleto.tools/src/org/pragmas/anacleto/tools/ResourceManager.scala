/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.tools

import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.ui.plugin.AbstractUIPlugin
//import org.osgi.framework.BundleContext
//import org.eclipse.core.runtime.Platform
import org.eclipse.swt.graphics.Image
import org.eclipse.ui.PlatformUI
//import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
//import org.eclipse.core.resources.IFile
import org.eclipse.core.filesystem.IFileStore
import org.eclipse.swt.graphics.Cursor
import org.eclipse.swt.widgets.Display
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.filesystem.IFileStore
import scalax.pragmas.Ordering.NaturalStringOrdering
import scala.collection.mutable.ListBuffer

object ResourceManager {
  def RESOURCES_BUNDLE_ID = "org.pragmas.anacleto.resources"

  def getImageDescriptor(path : String) : ImageDescriptor = {
    AbstractUIPlugin.imageDescriptorFromPlugin(RESOURCES_BUNDLE_ID, path)
  }

  def getIcon(icon : String) : Image = {
    if(icon.indexOf(".png") > -1 || icon.indexOf(".gif") > -1 ) {
      val imageDescriptor = getImageDescriptor("icons/" + icon)
      imageDescriptor.createImage
    } else {
      PlatformUI.getWorkbench.getSharedImages.getImage(icon)
    }
  }

  def getIconDescriptor(icon : String) : ImageDescriptor = {
    if(icon.indexOf(".png") > -1 || icon.indexOf(".gif") > -1 ) {
      getImageDescriptor("icons/" + icon)
    } else {
      PlatformUI.getWorkbench.getSharedImages.getImageDescriptor(icon)
    }
  }

  def getCursor(display : Display, cursor : String) : Cursor = {
    val image = getIcon(cursor)
    new Cursor(display, image.getImageData, 10, 0)
  }

  def getFileNames(fileStore : IFileStore, pattern : String = "", ordered : Boolean = true) : List[String] = {
    val listBuffer = ListBuffer[String]()
    val fileStoreList = fileStore.childStores(EFS.NONE, null)
    val buffer =
    for(fileStore <- fileStoreList
        if !fileStore.fetchInfo.getAttribute(EFS.ATTRIBUTE_HIDDEN);
        if fileStore.getName.take(1) != ".";
        if fileStore.getName.contains(pattern))
      yield fileStore.toString

    if (ordered)
      buffer.toList.sortBy(String => String)(NaturalStringOrdering)
    else
      buffer.toList
  }

  def getFilePaths(fileStore : IFileStore, pattern : String = "", ordered : Boolean = true) : List[IPath] = {
    val bufferNames = getFileNames(fileStore, pattern, ordered)
    var buffer = ListBuffer[IPath]()
    for (name <- bufferNames)
      buffer.append(new Path(name))
    buffer.toList
  }

  def getFileStores(fileStore : IFileStore, pattern : String = "", ordered : Boolean = true) : List[IFileStore] = {
    val bufferNames = getFileNames(fileStore, pattern, ordered)
    var buffer = ListBuffer[IFileStore]()
    for (name <- bufferNames)
      buffer.append(EFS.getLocalFileSystem.getStore(new Path(name)))
    buffer.toList
  }
}
