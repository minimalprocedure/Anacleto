/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.implicits

import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.KeyAdapter
import org.eclipse.swt.events.KeyEvent
import org.eclipse.swt.events.KeyListener

object Events {

  implicit def func2SelectionListener[T <: { def apply(e : SelectionEvent) }](func : T) = {
    new SelectionAdapter() {
      override def widgetSelected(e : SelectionEvent) = func.apply(e)
    }
  }

  implicit def func2KeyListener[T <: { def apply(e : KeyEvent) }](func1 : T) = {
    new KeyAdapter {
      override def keyPressed(e : KeyEvent) = func1.apply(e)
    }
  }

}
