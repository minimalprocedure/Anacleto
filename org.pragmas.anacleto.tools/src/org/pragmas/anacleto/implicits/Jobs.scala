/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.implicits

import org.eclipse.core.runtime.jobs._
import org.eclipse.ui.progress.UIJob
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.IStatus
import org.eclipse.core.runtime.Status
import org.eclipse.core.runtime.CoreException

object Jobs {

  def workerUI(name : String)(runInUIThreadBody : { def apply(monitor : IProgressMonitor) : IStatus }) : UIJob = {

    val uiJob = new UIJob("workerUI:" + name) {
      def runInUIThread(monitor : IProgressMonitor) : IStatus = {
        try {
          runInUIThreadBody.apply(monitor)
        } catch {
          case e : CoreException => {
            //Activator.Logger.exception("JOBUI WORKER", e)
            e.getStatus
          }
        }
      }
    }

    uiJob.schedule
    uiJob
  }

  def worker(name : String, monitorString : String)(jobBody : { def apply(monitor : IProgressMonitor) : IStatus }) : Job = {
    val job = new Job("worker:" + name) {
      def run(monitor : IProgressMonitor) : IStatus = {
        monitor.beginTask(monitorString, 100)
        try {
          jobBody.apply(monitor)
        } catch {
          case e : CoreException => {
            //Activator.Logger.exception("JOB WORKER", e)
            e.getStatus
          }
        }
        finally monitor.done
      }
    }
    job.setUser(true)
    job.schedule
    job
  }

  def workerWithDone(name : String, monitorString : String)(jobBody : { def apply(monitor : IProgressMonitor) : IStatus })(implicit doneBody : { def apply(event : IJobChangeEvent) : Any }) : Job = {
    val job = new Job("workerWithDone:" + name) {
      def run(monitor : IProgressMonitor) : IStatus = {
        monitor.beginTask(monitorString, 100)
        try {
          jobBody.apply(monitor)
        } catch {
          case e : CoreException => {
            //Activator.Logger.exception("JOB WORKER", e)
            e.getStatus
          }
        }
        finally monitor.done
      }
    }
    job.addJobChangeListener(new JobChangeAdapter {
      override def done(event : IJobChangeEvent) : Unit = {
        if (event.getResult.isOK) {
          job.done(Status.OK_STATUS)
          if (Reflections.methodExistByOneParameter(doneBody, "apply", classOf[IJobChangeEvent]))
            doneBody.apply(event)
        }
      }
    })
    job.setUser(true)
    job.schedule
    job
  }

}
