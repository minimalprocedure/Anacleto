/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.implicits

class StringExtensions(s : String) {

  def humanize : String = {
    val regex = "(\\w|\\d)([A-Z])".r
    regex.replaceAllIn(s, (replacer => replacer.matched.charAt(0) + " " + replacer.matched.charAt(1).toLower))
  }

  def toIntSafe : Int = {
    if(s.stripMargin.isEmpty) 0 else s.toInt
  }

}
