/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.implicits

object Reflections {
  def methodExistByOneParameter(klass : AnyRef, name : String, klassParameter : AnyRef) : Boolean = {
    val klassClass = klass.getClass
    val methods = klassClass.getMethods
    var found : Boolean = false
    for(m <- methods;
        if klassClass.toString + "." + name == klassClass.toString + "." + m.getName;
        if !found) {
      found = m.getParameterTypes.contains(klassParameter)
    }
    found
  }

  def methodExist(klass : AnyRef, name : String) : Boolean = {
    val klassClass = klass.getClass
    val methods = klassClass.getMethods
    methods.contains(name)
  }
}
