/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.logger

import org.eclipse.core.runtime.IStatus
import org.eclipse.core.runtime.Status
import org.eclipse.core.runtime.Platform

class Logger(ID : String) {

  var logEnabled = if (System.getProperty("anacleto.log.enabled") == "false") false else true

  private val bundle = Platform.getBundle(ID)
  private val pluginID : String = bundle.getSymbolicName
  private val logInst = Platform.getLog(bundle)

  def error(message : Any, e : Throwable) : Unit = {
    if (bundle != null && logEnabled)
      logInst.log(new Status(IStatus.ERROR, pluginID, IStatus.OK, "ERROR: " + message.toString, e))
  }

  def exception(message : Any, e : Throwable) : Unit = {
    if (bundle != null && logEnabled)
      logInst.log(new Status(IStatus.ERROR, pluginID, IStatus.OK, "EXCEPTION: " + message.toString, e))
  }

  def info(message : Any) : Unit = {
    if (bundle != null && logEnabled)
      logInst.log(new Status(IStatus.INFO, pluginID, "INFO: " + message.toString))
  }

  def debug(message : Any) : Unit = {
    if (bundle != null && logEnabled)
      logInst.log(new Status(IStatus.INFO, pluginID, "DEBUG: " + message.toString))
  }

  def warning(message : Any) : Unit = {
    if (bundle != null && logEnabled)
      logInst.log(new Status(IStatus.WARNING, pluginID, "WARNING: " + message.toString))
  }

}
