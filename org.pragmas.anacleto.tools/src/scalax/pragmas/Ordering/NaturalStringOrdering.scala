/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
/*
 * The Alphanum Algorithm is an improved sorting algorithm for strings
 * containing numbers.  Instead of sorting numbers in ASCII order like
 * a standard sort, this algorithm sorts numbers in numeric order.
 *
 * The Alphanum Algorithm is discussed at http://www.DaveKoelle.com
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/*
 * SCALA VERSION: Massimo Maria Ghisalberti (minimalprocedure@gmail.com)
 */

package scalax.pragmas.Ordering

object NaturalStringOrdering extends Ordering[String] {

  private def isDigit(ch: Char) = ch >= 48 && ch <= 57

  private def getChunk(s: String, slength: Int, mrk: Int): String = {
    var marker = mrk

    val chunk = new StringBuilder
    var c = s.charAt(marker)
    chunk.append(c)
    marker += 1

    def findMarker1: Unit = {
      if (marker < slength) {
        c = s.charAt(marker)
        if (isDigit(c)) {
          chunk.append(c)
          marker += 1
          findMarker1
        }
      }
    }

    def findMarker2: Unit = {
      if (marker < slength) {
        c = s.charAt(marker)
        if (!isDigit(c)) {
          chunk.append(c)
          marker += 1
          findMarker2
        }
      }
    }

    if (isDigit(c))
      findMarker1
    else
      findMarker2
    chunk.toString
  }

  def compareByVal(s1: String, s2: String): Boolean = {
    val result = compare(s1: String, s2: String)
    if (result < 0) true
    else false
  }

  def compare(s1: String, s2: String): Int = {
    var result = 0
    var thisMarker = 0
    var thatMarker = 0
    val s1Length = s1.length
    val s2Length = s2.length

    while (thisMarker < s1Length && thatMarker < s2Length) {
      val thisChunk = getChunk(s1, s1Length, thisMarker)
      thisMarker += thisChunk.length

      val thatChunk = getChunk(s2, s2Length, thatMarker)
      thatMarker += thatChunk.length

      // If both chunks contain numeric characters, sort them numerically
      if (isDigit(thisChunk.charAt(0)) && isDigit(thatChunk.charAt(0))) {
        // Simple chunk comparison by length.
        val thisChunkLength = thisChunk.length
        result = thisChunkLength - thatChunk.length

        // If equal, the first different number counts
        if (result == 0) {
          var continue = true
          for (
            index <- 0 to thisChunkLength - 1 if continue
          ) {
            result = thisChunk.charAt(index) - thatChunk.charAt(index)
            continue = if (result != 0) false else true
          }

        }
      } else {
        result = thisChunk.compareTo(thatChunk)
      }
      if (result != 0) return result
    }
    s1Length - s2Length
  }

}

object NaturalStringOrderingTest {
  def main(args: Array[String]): Unit = {

    val list = List[String](
      "1000X Radonius Maximus",
      "10X Radonius",
      "200X Radonius",
      "20X Radonius",
      "20X Radonius Prime",
      "30X Radonius",
      "40X Radonius",
      "Allegia 50 Clasteron",
      "Allegia 500 Clasteron",
      "Allegia 51 Clasteron",
      "Allegia 51B Clasteron",
      "Allegia 52 Clasteron",
      "Allegia 60 Clasteron",
      "Alpha 100",
      "Alpha 2",
      "Alpha 200",
      "Alpha 2A",
      "Alpha 2A-8000",
      "Alpha 2A-900",
      "Callisto Morphamax",
      "Callisto Morphamax 500",
      "Callisto Morphamax 5000",
      "Callisto Morphamax 600",
      "Callisto Morphamax 700",
      "Callisto Morphamax 7000",
      "Callisto Morphamax 7000 SE",
      "Callisto Morphamax 7000 SE2",
      "QRS-60 Intrinsia Machine",
      "QRS-60F Intrinsia Machine",
      "QRS-62 Intrinsia Machine",
      "QRS-62F Intrinsia Machine",
      "Xiph Xlater 10000",
      "Xiph Xlater 2000",
      "Xiph Xlater 300",
      "Xiph Xlater 40",
      "Xiph Xlater 5",
      "Xiph Xlater 50",
      "Xiph Xlater 500",
      "Xiph Xlater 5000",
      "Xiph Xlater 58")

    val ordered2 = list.sortBy(String => String)(NaturalStringOrdering)
    ordered2.foreach(println)

  }
}
