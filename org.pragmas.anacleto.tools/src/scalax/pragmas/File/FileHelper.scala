/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package scalax.pragmas.File

import java.io._

class FileHelper(file: File) {

  def writeln(text: String, fw : FileWriter = null): FileWriter = {
    var fileWriter: FileWriter = null
    if(fw != null)
      fileWriter = fw
    else
      fileWriter = new FileWriter(file)
    try { fileWriter.write(text + "\n") } catch { case e: Exception => fileWriter.close }
    fileWriter
  }

  def write(text: String): Unit = {
    val fw = new FileWriter(file)
    try { fw.write(text) } finally { fw.close }
  }

  def foreachLine(proc: String => Unit): Unit = {
    val br = new BufferedReader(new FileReader(file))
    try { while (br.ready) proc(br.readLine) } finally { br.close }
  }

  def deleteAll: Unit = {
    def deleteFile(dfile: File): Unit = {
      if (dfile.isDirectory) {
        val subfiles = dfile.listFiles
        if (subfiles != null)
          subfiles.foreach{ f => deleteFile(f) }
      }
      dfile.delete
    }
    deleteFile(file)
  }
}

object FileHelper {
  implicit def file2helper(file: File) = new FileHelper(file)
}

/*
//Now you can test it.
import FileHelper._
val dir = new File("/tmp/mydir/nested_dir")
dir.mkdirs
val file = new File(dir, "myfile.txt")
file.write("one\ntwo\nthree")
file.foreachLine{ line => println(">> " + line) }
dir.deleteAll
*/
