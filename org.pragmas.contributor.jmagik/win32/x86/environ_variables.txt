MAGICK_HOME=<imagemagick>
MAGICK_CONFIGURE_PATH=<imagemagick>\config
MAGICK_CODER_MODULE_PATH=<imagemagick>\modules\coders
MAGICK_CODER_FILTER_PATH=<imagemagick>\modules\filters
MAGICK_FONT_PATH=<fontpath>
LD_LIBRARY_PATH=<imagemagick>
