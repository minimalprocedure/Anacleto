/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.search.ui.views

import scala.collection.mutable.HashMap

import org.eclipse.ui.part.ViewPart
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.widgets.ToolBar
import org.eclipse.swt.widgets.ToolItem
import org.pragmas.anacleto.tools.ResourceManager
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.filesystem.IFileStore
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.IPath
import org.eclipse.ui.PlatformUI
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.ui.ide.FileStoreEditorInput
import org.eclipse.swt.events.{
  ControlEvent,
  ControlAdapter,
  SelectionAdapter,
  SelectionEvent,
  SelectionListener
}
import org.pragmas.lucene.indexer._
import org.pragmas.anacleto.implicits.Events._
import org.eclipse.jface.viewers.TreeViewer
//import org.eclipse.swt.widgets.TreeColumn
import org.pragmas.anacleto.search.ui.providers._
import org.apache.lucene.store.Directory
import java.io.File

import org.eclipse.jface.viewers.IOpenListener
import org.eclipse.jface.viewers.OpenEvent
import org.eclipse.jface.viewers.ISelectionChangedListener
import org.eclipse.jface.viewers.SelectionChangedEvent
import org.eclipse.jface.viewers.IStructuredSelection

import org.pragmas.anacleto.ui.views.ProjectManager
import org.pragmas.anacleto.ProjectResources.ProjectTypes
import org.pragmas.anacleto.ProjectResources.ProjectNodeTools
import org.pragmas.anacleto.ProjectResources
import org.pragmas.anacleto.models.APRJNames

import org.pragmas.anacleto.search.Activator
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.implicits.Jobs._
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Status
import org.eclipse.ui.IPartListener2
import org.eclipse.ui.IWorkbenchPartReference
import org.eclipse.ui.PlatformUI

import org.eclipse.ui.internal.EditorReference
import org.eclipse.ui.internal.ViewReference
import org.pragmas.anacleto.ui.editors.ScriptEditor

import org.eclipse.swt.events.KeyAdapter
import org.eclipse.swt.events.KeyEvent

object SearchView {
  val ID = "org.pragmas.anacleto.search.ui.views.SearchView"
  var indexStore : String = null
  var dataStore : String = null
  var projectStore : String = null

  var EXTENSIONS : List[String] = _

  var directoryIndex : Directory = _
  var MAX_RESULT = 250

}

class SearchView extends ViewPart with IPartListener2 {

  val LINENUM_SEP = ":     "

  val ID = SearchView.ID
  var pageContainer : Composite = _

  var toolBar : ToolBar = _
  var searchText : Text = _
  var searchTree : TreeViewer = null
  private val rootSearch = new TreeParent("__root__")
  private var currentNode : TreeObject = _
  private var searchResult : Map[String, List[Map[String, Any]]] = null

  private var isEditorOpenBySearch = false

  private def createIndex : Unit = {
    worker("SearchView.createIndex", "SearchView.createIndex") { monitor : IProgressMonitor =>
      if (resolveProjectPaths) {
        PermittedExtensions.EXTENSIONS = SearchView.EXTENSIONS
        val indexer = new TextFileIndexer(SearchView.indexStore,
          SearchView.dataStore,
          SearchView.projectStore)
      }
      Status.OK_STATUS
    }
  }

  private def findWord : Unit = {
    val word = searchText.getText
    resolveProjectPaths
    PermittedExtensions.EXTENSIONS = SearchView.EXTENSIONS
    if (new File(SearchView.indexStore).exists) {
      val searcher = new TextFileSearcher(SearchView.indexStore)
      searchResult = searcher.search(word)
    } else {
      createIndex
      val searcher = new TextFileSearcher(SearchView.indexStore)
      searchResult = searcher.search(word, SearchView.MAX_RESULT)
    }
  }

  private def createTreeObject(result : Map[String, Any]) : TreeObject = {
    val startEnd =
      if (result(TextFileIndexerFiled.DOC_START_END_TUPLES).asInstanceOf[List[(Int, Int)]].size == 0) "" else
        ":(" + result(TextFileIndexerFiled.DOC_START_END_TUPLES).asInstanceOf[List[(Int, Int)]](0)._1 +
          ',' +
          result(TextFileIndexerFiled.DOC_START_END_TUPLES).asInstanceOf[List[(Int, Int)]](0)._2 +
          ")"

    new TreeObject(
      result(TextFileIndexerFiled.LINE_NUM).toString + startEnd + LINENUM_SEP +
        result(TextFileIndexerFiled.LINE).asInstanceOf[String]
    )
  }

  def prepareSearchResultToTree : Unit = {
    workerUI("SearchView.prepareSearchResultToTree") { monitor : IProgressMonitor =>

      findWord

      rootSearch.removeChildren
      if (searchResult != null) {
        val filesMap = new HashMap[String, org.pragmas.anacleto.ProjectResources.TreeObject]
        var parent : TreeParent = null
        searchResult foreach {
          case (key, value) =>
            val projectNode = ProjectNodeTools.getChildByPropertyValue(APRJNames.name,
              new File(key).getName, ProjectManager.projectRoot)
            if (projectNode != null) {
              parent = new TreeParent("hits: " + value.size.toString + LINENUM_SEP + "doc: " + LINENUM_SEP + projectNode.property(APRJNames.title).toString)
              parent.nodeType = projectNode.nodeType
              parent.path = new File(SearchView.projectStore, key).toString
              value foreach { result =>
                val line = createTreeObject(result)
                line.lineNum = result(TextFileIndexerFiled.LINE_NUM).asInstanceOf[Int]
                line.dataNode = projectNode
                parent.addChild(line)
              }
              rootSearch.addChild(parent)
            }
        }
      }
      searchTree.setInput(rootSearch)
      searchResult = null
      Status.OK_STATUS
    }
  }

  private def resolveProjectPaths : Boolean = {
    if (ProjectManager.ProjectPathParentStore != null) {
      SearchView.indexStore = ProjectManager.ProjectPathParentStore.append(ProjectTypes.APPINDEX_CONTAINER).toOSString
      SearchView.dataStore = ProjectTypes.APPDATA_CONTAINER
      SearchView.projectStore = ProjectManager.ProjectPathParentStore.toOSString
      SearchView.EXTENSIONS =
        ((ProjectTypes.RESOURCES_TEXT_CONCAT + "," +
          ProjectTypes.RESOURCES_FILTERS_CONCAT + "," +
          ProjectTypes.RESOURCES_TEXT_CONCAT).split(',') map { el => "." + el }).toList
      true
    } else false
  }

  private def initToolBar(parent : Composite) : Unit = {

    val gridData = new GridData
    gridData.horizontalAlignment = GridData.FILL
    gridData.grabExcessHorizontalSpace = true

    toolBar = new ToolBar(parent, SWT.FLAT)
    toolBar.setLayoutData(gridData)
    val toolSearch = new ToolItem(toolBar, SWT.SEPARATOR)
    searchText = new Text(toolBar, SWT.BORDER)
    toolSearch.setControl(searchText)
    toolSearch.setWidth(420)

    val find = new ToolItem(toolBar, SWT.FLAT)
    find.setToolTipText("Search")
    //find.setImage(ResourceManager.getIcon("silk/arrow_right.png"))
    find.setText("search")
    find.addSelectionListener({ e : SelectionEvent =>
      if (Perspective.projectManager.isLoaded) {
        prepareSearchResultToTree
      }
    })
    searchText.addKeyListener({ e : KeyEvent =>
      if ((e.keyCode == SWT.Selection))
        prepareSearchResultToTree
    })

    val reload = new ToolItem(toolBar, SWT.FLAT)
    reload.setToolTipText("Reindex")
    reload.setImage(ResourceManager.getIcon("silk/arrow_refresh.png"))
    reload.addSelectionListener({ e : SelectionEvent =>
      if (Perspective.projectManager.isLoaded)
        createIndex
    })
  }

  private def initSearchResultView(parent : Composite) : Unit = {
    val gridData = new GridData
    gridData.horizontalAlignment = GridData.FILL
    gridData.verticalAlignment = GridData.FILL
    gridData.grabExcessHorizontalSpace = true
    gridData.grabExcessVerticalSpace = true
    searchTree = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER)
    searchTree.setAutoExpandLevel(2)
    searchTree.getControl.setLayoutData(gridData)
    searchTree.setContentProvider(new ViewContentProvider)
    searchTree.setLabelProvider(new ViewLabelProvider)
    searchTree.setInput(rootSearch)
  }

  private def prepareEventListeners : Unit = {

    searchTree.addOpenListener(new IOpenListener {
      override def open(event : OpenEvent) : Unit = {
        if (currentNode.isInstanceOf[TreeObject]) {
          isEditorOpenBySearch = true
          Perspective.projectManager.openResource(currentNode.dataNode, currentNode.lineNum)
        }
      }
    })

    /* Selection on Item node */
    searchTree.addSelectionChangedListener(new ISelectionChangedListener {
      override def selectionChanged(event : SelectionChangedEvent) : Unit = {
        if (event.getSelection.isEmpty) {
          Activator.Logger.warning("empty node")
          return
        }
        if (event.getSelection.isInstanceOf[IStructuredSelection]) {
          val selection = event.getSelection.asInstanceOf[IStructuredSelection]
          val selectionIterator = selection.iterator
          while (selectionIterator.hasNext) {
            currentNode = selectionIterator.next.asInstanceOf[TreeObject]
          }
        }
      }
    })
  }

  override def createPartControl(parent : Composite) : Unit = {
    pageContainer = parent
    val layout = new GridLayout(1, true)
    pageContainer.setLayout(layout)
    initToolBar(pageContainer)
    initSearchResultView(pageContainer)
    prepareEventListeners
    createIndex
  }

  override def setFocus : Unit = {
    //viewer.getControl.setFocus
  }

  override def dispose : Unit = {}

  def partInputChanged(partRef : IWorkbenchPartReference) : Unit = {
    //println("partInputChanged " + partRef)
  }

  def partOpened(partRef : IWorkbenchPartReference) : Unit = {
    if (!isEditorOpenBySearch)
      if (partRef.isInstanceOf[EditorReference]) {
        val editor = partRef.asInstanceOf[EditorReference].getEditor(false)
        if (editor.isInstanceOf[ScriptEditor]) createIndex
      } else isEditorOpenBySearch = false
    //println("partOpened " + partRef)
  }

  def partVisible(partRef : IWorkbenchPartReference) : Unit = {
    //println("partVisible " + partRef)
  }
  def partBroughtToTop(partRef : IWorkbenchPartReference) : Unit = {
    //println("partBroughtToTop " + partRef)
  }
  def partDeactivated(partRef : IWorkbenchPartReference) : Unit = {
    //println("partDeactivated " + partRef)
  }
  def partActivated(partRef : IWorkbenchPartReference) : Unit = {
    //println("partActivated " + partRef)
  }
  def partHidden(partRef : IWorkbenchPartReference) : Unit = {
    //println("partHidden " + partRef)
  }
  def partClosed(partRef : IWorkbenchPartReference) : Unit = {
    //if (!isEditorOpenBySearch)
    isEditorOpenBySearch = false
    if (partRef.isInstanceOf[EditorReference]) {
      val editor = partRef.asInstanceOf[EditorReference].getEditor(false)
      if (editor.isInstanceOf[ScriptEditor]) createIndex
    } //else isEditorOpenBySearch = false
    //println("partClosed " + partRef)
  }

}
