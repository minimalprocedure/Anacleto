/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.search.ui.views

import org.eclipse.swt.widgets.TreeColumn
import scala.collection.mutable.ListBuffer

import org.eclipse.ui.part.ViewPart
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.widgets.ToolBar
import org.eclipse.swt.widgets.ToolItem
import org.pragmas.anacleto.tools.ResourceManager
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.filesystem.IFileStore
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.IPath
import org.eclipse.ui.PlatformUI
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.ui.ide.FileStoreEditorInput
import org.eclipse.swt.events.{
  ControlEvent,
  ControlAdapter,
  SelectionAdapter,
  SelectionEvent,
  SelectionListener
}
import org.pragmas.lucene.indexer._
import org.pragmas.anacleto.implicits.Events._
import org.eclipse.jface.viewers.TreeViewer
//import org.eclipse.swt.widgets.TreeColumn
import org.pragmas.anacleto.search.ui.providers._
import org.apache.lucene.store.Directory
import java.io.File

import org.eclipse.jface.viewers.IOpenListener
import org.eclipse.jface.viewers.OpenEvent
import org.eclipse.jface.viewers.ISelectionChangedListener
import org.eclipse.jface.viewers.SelectionChangedEvent
import org.eclipse.jface.viewers.IStructuredSelection

import org.pragmas.anacleto.ui.views.ProjectManager
import org.pragmas.anacleto.ProjectResources.ProjectTypes
import org.pragmas.anacleto.ProjectResources.ProjectNodeTools
import org.pragmas.anacleto.ProjectResources
import org.pragmas.anacleto.models.APRJNames

import org.pragmas.anacleto.search.Activator
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.implicits.Jobs._
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Status
import org.eclipse.ui.IPartListener2
import org.eclipse.ui.IWorkbenchPartReference
import org.eclipse.ui.PlatformUI

import org.eclipse.ui.internal.EditorReference
import org.eclipse.ui.internal.ViewReference
import org.pragmas.anacleto.ui.editors.ScriptEditor

import org.eclipse.swt.events.KeyAdapter
import org.eclipse.swt.events.KeyEvent

import org.pragmas.anacleto.ProjectResources.ProjectTypes

object MacrosManagerView {
  val ID = "org.pragmas.anacleto.search.ui.views.MacrosManagerView"
}

class MacrosManagerView extends ViewPart with IPartListener2 {
  val ID = MacrosManagerView.ID

  val LINENUM_SEP = ":     "

  var pageContainer : Composite = _

  var toolBar : ToolBar = _
  //var macrosText : Text = _
  private var editor : ScriptEditor = _
  private var macros : List[List[String]] = null

  def isEditorActive : Boolean = if (editor != null) true else false

  var macrosTree : TreeViewer = null

  private val rootMacros = new TreeParent("__root__")
  private val notes = new TreeParent("Notes")
  notes.nodeType = ProjectTypes.NOTE
  rootMacros.addChild(notes)
  private val tags = new TreeParent("Tags")
  tags.nodeType = ProjectTypes.TAG
  rootMacros.addChild(tags)
  private val comments = new TreeParent("Comments")
  comments.nodeType = ProjectTypes.COMMENT
  rootMacros.addChild(comments)
  private val scripts = new TreeParent("Scripts")
  scripts.nodeType = ProjectTypes.SCRIPT
  rootMacros.addChild(scripts)
  private val summaries = new TreeParent("Summaries")
  summaries.nodeType = ProjectTypes.SUMMARY
  rootMacros.addChild(summaries)

  private var currentNode : TreeObject = _

  private def prepareTreeObject(data : List[String]) : TreeObject = {
    val node = new TreeObject("macro: " + data(0) + " - type: " + data(1) + " - position: " + data(2) + " - insertion: " + data(3) + " - title: " + data(4))
    node.data = data
    node.path = data(0)
    node.nodeType = ProjectTypes.MACRO
    node
  }

  private def linkToEditor : Unit = {
    editor = if (!isEditorActive) {
      val ed = Perspective.getActivePage.getActiveEditor
      if (ed.isInstanceOf[ScriptEditor]) ed.asInstanceOf[ScriptEditor] else null
    } else editor
  }

  private def prepareMacrosResultToTree : Unit = {
    workerUI("MacrosManagerView.prepareMacrosResultToTree") { monitor : IProgressMonitor =>
      linkToEditor
      if (isEditorActive) {
        macros = editor.getMacros.toList
        macros foreach { macro =>
          macro(1) match {
            case ProjectTypes.SCRIPT  => scripts.addChild(prepareTreeObject(macro))
            case ProjectTypes.SUMMARY => summaries.addChild(prepareTreeObject(macro))
            case ProjectTypes.NOTE    => notes.addChild(prepareTreeObject(macro))
            case ProjectTypes.COMMENT => tags.addChild(prepareTreeObject(macro))
            case ProjectTypes.TAG     => comments.addChild(prepareTreeObject(macro))
            case _                    => null
          }
        }
        macrosTree.setInput(rootMacros)
      } else macros = null
      Status.OK_STATUS
    }
  }

  private def initToolBar(parent : Composite) : Unit = {

    val gridData = new GridData
    gridData.horizontalAlignment = GridData.FILL
    gridData.grabExcessHorizontalSpace = true

    toolBar = new ToolBar(parent, SWT.FLAT)
    toolBar.setLayoutData(gridData)

    val find = new ToolItem(toolBar, SWT.FLAT)
    find.setToolTipText("Macros")

    find.setText("macros")
//    find.addSelectionListener({ e : SelectionEvent =>
//      if (Perspective.projectManager.isLoaded) {
//        //println(editor.getCursorPosition)
//      }
//    })

    val reload = new ToolItem(toolBar, SWT.FLAT)
    reload.setToolTipText("Rescan")
    reload.setImage(ResourceManager.getIcon("silk/arrow_refresh.png"))
    reload.addSelectionListener({ e : SelectionEvent =>
      prepareMacrosResultToTree
    })
  }

  private def initMacrosResultView(parent : Composite) : Unit = {
    val gridData = new GridData
    gridData.horizontalAlignment = GridData.FILL
    gridData.verticalAlignment = GridData.FILL
    gridData.grabExcessHorizontalSpace = true
    gridData.grabExcessVerticalSpace = true
    macrosTree = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.VIRTUAL)
    macrosTree.setAutoExpandLevel(2)
    val tree = macrosTree.getTree
    tree.setHeaderVisible(true)
    tree.setLinesVisible(true)
    val columnMacro = new TreeColumn(tree, SWT.LEFT)
    columnMacro.setText("macro")
    columnMacro.setWidth(200)
    val columnType = new TreeColumn(tree, SWT.LEFT)
    columnType.setText("type")
    columnType.setWidth(60)
    val columnPosition = new TreeColumn(tree, SWT.LEFT)
    columnPosition.setText("position")
    columnPosition.setWidth(40)
    val columnInsertion = new TreeColumn(tree, SWT.LEFT)
    columnInsertion.setText("insertion")
    columnInsertion.setWidth(80)
    val columnTitle = new TreeColumn(tree, SWT.LEFT)
    columnTitle.setText("title")
    columnTitle.setWidth(200)

    macrosTree.getControl.setLayoutData(gridData)
    macrosTree.setContentProvider(new ViewContentProvider)
    macrosTree.setLabelProvider(new ViewLabelProvider)
    macrosTree.setInput(rootMacros)
  }

  private def activateSearchView : Unit = {
    val sv = Perspective.getActivePage.findView(Perspective.ID_SEARCH_VIEW)
    if (sv != null && sv.isInstanceOf[SearchView]) {
      val searchView = sv.asInstanceOf[SearchView]
      searchView.searchText.setText('"' + currentNode.path + '"')
      searchView.prepareSearchResultToTree
      Perspective.getActivePage.showView(Perspective.ID_SEARCH_VIEW)
    } else null
  }

  private def prepareEventListeners : Unit = {

    macrosTree.addOpenListener(new IOpenListener {
      override def open(event : OpenEvent) : Unit = {
        if (currentNode.isInstanceOf[TreeObject]) {
          activateSearchView
        }
      }
    })

    /* Selection on Item node */
    macrosTree.addSelectionChangedListener(new ISelectionChangedListener {
      override def selectionChanged(event : SelectionChangedEvent) : Unit = {
        if (event.getSelection.isEmpty) {
          Activator.Logger.warning("empty node")
          return
        }
        if (event.getSelection.isInstanceOf[IStructuredSelection]) {
          val selection = event.getSelection.asInstanceOf[IStructuredSelection]
          val selectionIterator = selection.iterator
          while (selectionIterator.hasNext) {
            currentNode = selectionIterator.next.asInstanceOf[TreeObject]
          }
        }
      }
    })
  }

  override def createPartControl(parent : Composite) : Unit = {
    pageContainer = parent
    val layout = new GridLayout(1, true)
    pageContainer.setLayout(layout)
    initToolBar(pageContainer)
    initMacrosResultView(pageContainer)
    prepareEventListeners
  }

  override def setFocus : Unit = {
    //viewer.getControl.setFocus
  }

  override def dispose : Unit = {}

  def partInputChanged(partRef : IWorkbenchPartReference) : Unit = {
    //println("partInputChanged " + partRef)
  }

  def partOpened(partRef : IWorkbenchPartReference) : Unit = {
    editor = if (partRef != null)
      if (partRef.isInstanceOf[EditorReference]) {
        val ed = partRef.asInstanceOf[EditorReference].getEditor(false)
        if (ed.isInstanceOf[ScriptEditor]) ed.asInstanceOf[ScriptEditor] else null
      } else null
    else null
    prepareMacrosResultToTree
  }

  def partVisible(partRef : IWorkbenchPartReference) : Unit = {
    //println("partVisible " + partRef)
  }
  def partBroughtToTop(partRef : IWorkbenchPartReference) : Unit = {
    //println("partBroughtToTop " + partRef)
  }
  def partDeactivated(partRef : IWorkbenchPartReference) : Unit = {
    //println("partDeactivated " + partRef)
  }
  def partActivated(partRef : IWorkbenchPartReference) : Unit = {
    //println("partActivated " + partRef)
  }
  def partHidden(partRef : IWorkbenchPartReference) : Unit = {
    //println("partHidden " + partRef)
  }
  def partClosed(partRef : IWorkbenchPartReference) : Unit = {
    editor = null
    //println("partClosed " + partRef)
  }

}
