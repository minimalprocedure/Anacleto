/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.search.ui.providers

import org.eclipse.jface.viewers.IStructuredContentProvider
import org.eclipse.jface.viewers.ITreeContentProvider
import org.eclipse.jface.viewers.Viewer

class ViewContentProvider extends IStructuredContentProvider with ITreeContentProvider {
  def inputChanged(v : Viewer, oldInput : Object, newInput : Object) : Unit = {}

  def dispose : Unit = {}

  def getElements(parent : Object) : Array[Object] = {
    getChildren(parent)
  }

  def getParent(child : Object) : Object = {
    if (child.isInstanceOf[TreeObject]) {
      child.asInstanceOf[TreeObject].parent
    } else { null }
  }

  def getChildren(parent : Object) : Array[Object] = {
    if (parent.isInstanceOf[TreeParent]) {
      parent.asInstanceOf[TreeParent].getChildren.asInstanceOf[Array[Object]]
    } else { new Array[Object](0) }
  }

  def hasChildren(parent : Object) : Boolean = {
    if (parent.isInstanceOf[TreeParent]) {
      parent.asInstanceOf[TreeParent].hasChildren
    } else { false }
  }
}
