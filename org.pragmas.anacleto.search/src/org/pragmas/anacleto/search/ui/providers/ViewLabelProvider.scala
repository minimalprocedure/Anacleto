/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.search.ui.providers

import org.eclipse.jface.viewers.ITableLabelProvider
import java.util.ArrayList
import org.eclipse.jface.viewers.LabelProvider
import org.eclipse.ui.ISharedImages
import org.eclipse.swt.graphics.Image
import org.pragmas.anacleto.tools.ResourceManager
import org.pragmas.anacleto.ProjectResources.ProjectTypes

class ViewLabelProvider extends LabelProvider with ITableLabelProvider {

  def getColumnImage(obj : Object, columnIndex : Int) : Image = {
    if (columnIndex < 1) getImage(obj) else null
  }

  def getColumnText(obj : Object, columnIndex : Int) : String = {
    val data = obj.asInstanceOf[TreeObject].data
    if (data != null)
      data.asInstanceOf[List[String]](columnIndex).toString
    else if (columnIndex < 1) obj.toString else ""

  }

  override def getText(obj : Object) : String = {
    obj.toString
  }

  override def getImage(obj : Object) : Image = {
    val nodeObj = obj.asInstanceOf[TreeObject]
    val imageKey =
    nodeObj.nodeType match {
      case ProjectTypes.PROJECT         => "silk/book.png"
      case ProjectTypes.CONTAINER       => "silk/container.png"
      case ProjectTypes.IMAGE           => "silk/image.png"
      case ProjectTypes.IMAGE_FILTER    => "silk/image_filter.png"
      case ProjectTypes.IMAGE_FILTER_GO => "silk/image_filter_go.png"
      case ProjectTypes.SCRIPT          => "silk/script.png"
      case ProjectTypes.LOCATION        => "silk/location.png"
      case ProjectTypes.PERSON          => "silk/person.png"
      case ProjectTypes.SUMMARY         => "silk/summary.png"
      case ProjectTypes.NOTE            => "silk/note.png"
      case ProjectTypes.COMMENT         => "silk/comment.png"
      case ProjectTypes.TAG             => "silk/tag.png"
      case ProjectTypes.MACRO           => "silk/macro.png"
      case _                            => "silk/search.png"
    }
    ResourceManager.getIcon(imageKey)
  }

}
