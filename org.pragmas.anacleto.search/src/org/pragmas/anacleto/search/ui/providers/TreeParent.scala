/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.search.ui.providers

import scala.collection.mutable.ListBuffer

class TreeParent(name : String) extends TreeObject(name) {

  var children = new ListBuffer[TreeObject]

  def addChild(child : TreeObject) : TreeObject = {
    children += child
    child.parent = this
    child
  }

  def removeChild(child : TreeObject) : Unit = {
    if (children.contains(child)) {
      children -= child
      child.parent = null
    }
  }

  def removeChildren : Unit = {
    getChildren foreach { c => removeChild(c) }
  }

  def getChildren : Array[TreeObject] = {
    children.toArray.asInstanceOf[Array[TreeObject]]
  }

  def hasChild(child : TreeObject) : Boolean = {
    if (children.contains(child)) true else false
  }

  def hasChildren : Boolean = {
    children.size > 0
  }
}
