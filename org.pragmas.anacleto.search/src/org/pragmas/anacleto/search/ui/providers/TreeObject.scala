/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.search.ui.providers

//import org.pragmas.anacleto.ProjectResources.TreeObject
import org.pragmas.anacleto.ProjectResources.ProjectTypes


class TreeObject(var name : String) {

  var nodeType : String = ProjectTypes.NODE

  private var __lineNum__ : Int = 0

  var parent : TreeParent = null
  def hasParent : Boolean = if (parent == null) false else true

  var dataNode : org.pragmas.anacleto.ProjectResources.TreeObject = _
  private var __data__ : Any = _

  def data_=(value : Any) : Unit = __data__ = value
  def data = __data__

  private var __path__ : String = null
  def path_=(value : String) = __path__ = value
  def path : String = if(__path__ == null) "" else __path__

  override def toString : String = {
    name
  }

  def lineNum_=(value : Int) : Unit = __lineNum__ = value
  def lineNum : Int = __lineNum__

}
