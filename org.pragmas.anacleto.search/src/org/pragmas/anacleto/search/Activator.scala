/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.search

import org.eclipse.ui.plugin.AbstractUIPlugin
import org.osgi.framework.BundleContext
import org.pragmas.anacleto.logger.Logger

import org.pragmas.anacleto.search.ui.views.SearchView

object Activator extends AbstractUIPlugin {
  var plugin : Activator = _
  def ID = "org.pragmas.anacleto.search"
  val Logger = new Logger(ID)
}

class Activator extends AbstractUIPlugin {

  // The plug-in ID
  def ID = Activator.ID
  Activator.plugin = this

  override def start(context : BundleContext) = {
    super.start(context)
    val id = SearchView.ID
    Activator.Logger.debug("Bundle: start")
  }

  override def stop(context : BundleContext) = {
    Activator.plugin = null
    super.stop(context)
    Activator.Logger.debug("Bundle: stop")
  }

  def getDefault : Activator = {
    Activator.plugin
  }

}
