/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.lucene.indexer

import org.apache.lucene.analysis.standard.StandardAnalyzer
//import org.apache.lucene.analysis.standard.StandardFilter
//import org.apache.lucene.analysis.WhitespaceAnalyzer
//import org.apache.lucene.analysis.tokenattributes.{ OffsetAttribute, TermAttribute, PositionIncrementAttribute }
import org.apache.lucene.document.Document
import org.apache.lucene.document.Field
import org.apache.lucene.index.IndexWriter
//import org.apache.lucene.queryParser.ParseException
//import org.apache.lucene.queryParser.QueryParser
//import org.apache.lucene.search._
import org.apache.lucene.store.RAMDirectory
import org.apache.lucene.store.SimpleFSDirectory
import org.apache.lucene.util.Version
//import org.apache.lucene.queryParser.FastCharStream

//import java.io.IOException;

import java.io._
import scala.collection.mutable.ListBuffer

object PermittedExtensions {
  var EXTENSIONS = List(".txt", ".html")

  def isValid(filename : String) : Boolean = {
    val ext = filename.slice(filename.lastIndexOf('.'), filename.size)
    EXTENSIONS.contains(ext)
  }
}

object TextFileIndexerFiled {
  val CONTENT = "CONTENT"
  val LINE = "LINE"
  val LINE_NUM = "LINENUM"
  val PATH = "PATH"
  val PATHISRELATIVE = "PATHISRELATIVE"
  val DOC_ID = "DOCID"
  val DOC_SCORE = "DOCSCORE"
  val DOC_START_END_TUPLES = "DOCSTARTENDTUPLE"
  val TERM = "TERM"
  val INDEX = "INDEX"
}

class TextFileIndexer(indexDir : String, fileNamePath : String, basePath : String = null) {

  private var isPathRelative = false

  private val queue = ListBuffer[File]()
  private val analyzer = new StandardAnalyzer(Version.LUCENE_30)
  val directory = if (indexDir != null) new SimpleFSDirectory(new File(indexDir)) else new RAMDirectory
  private val writer = new IndexWriter(directory, analyzer, true, IndexWriter.MaxFieldLength.LIMITED)

  private val fileNamePathNorm = if (basePath != null) {
    isPathRelative = true
    new File(basePath, fileNamePath).toString
  } else fileNamePath

  indexFileOrDirectory
  closeIndex
  if (directory.isInstanceOf[SimpleFSDirectory])
    closeDirectory

  private def listFiles(file : File) : Unit = {
    if (file.exists) {
      if (file.isDirectory) file.listFiles foreach { f => listFiles(f) }
      else if (PermittedExtensions.isValid(file.getName.toLowerCase)) queue += file
    }
  }

  private def resolveRelativePath(file : File) : String = {
    if (isPathRelative)
      if (basePath(basePath.size - 1) == '/' || basePath(basePath.size - 1) == '\\')
        file.getAbsolutePath.drop(basePath.size)
      else
        file.getAbsolutePath.drop(basePath.size + 1)
    else
      file.getAbsolutePath
  }

  private def indexFileOrDirectory : Unit = {
    listFiles(new File(fileNamePathNorm))
    val originalnumDocs = writer.numDocs
    queue foreach { f =>
      //println("Indexing File: " + f)
      var fileReader : FileReader = null
      try {
        //val doc = new Document
        fileReader = new FileReader(f)

        val lineNumberReader = new LineNumberReader(fileReader)
        var line = lineNumberReader.readLine
        while (line != null) {
          val num = lineNumberReader.getLineNumber
          val doc = new Document
          doc.add(new Field(TextFileIndexerFiled.LINE, line, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS))
          doc.add(new Field(TextFileIndexerFiled.LINE_NUM, num.toString, Field.Store.YES, Field.Index.NOT_ANALYZED))
          doc.add(new Field(TextFileIndexerFiled.PATH, resolveRelativePath(f), Field.Store.YES, Field.Index.NOT_ANALYZED))
          doc.add(new Field(TextFileIndexerFiled.PATHISRELATIVE, isPathRelative.toString, Field.Store.YES, Field.Index.NOT_ANALYZED))
          writer.addDocument(doc)
          line = lineNumberReader.readLine
        }

        //doc.add(new Field(TextFileIndexerFiled.CONTENT, fileReader, Field.TermVector.WITH_POSITIONS_OFFSETS))
        //doc.add(new Field(TextFileIndexerFiled.PATH, f.getAbsoluteFile.toString, Field.Store.YES, Field.Index.NOT_ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS))

        //writer.addDocument(doc)
      } catch { case e : Exception => println("Could not add: " + f) }
      finally fileReader.close
    }
  }

  def closeIndex : Unit = {
    writer.optimize
    writer.close
  }

  def closeDirectory : Unit = {
    directory.close
  }

}
