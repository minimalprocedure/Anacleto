/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.lucene.indexer

import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.document.Document
import org.apache.lucene.index.Term
import org.apache.lucene.queryParser.QueryParser
import org.apache.lucene.search._
import org.apache.lucene.search.spans._
import org.apache.lucene.store.SimpleFSDirectory
import org.apache.lucene.util.Version
import org.apache.lucene.store.Directory
import java.io._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap

class TextFileSearcher(indexDir : Any) {

  var directory : Directory =
    if (indexDir.isInstanceOf[String])
      new SimpleFSDirectory(new File(indexDir.asInstanceOf[String]))
    else if (indexDir.isInstanceOf[Directory]) indexDir.asInstanceOf[Directory]
    else throw new IllegalArgumentException

  private var searcher : IndexSearcher = _

  open

  def open : Unit = {
    searcher = if (searcher == null) new IndexSearcher(directory) else searcher
  }

  def close : Unit = {
    searcher.close
    searcher = null
  }

  private def getSpans(value : String, searcher : IndexSearcher, hitsPerPage : Int) : List[(Int, Int, Int)] = {
    val query = new SpanTermQuery(new Term(TextFileIndexerFiled.LINE, value))
    val collector = TopScoreDocCollector.create(5 * hitsPerPage, false)
    searcher.search(query, collector)
    val spans = query.getSpans(searcher.getIndexReader)
    val listSpans = ListBuffer[(Int, Int, Int)]()
    while (spans.next) {
      val tp = (spans.doc, spans.start, spans.end)
      listSpans += tp
    }
    listSpans.toList
  }

  private def filterSpansByID(id : Int, spans : List[(Int, Int, Int)]) : List[(Int, Int)] = {
    (spans map { el => if (id == el._1) (el._2, el._3) else null } filter { el => el != null }).toList
  }

  def search(queryString : String, hitsPerPage : Int = 10) : Map[String, List[Map[String, Any]]] = {

    //val queryString = QueryParser.escape(qString)
    val searcher = new IndexSearcher(directory)
    val analyzer = new StandardAnalyzer(Version.LUCENE_30)
    val parser = new QueryParser(Version.LUCENE_30, TextFileIndexerFiled.LINE, analyzer)
    val query = parser.parse(queryString)
    val collector = TopScoreDocCollector.create(5 * hitsPerPage, false)

    searcher.search(query, collector)

    val hits = collector.topDocs.scoreDocs
    val hitCount = collector.getTotalHits
    var resultStruct = HashMap[String, List[Map[String, Any]]]()

      def collect(path : String, struct : Map[String, Any]) : Unit = {
        if (resultStruct.contains(path)) {
          resultStruct(path) = resultStruct(path) ++ List(struct)
        } else {
          resultStruct += (path -> List[Map[String, Any]](struct))
        }
        resultStruct(path) = resultStruct(path).sortBy {el => el(TextFileIndexerFiled.LINE_NUM).asInstanceOf[Int]}
      }

    val collectedFiles = ListBuffer[String]()
    val bufferFiles = ListBuffer[Map[String, Map[String, Any]]]()

    if (hitCount == 0) println("No matches were found for \"" + queryString + "\"")
    else {
      val spans = getSpans(queryString, searcher, hitsPerPage)
      for (index <- 0 to hitCount - 1) {
        val scoreDoc = hits(index)
        val docId = scoreDoc.doc
        val doc = searcher.doc(docId)
        val path = doc.get(TextFileIndexerFiled.PATH)

        val buffer = Map[String, Any](
          TextFileIndexerFiled.INDEX -> (index + 1),
          TextFileIndexerFiled.TERM -> queryString,
          TextFileIndexerFiled.DOC_ID -> scoreDoc.doc,
          TextFileIndexerFiled.DOC_SCORE -> scoreDoc.score,
          TextFileIndexerFiled.PATH -> doc.get(TextFileIndexerFiled.PATH),
          TextFileIndexerFiled.PATHISRELATIVE -> doc.get(TextFileIndexerFiled.PATHISRELATIVE),
          TextFileIndexerFiled.LINE -> doc.get(TextFileIndexerFiled.LINE),
          TextFileIndexerFiled.LINE_NUM -> doc.get(TextFileIndexerFiled.LINE_NUM).toInt,
          TextFileIndexerFiled.DOC_START_END_TUPLES -> filterSpansByID(scoreDoc.doc, spans))

        collect(path, buffer)
      }
    }
    resultStruct.toMap
  }

}
