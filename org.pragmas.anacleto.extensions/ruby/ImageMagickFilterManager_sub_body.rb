#-------------------------------------------------------------------------------
# Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
# 
# Contributors:
#     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
#-------------------------------------------------------------------------------
filter = @extensionType.new($__input_data__)
$__output_data__ = filter.execute
