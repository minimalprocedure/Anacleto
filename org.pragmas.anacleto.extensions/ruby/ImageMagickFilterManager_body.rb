#-------------------------------------------------------------------------------
# Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
# 
# Contributors:
#     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
#-------------------------------------------------------------------------------
class ImageMagickFilter
    include ImageMagickFilterManagerModule

    attr_accessor :image, :image_info

    def initialize(__input_data__)
      @image = __input_data__
    end

    def execute()
        @image = load_image("/home/nissl/backgrounds/background-1.png")
        save_image("/home/nissl/backgrounds/background-1_saved.png")
        save_image("/home/nissl/backgrounds/background-1_saved.jpg")
      @image
    end
end

$__output_data__ = ImageMagickFilter.new($__input_data__).execute
