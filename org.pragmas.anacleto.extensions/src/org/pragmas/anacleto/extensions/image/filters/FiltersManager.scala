/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.extensions.image.filters

import java.io._
import javax.script._
import java.util._
import java.net.URLClassLoader
import java.net.URL

import scala.collection.mutable._
import org.jruby.embed.jsr223._

import org.pragmas.anacleto.extensions._


import magick.{MagickImage, ImageInfo, QuantizeInfo, ColorspaceType}

trait ImageMagickExtensionManipulator {

  private var __image : MagickImage = null
  private var __imageInfo : ImageInfo = null

  def image : MagickImage = __image
  def image_=(image : MagickImage) : Unit = {
    __image = image
  }

  def loadImage(filepath : String) : Unit = {
    __imageInfo = new ImageInfo(filepath)
    __image = new MagickImage(__imageInfo)
  }

  def execute

}

class ImageMagickFilterManager(engine : ScriptEngine) {

  private val SCRIPT_TEST = """
    class ImageMagickFilter
        include ImageMagickExtensionManipulator

        def initialize(magickImage)
            image = magickImage
        end

        def execute()
            image
        end
    end
  """

  private val VAR_MAGICK_IMAGE = "magickImage"
  private val VAR_EXTENSION = "filterObject"

  private val __head = """
    include_class 'org.pragmas.anacleto.extensions.image.filters.Extension'
    include_class 'org.pragmas.anacleto.extensions.image.filters.ImageMagickFilter'
    include_class 'magick.MagickImage'
    include_class 'magick.ImageInfo'
    include_class 'magick.QuantizeInfo'
    include_class 'magick.ColorspaceType'
  """

  private val __tail = """
    puts "Initialize JRuby plugin\n"
    $filterObject = ImageMagickFilter.new(image).execute
  """

  def execute(source : String, magickImage : MagickImage) : MagickImage = {
    if(engine != null) {
      engine.put(VAR_MAGICK_IMAGE, magickImage)
      val scriptSource = __head + "\n" + source + "\n" + __tail
      engine.eval(scriptSource)
      val context = engine.get(VAR_EXTENSION).asInstanceOf[ImageMagickExtensionManipulator]
      context.asInstanceOf[MagickImage]
    } else null
  }
}

/*
     engine.put(VAR_COUNTER, counter)

     engine.eval(source) // eval by source
     val context = engine.get(VAR_EXTENSION).asInstanceOf[Extension]
     context.execute
     println("counter: " + counter.getI)
     context

 */



/*



class Counter {
  private var i: Int = 0
  def getI = i

  def setI(i: Int) = {
      this.i = i
  }
}

trait Extension {
  def execute
}

object FiltersManager {

  val scriptTest = """

include_class 'org.pragmas.anacleto.extensions.image.filters.Counter'
include_class 'org.pragmas.anacleto.extensions.image.filters.Extension'

include_class 'magick.MagickImage'
include_class 'magick.ImageInfo'
include_class 'magick.QuantizeInfo'
include_class 'magick.ColorspaceType'

class RubyExtension
    include Extension
    def initialize(c)
        @counter = c
    end

    def execute()
        i = @counter.getI()
        puts "JRuby initial counter = #{i}\n"
        @counter.setI(i + 1)
    end
end

puts "Initialize JRuby plugin\n"
$extension = RubyExtension.new($counter)
"""

  val scriptTest2 = "puts $LOAD_PATH"

  val EXTENSION_PREFIX = "extension"
  val VAR_EXTENSION = "extension"

  val VAR_COUNTER = "counter"

  // prepare the application object model
  val counter = new Counter
  // prepare plug-in repository (just a list)
  var extensions = new ArrayBuffer[Extension]()

  //---------------------------

  prepareClassPath

  private val factory = new JRubyEngineFactory
  private val engine = factory.getScriptEngine

   private def prepareClassPath : Unit = {
     var cpath = Activator.context.getBundle.getHeaders.get("Bundle-ClassPath").asInstanceOf[String]
     cpath += ":./org.pragmas.anavleto.extensions"
     System.setProperty("org.jruby.embed.class.path", cpath);
     System.setProperty("jruby.home",cpath);
   }

   def executeExtensionSource(source : String) : Unit = {

     engine.put(VAR_COUNTER, counter)

     engine.eval(source) // eval by source
     val context = engine.get(VAR_EXTENSION).asInstanceOf[Extension]
     context.execute
     println("counter: " + counter.getI)
     context
   }

  def executePlugins {
    extensions.foreach(p => p.execute)
    // check the effect
    println("Final counter = " + counter.getI)
  }

  def loadExtensions(extensionDir : File) : Unit = {

    val extensionScripts : Array[File] = extensionDir.listFiles(
      new FilenameFilter {
        override def accept(dir: File, name: String) = name.startsWith(EXTENSION_PREFIX)
    })

    val factory = new ScriptEngineManager

    for(script <- extensionScripts){
      val name = script.getName
      val ext = name.substring(name.indexOf(".") + 1, name.length)
      val engine = factory.getEngineByExtension(ext)
      if(engine != null) {
        // pass the object model
        engine.put(VAR_COUNTER, counter)
        // evaluate the plug-in script
        engine.eval(new FileReader(script.getAbsolutePath))
        // store the plug-in
        extensions += engine.get(VAR_EXTENSION).asInstanceOf[Extension]
      }
    }
  }
}

*/
