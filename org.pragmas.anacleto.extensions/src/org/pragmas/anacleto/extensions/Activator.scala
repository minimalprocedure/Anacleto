/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.extensions

import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.ui.plugin.AbstractUIPlugin
import org.osgi.framework.BundleContext
import org.eclipse.core.runtime.Platform
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.Path

import java.io.File
import java.io.InputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.IOException

object Activator /*extends AbstractUIPlugin*/ {
  var plugin : Activator = _
  def ID = "org.pragmas.anacleto.extensions"
  var context :BundleContext = _

  def getImageDescriptor(path : String) : ImageDescriptor = {
    AbstractUIPlugin.imageDescriptorFromPlugin(ID, path)
  }
/*
  def getBundleLocationPath : String = {
    val bundle = Platform.getBundle(ID)
    val locationUrl = FileLocator.find(bundle, new Path("/"), null)
    val fileUrl = FileLocator.toFileURL(locationUrl)
    fileUrl.getFile
  }

  def getClassLocationPath(c : Any) : String = {
    Activator.getBundleLocationPath +
      this.getClass.getName.replaceAll(".", "/")
  }

  def getResourceAsStream(resource : String) : InputStream = {
    val bundle = Platform.getBundle(ID)
    val entry = bundle.getEntry(resource)
    if(entry != null) entry.openStream else null
  }
*/

  /* convert InputStream to String*/
  private def ISToString(inputStream : InputStream) : String = {
    val dataBuffer = new BufferedReader(new InputStreamReader(inputStream))
    val stringBuilder = new StringBuilder
    try{
      var line = dataBuffer.readLine
      while ( line != null ) {
        stringBuilder.append(line + "\n")
        line = dataBuffer.readLine
      }
    } catch {
      case ex : Exception => ex.getMessage
    } finally {
      inputStream.close
      dataBuffer.close
    }
    stringBuilder.toString
  }

  def getResourceAsString(resource : String) : String = {
    val bundle = Platform.getBundle(ID)
    val entry = bundle.getEntry(resource)
    if(entry != null) ISToString(entry.openStream) else ""
  }

}

class Activator extends AbstractUIPlugin  {
  // The plug-in ID
  def ID = Activator.ID

  Activator.plugin = this

  override def start(context :BundleContext ) = {
    Activator.context = context
    super.start(context)
  }

  override def stop(context :BundleContext ) = {
    Activator.plugin = null
    super.stop(context)
  }

  def getDefault : Activator = {
    Activator.plugin
  }

  def getImageDescriptor(path : String) : ImageDescriptor = {
    Activator.getImageDescriptor(path)
  }

}
