/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.extensions

import java.io._
import javax.script._
//import java.util._
import java.net.URLClassLoader
import java.net.URL

import magick.{ MagickImage, ImageInfo, QuantizeInfo, ColorspaceType }

import scala.collection.mutable._
import org.jruby.embed.jsr223._

import org.pragmas.anacleto.extensions.scriptlets._

object ExtensionType {
  val IMAGE_MAGICK_MANIPULATOR = "imagemagick.manipulator"
}

object ExtensionConstants {
  val DATA_INPUT = "__input_data__"
  val DATA_OUTPUT = "__output_data__"
}

object ExtensionManager {

  private val factory = new JRubyEngineFactory
  //private val engine = factory.getScriptEngine

  prepareClassPath

  private def prepareClassPath : Unit = {
    var cpath = Activator.context.getBundle.getHeaders.get("Bundle-ClassPath").asInstanceOf[String]
    cpath += ":./org.pragmas.anacleto.extensions"
    System.setProperty("org.jruby.embed.class.path", cpath);
    System.setProperty("jruby.home", cpath);
  }

  def execute(sourceData : Map[String, String], data : Any, extensionType : String) : Any = {
    extensionType match {
      case ExtensionType.IMAGE_MAGICK_MANIPULATOR => {
        val scriptletManager = new ImageMagickFilterManager(factory.getScriptEngine)
        scriptletManager.execute(sourceData, data.asInstanceOf[MagickImage])
      }
      case _ => null
    }

  }

  def getScriptletMetas(source : String) : Map[String, String] = {
    val MetaBlock = """(?idmsux)=begin\s*(.*)\s*=end""".r
    val MetaInfos = """@(extensionType|returnType|description|author|version?)\s+(.*)""".r
    val Meta = """@(.*?)\s+(.*)""".r
    val metaBlock = (MetaBlock findFirstIn source).get
    var metas : Map[String, String] = Map()
    for (Meta(metaName, metaValue) <- MetaInfos findAllIn metaBlock)
      metas += (metaName.toString -> metaValue.toString)
    metas
  }

  def readScriptletFile(filepath : String) : Map[String, String] = {
    val fileScriptlet = new File(filepath)
    val reader = new FileReader(fileScriptlet)
    val dataBuffer = new BufferedReader(reader)
    val stringBuilder = new StringBuilder
    try {
      var line = dataBuffer.readLine
      while (line != null) {
        stringBuilder.append(line + "\n")
        line = dataBuffer.readLine
      }
    } catch {
      case ex : Exception => ex.getMessage
    }
    finally {
      dataBuffer.close
      reader.close
    }
    val source = stringBuilder.toString
    val data = getScriptletMetas(source)
    data += ("source" -> source)
    data
  }

}
