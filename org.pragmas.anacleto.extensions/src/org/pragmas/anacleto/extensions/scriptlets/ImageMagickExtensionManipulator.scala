/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.extensions.scriptlets

import java.io._
import javax.script._
//import java.util._
import java.net.URLClassLoader
import java.net.URL

import scala.collection.mutable._
import scala.util.matching.Regex
import org.jruby.embed.jsr223._

import org.pragmas.anacleto.extensions._

import magick.MagickImage

class ImageMagickFilterManager(engine : ScriptEngine) {

  val apiModuleSource = Activator.getResourceAsString("/ruby/" + getClassNameShort + "_api.rb")
  val apiHeadSource = Activator.getResourceAsString("/ruby/" + getClassNameShort + "_head.rb")
  val apiTailSource = Activator.getResourceAsString("/ruby/" + getClassNameShort + "_tail.rb")
  //val apiBodySourceTest = Activator.getResourceAsString("/ruby/" + getClassNameShort + "_body.rb")
  val apiBodySubSource = Activator.getResourceAsString("/ruby/" + getClassNameShort + "_sub_body.rb")

  private def getClassNameShort : String = {
    this.getClass.getName.replace(this.getClass.getPackage.getName + ".", "")
  }

  def execute(sourceData : Map[String, String], magickImage : MagickImage) : MagickImage = {
    if(engine != null && magickImage != null && !sourceData("source").isEmpty) {
      if(sourceData.contains("extensionType") && sourceData.contains("returnType")) {
        engine.put(ExtensionConstants.DATA_INPUT, magickImage)
        val scriptSource = new StringBuilder
        scriptSource.append("\n#<<--apiHeadSource-->>\n" + apiHeadSource)
        scriptSource.append("\n#<<--apiModuleSource-->>\n" + apiModuleSource)
        scriptSource.append("\n#<<--source-->>\n" + sourceData("source"))
        scriptSource.append("\n#<<--apiBodySubSource-->>\n" + apiBodySubSource.replaceAll("@extensionType", sourceData("extensionType")))
        scriptSource.append("\n#<<--apiTailSource-->>\n" + apiTailSource)
        engine.eval(scriptSource.toString)
        val context = engine.get(ExtensionConstants.DATA_OUTPUT).asInstanceOf[MagickImage]
        sourceData("returnType") match {
          case "MagickImage" => context.asInstanceOf[MagickImage]
         case _ => null
        }
      } else null
    } else null
  }
}
