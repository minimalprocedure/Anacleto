#-------------------------------------------------------------------------------
# Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
#-------------------------------------------------------------------------------
#include_class 'org.pragmas.anacleto.extensions.scriptlets.ImageMagickExtensionManipulator'
include_class 'java.awt.Rectangle'
include_class 'java.awt.Dimension'
include_class 'magick.MagickImage'
include_class 'magick.ImageInfo'
include_class 'magick.QuantizeInfo'
include_class 'magick.ColorspaceType'
include_class 'magick.ProfileInfo'
include_class 'magick.PixelPacket'
include_class 'magick.MontageInfo'

module ImageMagickFilterManagerModule
  def sample!(col, rows)
    @image = @image.sampleImage(cols, rows)
  end

  def sample(col, rows)
    @image.sampleImage(cols, rows)
  end

  def scale!(col, rows)
    @image = @image.scaleImage(cols, rows)
  end

  def scale(col, rows)
    @image.scaleImage(cols, rows)
  end

  def cloneImage!(to_be_clone = @image)
    @image = to_be_clone.cloneImage(0, 0, true)
  end

  def cloneImage(to_be_clone = @image)
    to_be_clone.cloneImage(0, 0, true)
  end

  def clone
    @image.cloneImage(0, 0, true)
  end

  def equalize!
    @image.equalizeImage()
  end

  def normalize
    @image.normalizeImage()
  end

  def rotate!(rotationStep = 90)
    @image = @image.rotateImage(rotationStep)
  end

  def rotate(rotationStep = 90)
    @image.rotateImage(rotationStep)
  end

  def greyscale!
    qi = QuantizeInfo.new
    qi.setColorspace(ColorspaceType.GRAYColorspace)
    @image.quantizeImage(qi)
  end

  def crop!(chopInfo)
    @image = @image.cropImage(chopInfo)
  end

  def crop(chopInfo)
    @image.cropImage(chopInfo)
  end

  #def greyscale
  #  qi = QuantizeInfo.new
  #  qi.setColorspace(ColorspaceType.GRAYColorspace)
  #  @image.quantizeImage(qi)
  #end

  def contrast!(sharpen = true)
    @image = @image.contrastImage(sharpen)
  end

  def contrast(sharpen = true)
    @image.contrastImage(sharpen)
  end

  def sharp!(radius = 6, sigma = 3)
    @image = @image.sharpenImage(radius, sigma)
  end

  def sharp(radius = 6, sigma = 3)
    @image.sharpenImage(radius, sigma)
  end

  def enhance
    @image.enhanceImage()
  end

  def enhance!
    @image = @image.enhanceImage()
  end

  def unsharp_mask!(raduis = 6.0, sigma = 3.0, amount = 1.0, threshold = 0.0)
    @image = @image.unsharpMaskImage(raduis,sigma,amount,threshold)
  end

  def unsharp_mask(raduis = 6.0, sigma = 3.0, amount = 1.0, threshold = 0.0)
    @image.unsharpMaskImage(raduis,sigma,amount,threshold)
  end

  def load_image(filepath)
    @image_info = ImageInfo.new(filepath)
    @image = MagickImage.new(@image_info)
  end

  def save_image(filepath)
    @image_info = ImageInfo.new()
    @image.setFileName(filepath)
    @image.writeImage(@image_info)
  end

end

class ImageFilter
  include ImageMagickFilterManagerModule
  attr_accessor :image, :image_info
  def initialize(__input_data__)
    #@image = cloneImage(__input_data__)
    cloneImage!(__input_data__)
  end

  def execute
    @image
  end

end
