#-------------------------------------------------------------------------------
# Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
# 
# Contributors:
#     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
#-------------------------------------------------------------------------------
=begin

@extensionType ImageMagickFilterTest
@returnType MagickImage
@description: unsharp_mask
@author minimalprocedure
@version 1.0

=end
#TODO: none
class ImageMagickFilterTest < ImageFilter
    def execute
      unsharp_mask!(6.0, 3.0, 9.0, 0)
      image
    end
end

