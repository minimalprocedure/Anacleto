/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui

import java.io.File

import org.eclipse.ui.application.IWorkbenchConfigurer
import org.eclipse.equinox.app.IApplication
import org.eclipse.equinox.app.IApplicationContext
import org.eclipse.swt.widgets.Display
import org.eclipse.ui.IWorkbench
import org.eclipse.ui.PlatformUI
import org.eclipse.swt.widgets.Shell

import org.eclipse.core.runtime.Platform
import org.pragmas.anacleto.tools.Environment

/**
 * This class controls all aspects of the application's execution
 */

object Application {
  val TITLE = "Hi.Doc"
  val VERSION_MAJOR = 0
  val VERSION_MINOR = 9
  val VERSION_SUBMINOR = 0
  val VERSION_MARKER = "0"
  val COPYRIGHT = "(c) Massimo Maria Ghisalberti 2010\n<minimal.procedure@gmail.com> http://pragmas.org"

  def VERSION : String = {
    VERSION_MAJOR.toString + "." +
      VERSION_MINOR.toString + "." +
      VERSION_SUBMINOR.toString + "." +
      VERSION_MARKER.toString
  }

  var configurer : IWorkbenchConfigurer = _

  val PATH = (Platform.getInstallLocation.getURL.toString
    .replaceFirst(Platform.getInstallLocation.getURL.getProtocol + ":", ""))

  private def environSettings : Unit = {

    /*
      MAGICK_HOME=<imagemagick>
      MAGICK_CONFIGURE_PATH=<imagemagick>\config
      MAGICK_CODER_MODULE_PATH=<imagemagick>\modules\coders
      MAGICK_CODER_FILTER_PATH=<imagemagick>\modules\filters
      MAGICK_FONT_PATH=<fontpath>
      LD_LIBRARY_PATH=<imagemagick>
     */

    Environment.unsetenv("JRUBY_HOME")

    if (System.getProperty("jmagick.home") != null) {
      val jmagickHome = new File(Application.PATH, System.getProperty("jmagick.home")).toString
      Environment.setenv("MAGICK_HOME", jmagickHome, true)
    }

    if (System.getProperty("jmagick.configure.path") != null) {
      val jmagickConfigurePath = new File(Application.PATH, System.getProperty("jmagick.configure.path")).toString
      Environment.setenv("MAGICK_CONFIGURE_PATH", jmagickConfigurePath, false)
    }

    if (System.getProperty("jmagick.coder.module.path") != null) {
      val jmagickCoderModulePath = new File(Application.PATH, System.getProperty("jmagick.coder.module.path")).toString
      Environment.setenv("MAGICK_CODER_MODULE_PATH", jmagickCoderModulePath, false)
    }

    if (System.getProperty("jmagick.coder.filter.path") != null) {
      val jmagickCoderFilterPath = new File(Application.PATH, System.getProperty("jmagick.coder.filter.path")).toString
      Environment.setenv("MAGICK_CODER_FILTER_PATH", jmagickCoderFilterPath, false)
    }

    if (System.getProperty("jmagick.font.path") != null) {
      val jmagickFontPath = new File(Application.PATH, System.getProperty("jmagick.font.path")).toString
      Environment.setenv("MAGICK_FONT_PATH", jmagickFontPath, false)
    }

    if (System.getProperty("ld.library.path") != null) {
      val ldLibraryPath = new File(Application.PATH, System.getProperty("ld.library.path")).toString
      Environment.setenv("LD_LIBRARY_PATH", ldLibraryPath, false)
    }

    if (System.getProperty("win.path") != null) {
      val winPath = new File(Application.PATH, System.getProperty("win.path")).toString
      if (System.getProperty("os.name").equals("Linux"))
        Environment.setenv("PATH", winPath + ":$PATH", false)
      else
        Environment.setenv("PATH", winPath + ";%PATH%", false)
    }

  }

  def printEnviron : Unit = {
    println("=============== ENVIRON VARIABLES ===============\n")
    val envs = Environment.getenv
    val keysIterator = envs.keySet.iterator
    while (keysIterator.hasNext) {
      val key = keysIterator.next
      println(key + "=" + envs.get(key).toString)
    }
    println("=================================================\n")
  }

  environSettings
  //printEnviron
}

class Application extends IApplication {

  override def start(context : IApplicationContext) : Object = {
    val display = PlatformUI.createDisplay
    try {
      val applicationWorkbenchAdvisor = new ApplicationWorkbenchAdvisor
      val returnCode = PlatformUI.createAndRunWorkbench(display, applicationWorkbenchAdvisor)
      if (returnCode == PlatformUI.RETURN_RESTART) { return IApplication.EXIT_RESTART } else { return IApplication.EXIT_OK }
    } finally { display.dispose }

  }

  override def stop : Unit = {
    val workbench = PlatformUI.getWorkbench
    if (workbench == null) return
    val display = workbench.getDisplay
    display.syncExec(new Runnable { override def run : Unit = if (!display.isDisposed) workbench.close })
  }
}
