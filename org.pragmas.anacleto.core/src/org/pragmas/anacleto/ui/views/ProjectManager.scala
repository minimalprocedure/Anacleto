/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui.views

import org.eclipse.ui.views.properties.PropertySheetPage
import org.eclipse.core.runtime.IAdaptable
import org.eclipse.ui.views.properties.PropertySheetEntry
import java.io.File
import java.net.URI
import java.io.OutputStreamWriter
import java.util.UUID
import org.eclipse.jface.viewers.TreeViewer
import org.eclipse.jface.viewers.Viewer
import org.eclipse.jface.viewers.ISelectionChangedListener
import org.eclipse.jface.viewers.SelectionChangedEvent
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.jface.viewers.StructuredViewer
import org.eclipse.jface.viewers.IOpenListener
import org.eclipse.jface.viewers.OpenEvent
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Composite
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.IWorkbench
import org.eclipse.ui.part.ViewPart
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.ui.ide.FileStoreEditorInput
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.filesystem.IFileStore
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.IPath
import org.eclipse.core.filesystem.URIUtil
import org.pragmas.anacleto.logger.Logger
import org.pragmas.anacleto.commands._
import org.pragmas.anacleto.ui.editors._
import org.pragmas.anacleto.ProjectResources._
import org.pragmas.anacleto.models._
import com.hp.hpl.jena.rdf.model._
import scala.collection.mutable.ListBuffer
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.logger.Logger
import org.eclipse.jface.action.IStatusLineManager
import org.eclipse.jface.action._
import org.eclipse.ui.progress.UIJob
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.IStatus
import org.eclipse.core.runtime.Status
import org.pragmas.anacleto.implicits.Jobs._
import org.pragmas.anacleto.tools._
import scalax.pragmas.File._
import java.io._
import magick.{ MagickImage, ImageInfo, QuantizeInfo, ColorspaceType }
import org.pragmas.anacleto.Activator
import org.pragmas.anacleto.compression.Compression

import org.eclipse.ui.views.properties.IPropertySource
import org.eclipse.ui.ISaveablePart2
import java.util.Calendar
import java.text.SimpleDateFormat
import org.eclipse.ui.IPageLayout

object ProjectManagerSavePolicy {
  val MANUAL = 1000
  val AUTO = 1010
  val TIMER = 1020
}

object ProjectManager {
  val ID = "org.pragmas.anacleto.ui.views.ProjectManager"
  var Project : ProjectRdfModel = _
  var projectRoot : TreeParent = _
  //var ProjectPathUri : String = _
  var ProjectPathParentStore : IPath = _
  var ProjectPathStore : IPath = _
  var ProjectPath : IPath = _

  //val dumpingFormat = "TURTLE"
  var dumpingFormat = RDFDumpingFormat.DEFAULT

  /*TODO: load at runtime for plugins contributors */
  val editors : Array[Map[String, String]] = Array(
    Map("name" -> "ImageEditor", "id" -> ImageEditor.ID,
      "extensions" -> "jpg,png,tif"),
    Map("name" -> "markupEditor", "id" -> ScriptEditor.ID,
      "extensions" -> ProjectTypes.RESOURCES_TEXT_CONCAT),
    Map("name" -> "PersonEditor", "id" -> PersonEditor.ID,
      "extensions" -> ProjectTypes.RESOURCES_PERSON),
    Map("name" -> "LocationEditor", "id" -> LocationEditor.ID,
      "extensions" -> ProjectTypes.RESOURCES_LOCATION),
    Map("name" -> "MultiPurposeEditor", "id" -> MultiPurposeEditor.ID,
      "extensions" -> ProjectTypes.RESOURCES_FILTERS_CONCAT))

  def resolveEditorIdByExt(extension : String) : String = {
    var id = ""
    for (
      editor <- ProjectManager.editors;
      extensions = editor("extensions").split(Array(',', ';'));
      if (extensions.contains(extension))
    ) id = editor("id")
    id
  }

}

object ProjectManagerStatusMessage {

  val SAVED = "saved"
  val TO_BE_SAVE = "to be save"
  val OPEN = "open"
  val LOADED = "loaded"
  val CLOSED = "closed"
  var CURRENT = ""

  def set(value : String = "") : Unit = CURRENT = value
  def get = CURRENT
  def reset : Unit = CURRENT = ""
}

class ProjectManager extends ViewPart with ISaveablePart2 {

  val ID = ProjectManager.ID
  var currentNode : TreeObject = _

  var pagesContainer : TreeParent = _
  var scriptsContainer : TreeParent = _
  var filtersContainer : TreeParent = _
  var summariesContainer : TreeParent = _
  var personsContainer : TreeParent = _
  var locationsContainer : TreeParent = _
  var notesContainer : TreeParent = _
  var tagsContainer : TreeParent = _
  var commentsContainer : TreeParent = _

  private var dirty : Boolean = false
  private var viewer : TreeViewer = _
  private var projectStructure : Map[String, TreeParent] = _
  private var statusLineManager : IStatusLineManager = _
  private var statusLineContributors : Array[IContributionItem] = _
  private var removedResources : ListBuffer[String] = ListBuffer()

  private val DATE_FORMAT_BACKUP = "yyyyMMddHHmmss"

  //private var PART_NAME : String = "Project Manager"
  //this.setPartName(PART_NAME)

  Perspective.projectManager = this

  var SAVE_POLICY = ProjectManagerSavePolicy.AUTO

  def setDirty(value : Boolean) : Unit = {
    dirty = value
    ProjectManagerStatusMessage.set(
      if (dirty == false) ProjectManagerStatusMessage.OPEN else ProjectManagerStatusMessage.TO_BE_SAVE
    )
    //if (dirty == false) this.setPartName(PART_NAME) else this.setPartName("* " + PART_NAME)
    setStatusMessage
  }

  def isDirty : Boolean = dirty
  def isLoaded : Boolean = if (ProjectManager.ProjectPathStore == null) false else true

  private def saveProjectByPolicy : Unit = {
    SAVE_POLICY match {
      case ProjectManagerSavePolicy.AUTO   => ""
      case ProjectManagerSavePolicy.MANUAL => ""
      case ProjectManagerSavePolicy.TIMER  => ""
      case _                               => ""
    }

  }

  private def prepareStatusline : Unit = {
    if (statusLineContributors == null) {
      statusLineManager = getViewSite.getActionBars.getStatusLineManager
      if (statusLineManager != null) {

        var contribution = new StatusLineContributionItem("project.status.saved")
        statusLineManager.add(contribution)
        contribution.setText(" ")

        contribution = new StatusLineContributionItem("project.currentNode")
        statusLineManager.add(contribution)
        contribution.setText(" ")

        statusLineContributors = statusLineManager.getItems
      }
    }
  }

  private def setStatusMessage : Unit = {
    val uiJob = new UIJob("setStatusMessage") {
      def runInUIThread(monitor : IProgressMonitor) : IStatus = {
        prepareStatusline
        statusLineManager.setMessage("Project status: " + ProjectManagerStatusMessage.get)
        ProjectManagerStatusMessage.reset
        Status.OK_STATUS
      }
    }
    uiJob.schedule
  }

  private def setStatusCurrentNodeMessage(value : String) : Unit = {
    prepareStatusline
    statusLineContributors(1).
      asInstanceOf[StatusLineContributionItem].
      setText("Node info: " + value)
  }

  private def loadProjectFromUri(projectPathURI : String) : TreeObject = {
    resolveProjectFilePath(projectPathURI)

    backupProject

    ProjectManager.Project = new ProjectRdfModel
    val anaProject = ProjectManager.Project
    anaProject.loadModelFromUri(URIUtil.toURI(ProjectManager.ProjectPath).toString, ProjectManager.dumpingFormat)
    val projectData = anaProject.getProjectData
    projectStructure = addBasicProjectStructure(projectData)
    val pagesData = anaProject.getPagesData

    var childAdded : TreeObject = null
    for (pageData <- pagesData) {
      val pageStructure = addBasicPageStructure(pageData)
      projectStructure(ProjectTypes.PAGES).addChild(pageStructure(ProjectTypes.PAGE))

      //Activator.Logger.info("Load images")
      val images = anaProject.getElementsfromPage(ProjectTypes.IMAGE, pageData(APRJNames.name).toString)
      for (image <- images) {
        childAdded = pageStructure(ProjectTypes.IMAGES).addChild(new TreeObject(image(APRJNames.title).toString, ProjectTypes.IMAGE))
        childAdded.store = image
        //Activator.Logger.debug("[ProjectManager::loadProjectFromUri]" + image.toString)
      }

      //Activator.Logger.info("Load scripts")
      val scripts = anaProject.getElementsfromPage(ProjectTypes.SCRIPT, pageData(APRJNames.name).toString)
      for (script <- scripts) {
        childAdded = pageStructure(ProjectTypes.SCRIPTS).addChild(new TreeObject(script(APRJNames.title).toString, ProjectTypes.SCRIPT))
        childAdded.store = script
        referenceResource(childAdded)
      }

      //Activator.Logger.info("Load summaries")
      val summaries = anaProject.getElementsfromPage(ProjectTypes.SUMMARY, pageData(APRJNames.name).toString)
      for (summary <- summaries) {
        childAdded = pageStructure(ProjectTypes.SUMMARIES).addChild(new TreeObject(summary(APRJNames.title).toString, ProjectTypes.SUMMARY))
        childAdded.store = summary
        referenceResource(childAdded)
      }

      //Activator.Logger.info("Load persons")
      val persons = anaProject.getElementsfromPage(ProjectTypes.PERSON, pageData(APRJNames.name).toString)
      for (person <- persons) {
        childAdded = pageStructure(ProjectTypes.PERSONS).addChild(new TreeObject(person(APRJNames.title).toString, ProjectTypes.PERSON))
        childAdded.store = person
        referenceResource(childAdded)
      }

      //Activator.Logger.info("Load locations")
      val locations = anaProject.getElementsfromPage(ProjectTypes.LOCATION, pageData(APRJNames.name).toString)
      for (location <- locations) {
        childAdded = pageStructure(ProjectTypes.LOCATIONS).addChild(new TreeObject(location(APRJNames.title).toString, ProjectTypes.LOCATION))
        childAdded.store = location
        referenceResource(childAdded)
      }

      //Activator.Logger.info("Load notes")
      val notes = anaProject.getElementsfromPage(ProjectTypes.NOTE, pageData(APRJNames.name).toString)
      for (note <- notes) {
        childAdded = pageStructure(ProjectTypes.NOTES).addChild(new TreeObject(note(APRJNames.title).toString, ProjectTypes.NOTE))
        childAdded.store = note
        referenceResource(childAdded)
      }

      //Activator.Logger.info("Load tags")
      val tags = anaProject.getElementsfromPage(ProjectTypes.TAG, pageData(APRJNames.name).toString)
      for (tag <- tags) {
        val tag_type = if (tag.contains("type") /*tag("type") != null*/ ) tag("type").toString else ProjectTypes.TAG
        childAdded = pageStructure(ProjectTypes.TAGS).addChild(new TreeObject(tag(APRJNames.title).toString, tag_type))
        childAdded.store = tag
        referenceResource(childAdded)
      }

      //Activator.Logger.info("Load comments")
      val comments = anaProject.getElementsfromPage(ProjectTypes.COMMENT, pageData(APRJNames.name).toString)
      for (comment <- comments) {
        childAdded = pageStructure(ProjectTypes.COMMENTS).addChild(new TreeObject(comment(APRJNames.title).toString, ProjectTypes.COMMENT))
        childAdded.store = comment
        referenceResource(childAdded)
      }

      //Activator.Logger.info("Load filters")
      loadFilters(projectStructure(ProjectTypes.FILTERS))
    }
    projectStructure(ProjectTypes.ROOT)
  }

  private def loadFilters(filters : TreeParent) : Unit = {
    val filtersPath = ProjectManager.ProjectPathParentStore.append(ProjectTypes.APPDATA_CONTAINER).append(ProjectTypes.FILTERS)
    if (filtersPath != null) {
      val filtersStore = EFS.getLocalFileSystem.getStore(filtersPath)
      val filterPathList = ResourceManager.getFilePaths(filtersStore)
      for (filter <- filterPathList) {
        val node = new TreeObject(filter.lastSegment, ProjectTypes.IMAGE_FILTER)
        filters.addChild(node)
      }
    }
    filters
  }

  def addImageFilter(name : String, ext : String, source : String) : Unit = {
    import FileHelper._
    val filtersPath = ProjectManager.ProjectPathParentStore.append(ProjectTypes.APPDATA_CONTAINER).append(ProjectTypes.FILTERS)
    if (filtersPath != null) {
      val script = new File(filtersPath.toString, name + "." + ext)
      script.write(source)
      filtersContainer.removeChildren
      loadFilters(filtersContainer)
      refresh
    }
  }

  private def initEmptyProjectRdf(data : Map[String, String]) : String = {
    val anaProject = new ProjectRdfModel
    anaProject.createProject(data(APRJNames.name), Map(
      APRJNames.uuid -> UUID.randomUUID.toString,
      APRJNames.title -> data(APRJNames.title),
      APRJNames.subject -> data(APRJNames.subject),
      APRJNames.description -> data(APRJNames.description),
      APRJNames.creator -> data(APRJNames.creator)))
    anaProject.dumpIn(ProjectManager.dumpingFormat)
  }

  def initEmptyProject(projectLocation : String, data : Map[String, String]) : Unit = {
    if (!projectLocation.isEmpty && !data(APRJNames.name).isEmpty && !data(APRJNames.title).isEmpty) {
      val newProjectPath = projectLocation + IPath.SEPARATOR + data(APRJNames.title)
      resolveProjectFilePath(newProjectPath, true)

      EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPathParentStore.append(ProjectTypes.APPINDEX_CONTAINER)).mkdir(EFS.NONE, null)
      EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPathParentStore.append(ProjectTypes.ARCHIVES_CONTAINER)).mkdir(EFS.NONE, null)
      EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPathParentStore.append(ProjectTypes.MATERIALS_CONTAINER)).mkdir(EFS.NONE, null)

      EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPathStore).mkdir(EFS.NONE, null)
      EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPathStore.append(ProjectTypes.PAGES)).mkdir(EFS.NONE, null)
      EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPathStore.append(ProjectTypes.FILTERS)).mkdir(EFS.NONE, null)
      //EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPathStore.append(ProjectTypes.ARCHIVES)).mkdir(EFS.NONE, null)
      EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPathStore.append(ProjectTypes.BACKUPS)).mkdir(EFS.NONE, null)
      val rdf = initEmptyProjectRdf(data)
      setDirty(true)
      dumpProjectResourceToFile(ProjectManager.ProjectPathStore, rdf)
      openProject(newProjectPath)
      setDirty(true)
    }
  }

  private def backupsStore : IPath = {
    ProjectManager.ProjectPathStore.append(ProjectTypes.BACKUPS)
  }

  //  private def archivesStore : IPath = {
  //    ProjectManager.ProjectPathStore.append(ProjectTypes.ARCHIVES)
  //  }

  private def prepareBackupName(filename : String) : String = {
    val format = new SimpleDateFormat(DATE_FORMAT_BACKUP).format(Calendar.getInstance.getTime)
    var ext = new Path(filename).getFileExtension
    if (ext != null) {
      if (!ext.isEmpty) {
        val extension = "." + ext
        filename.stripSuffix(extension) + "-" + format + extension
      } else filename + "-" + format
    } else filename + "-" + format
  }

  private def backupProject : Unit = {
    val project = EFS.getLocalFileSystem.getStore(ProjectManager.ProjectPath)
    val bkProjectName = prepareBackupName(ProjectManager.ProjectPath.lastSegment)
    val bkProjectstore = EFS.getLocalFileSystem.getStore(backupsStore.append(bkProjectName))
    worker("backupProject", "backupProject") { monitor : IProgressMonitor =>
      project.copy(bkProjectstore, EFS.OVERWRITE, monitor)
      Status.OK_STATUS
    }

  }

  def archiveProject : Unit = {
    worker("archiveProject", "archiveProject") { monitor : IProgressMonitor =>
      val name = ProjectManager.ProjectPathParentStore.lastSegment
      val nameArchive = prepareBackupName(name)
      Compression.archiveToZip(
        List(
          ProjectManager.ProjectPathStore.toOSString,
          ProjectManager.ProjectPathParentStore.append(ProjectTypes.APPINDEX_CONTAINER).toOSString
        ),
        ProjectManager.ProjectPathParentStore.append(ProjectTypes.ARCHIVES_CONTAINER).append(nameArchive).toOSString,
        name,
        ProjectTypes.ARCHIVE_EXTENSION
      )
      Status.OK_STATUS
    }
  }

  //  override def getAdapter(adapter : Class[_]) : AnyRef = {
  //    //    println(adapter)
  //    //    if (adapter == classOf[IPropertySheetPage])
  //    //      currentNode
  //    //    else
  //    //      null
  //    super.getAdapter(adapter)
  //  }

  private def pagesStore : IPath = {
    ProjectManager.ProjectPathStore.append(ProjectTypes.PAGES)
  }

  private def pageStore(page : String) : IPath = {
    if (page != null) {
      val pgStore = pagesStore.append(page)
      val store = EFS.getLocalFileSystem.getStore(pgStore)
      if (!store.fetchInfo.exists) store.mkdir(EFS.NONE, null)
      pgStore
    } else null
  }

  private def currentPageStore : IPath = {
    val pgStore = pagesStore.append(ProjectNodeTools.parentPage(currentNode).property(APRJNames.name).toString)
    val store = EFS.getLocalFileSystem.getStore(pgStore)
    if (!store.fetchInfo.exists) store.mkdir(EFS.NONE, null)
    pgStore
  }

  private def resolveProjectExtByFormat : String = {
    ProjectManager.dumpingFormat match {
      case RDFDumpingFormat.TURTLE     => ProjectTypes.PROJECT_FILE_EXT_TURTLE
      case RDFDumpingFormat.XML_ABBREV => ProjectTypes.PROJECT_FILE_EXT_XML_ABBREV
      case RDFDumpingFormat.NTRIPLE    => ProjectTypes.PROJECT_FILE_EXT_NTRIPLE
      case RDFDumpingFormat.N3         => ProjectTypes.PROJECT_FILE_EXT_N3
      case RDFDumpingFormat.XML        => ProjectTypes.PROJECT_FILE_EXT_XML
      case RDFDumpingFormat.RDF        => ProjectTypes.PROJECT_FILE_EXT_RDF
      case _                           => ProjectTypes.PROJECT_FILE_EXT_XML
    }
  }

  private def resolveProjectPathByType(projectPathURI : String) : Path = {
    var path : Path = null
    path = new Path(projectPathURI + ProjectTypes.PROJECT_FILE_EXT_TURTLE)
    if (path.toFile.exists) { ProjectManager.dumpingFormat = RDFDumpingFormat.TURTLE; path }
    else {
      path = new Path(projectPathURI + ProjectTypes.PROJECT_FILE_EXT_XML_ABBREV)
      if (path.toFile.exists) { ProjectManager.dumpingFormat = RDFDumpingFormat.XML_ABBREV; path }
      else {
        path = new Path(projectPathURI + ProjectTypes.PROJECT_FILE_EXT_NTRIPLE)
        if (path.toFile.exists) { ProjectManager.dumpingFormat = RDFDumpingFormat.NTRIPLE; path }
        else {
          path = new Path(projectPathURI + ProjectTypes.PROJECT_FILE_EXT_N3)
          if (path.toFile.exists) { ProjectManager.dumpingFormat = RDFDumpingFormat.N3; path }
          else {
            path = new Path(projectPathURI + ProjectTypes.PROJECT_FILE_EXT_XML)
            if (path.toFile.exists) { ProjectManager.dumpingFormat = RDFDumpingFormat.XML; path }
            else {
              path = new Path(projectPathURI + ProjectTypes.PROJECT_FILE_EXT_RDF)
              if (path.toFile.exists) { ProjectManager.dumpingFormat = RDFDumpingFormat.RDF; path }
              else null
            }
          }
        }
      }
    }
  }

  private def resolveProjectFilePath(projectPathURI : String, isNewProject : Boolean = false) : Unit = { //IPath = {

    val parentStore = new Path(projectPathURI)
    val projectStore = parentStore.append(ProjectTypes.APPDATA_CONTAINER)

    val fragmentPath = IPath.SEPARATOR + ProjectTypes.APPDATA_CONTAINER + IPath.SEPARATOR + ProjectTypes.PROJECT_FILE_NAME
    val ppath = if (projectPathURI != null) {
      if (isNewProject)
        new Path(projectPathURI + fragmentPath + resolveProjectExtByFormat)
      else
        resolveProjectPathByType(projectPathURI + fragmentPath)
    } else {
      if (isNewProject)
        ProjectManager.ProjectPath.append(fragmentPath).append(resolveProjectExtByFormat)
      //new Path(ProjectManager.ProjectPath, fragmentPath, resolveProjectExtByFormat)
      else
        resolveProjectPathByType(ProjectManager.ProjectPath.append(fragmentPath).toOSString)
    }

    if (ppath != null) {
      ProjectManager.ProjectPathParentStore = parentStore
      ProjectManager.ProjectPathStore = projectStore
      //ProjectManager.ProjectPathUri = URIUtil.toURI(ppath).toString
      ProjectManager.ProjectPath = ppath
    }

    ppath
  }

  private def deleteCurrentResourceDelayed : Unit = {
    removedResources += ProjectNodeTools.resourcePath(currentNode)
  }

  def deleteResources : Unit = {
    for (removed <- removedResources) {
      //TODO: remove cast when stable editor appear, make compiler happy
      val path = new Path(removed.asInstanceOf[String]) //
      val store = EFS.getLocalFileSystem.getStore(path)
      if (store.fetchInfo.exists) store.delete(EFS.NONE, null)
      removedResources -= removed
    }
  }

  def removeCurrentNode : Unit = {
    deleteCurrentResourceDelayed
    val parent = currentNode.parent
    parent.removeChild(currentNode)
    currentNode.deChildFromAllReferenceParent
    currentNode = parent
    refresh
    setDirty(true)
  }

  def renameCurrentNode(name : String) : Unit = {
    currentNode.nodeType match {
      case ProjectTypes.IMAGE_FILTER => {
        val path = ProjectNodeTools.resourcePath(currentNode)
        val store = new File(path) //EFS.getLocalFileSystem.getStore(path)
        val nName = if (name.indexOf(".rb") > -1) name else name + ".rb"
        store.renameTo(new File(store.getParent, nName))
        currentNode.rename(nName)
      }
      case _ => currentNode.rename(name)
    }
    refresh
    setDirty(true)
  }

  def currentNodeAsParent : TreeParent = {
    currentNode.asInstanceOf[TreeParent]
  }

  private def currentElementStore(storeName : String) : IFileStore = {
    val elementStore = currentPageStore.append(storeName)
    val store = EFS.getLocalFileSystem.getStore(elementStore)
    if (!store.fetchInfo.exists) store.mkdir(EFS.NONE, null)
    store
  }

  private def copyEmptyFileResourceToProject(
    resourceType : String,
    //name : String = null,
    data : Map[String, String] = null,
    additionalData : Map[String, String] = null) : Map[String, String] = {
    val unique = UUID.randomUUID.toString
    val extension = resourceType
    val fileUniqueName = unique + "." + extension
    val elementStore = currentElementStore(ProjectTypes.RESOLVE_PARENT(resourceType))
    val elementPath = elementStore.getChild(fileUniqueName)
    val input = elementPath.openOutputStream(EFS.NONE, null)

    val content =
      resourceType match {

        case ProjectTypes.SCRIPT  => "\nh1. main title: <" + resourceType + ">\n"
        case ProjectTypes.SUMMARY => "\nh1. main title: <" + resourceType + ">\n"
        case ProjectTypes.PERSON => {
          val personModel = new PersonRdfModel
          //val name = if (data != null && data.contains(APRJNames.title)) data(APRJNames.title) else resourceType
          //val name = fileUniqueName
          personModel.createPerson(fileUniqueName, data)
          personModel.dump
        }
        case ProjectTypes.LOCATION => {
          val locationModel = new LocationRdfModel
          //val name = if (data != null && data.contains(APRJNames.title)) data(APRJNames.title) else resourceType
          locationModel.createLocation(fileUniqueName, data)
          locationModel.dump
        }
        case ProjectTypes.NOTE    => "\nh1. main title: <" + resourceType + ">\n"
        case ProjectTypes.TAG     => "\nh1. main title: <" + resourceType + ">\n"
        case ProjectTypes.COMMENT => "\nh1. main title: <" + resourceType + ">\n"

        case _                    => ""
      }

    //val content = "\nh1. main title: <"+ resourceType +">\n"

    input.write(content.getBytes)
    input.close
    if (elementPath.fetchInfo.exists) {
      Map(
        TreeObjectProperties.UUID -> unique,
        TreeObjectProperties.FILE_EXT -> extension)
    } else null
  }

  private def resolveSubStore(store : IFileStore, substoreType : String) : IFileStore = {
    val subStorePath = new Path(store.toString).append(substoreType)
    val subStore = EFS.getLocalFileSystem.getStore(subStorePath)
    if (!subStore.fetchInfo.exists) subStore.mkdir(EFS.NONE, null)
    subStore
  }

  private def imageScaleAndSave(image : MagickImage, sideSize : Double, resolution : Double, filepath : String) : Boolean = {
    var newImage : MagickImage = null
    val originalDimension = image.getDimension
    if (sideSize != 100.0) {
      var width = (originalDimension.width * sideSize).asInstanceOf[Int]
      var height = (originalDimension.height * sideSize).asInstanceOf[Int]
      newImage = image.sampleImage(width, height)
    } else {
      newImage = image
    }
    if (resolution > 0.0) {
      newImage.setXResolution(resolution)
      newImage.setYResolution(resolution)
    }
    newImage.setFileName(filepath)
    val imageInfo = new ImageInfo
    imageInfo.setQuality(9)
    newImage.writeImage(imageInfo)
  }

  private def convertImageToWorkingSet(storePath : String, imageName : String) : Unit = {
    val imagePath = new Path(storePath).append(imageName)
    val imageScaledPath = imagePath.removeFileExtension.toOSString
    val imagePathString = imagePath.toOSString
    val imgeInfo = new ImageInfo(imagePathString)
    val image = new MagickImage(imgeInfo)

    imageScaleAndSave(image, ImageEditorImage.SMALL_SIZE, ImageEditorImage.BASIC_RESOLUTION,
      imageScaledPath + ImageEditorImage.SMALL_TAG + ImageEditorImage.DEFAULT_EXT_AND_DOT)

    imageScaleAndSave(image, ImageEditorImage.MEDIUM_SIZE, ImageEditorImage.BASIC_RESOLUTION,
      imageScaledPath + ImageEditorImage.MEDIUM_TAG + ImageEditorImage.DEFAULT_EXT_AND_DOT)

    imageScaleAndSave(image, ImageEditorImage.LARGE_SIZE, ImageEditorImage.BASIC_RESOLUTION,
      imageScaledPath + ImageEditorImage.LARGE_TAG + ImageEditorImage.DEFAULT_EXT_AND_DOT)

    imageScaleAndSave(image, ImageEditorImage.DEFAULT_SIZE, ImageEditorImage.DEFAULT_RESOLUTION,
      imageScaledPath + ImageEditorImage.DEFAULT_TAG + ImageEditorImage.DEFAULT_EXT_AND_DOT)
  }

  private def copyImageToProject(filePathName : String) : Map[String, String] = {
    val filePath = new Path(filePathName)
    val unique = UUID.randomUUID.toString
    val fileUniqueName = unique + "." + filePath.getFileExtension
    val fileStore = EFS.getLocalFileSystem.getStore(filePath)
    val elementStore = currentElementStore(ProjectTypes.IMAGES)

    /* filters path */
    resolveSubStore(elementStore, ProjectTypes.FILTERS)

    val elementPath = elementStore.getChild(fileUniqueName)
    fileStore.copy(elementPath, EFS.OVERWRITE, null)
    if (elementPath.fetchInfo.exists) {
      convertImageToWorkingSet(elementStore.toString, fileUniqueName)
      Map(
        TreeObjectProperties.UUID -> unique,
        TreeObjectProperties.FILE_EXT -> filePath.getFileExtension)
    } else null
  }

  def addImageToPage(filePath : String, data : Map[String, String]) : Unit = {
    val page = currentNodeAsParent
    if (page.nodeType == ProjectTypes.IMAGES) {
      val copied = copyImageToProject(filePath)
      if (copied != null) {
        val image = new TreeObject(data(APRJNames.title), ProjectTypes.IMAGE)
        image.store = data
        /*TODO: test filter */
        image.uniqueName = copied(TreeObjectProperties.UUID)
        image.property(ProjectTypes.FILTERS, List[String]())
        image.property(APRJNames.name, copied(TreeObjectProperties.UUID) + "." + copied(TreeObjectProperties.FILE_EXT))
        page.addChild(image)
      }
      //dumpProjectResourceToFile(ProjectManager.ProjectPathStore, null)
      refresh
      setDirty(true)
    }
  }

  private def referenceResource(resource : TreeObject) : TreeObject = {
    resource.nodeType match {
      case ProjectTypes.IMAGE    => projectStructure(ProjectTypes.IMAGES).referenceChild(resource)
      case ProjectTypes.SCRIPT   => projectStructure(ProjectTypes.SCRIPTS).referenceChild(resource)
      case ProjectTypes.LOCATION => projectStructure(ProjectTypes.LOCATIONS).referenceChild(resource)
      case ProjectTypes.PERSON   => projectStructure(ProjectTypes.PERSONS).referenceChild(resource)
      case ProjectTypes.SUMMARY  => projectStructure(ProjectTypes.SUMMARIES).referenceChild(resource)
      case ProjectTypes.NOTE     => projectStructure(ProjectTypes.NOTES).referenceChild(resource)
      case ProjectTypes.COMMENT  => projectStructure(ProjectTypes.COMMENTS).referenceChild(resource)
      case ProjectTypes.TAG      => projectStructure(ProjectTypes.TAGS).referenceChild(resource)
      case _                     => null
    }
  }

  def addResourceToPage(resourceType : String, data : Map[String, String] = null, additionalData : Map[String, String] = null) : Unit = {
    val page = currentNodeAsParent
    val copied = copyEmptyFileResourceToProject(resourceType, data, additionalData)
    val resource = new TreeObject(data(APRJNames.title), resourceType)

    resource.store = data
    resource.uniqueName = copied(TreeObjectProperties.UUID)

    resource.property(APRJNames.name, copied(TreeObjectProperties.UUID) + "." + copied(TreeObjectProperties.FILE_EXT))
    page.addChild(resource)
    referenceResource(resource)
    //dumpProjectResourceToFile(ProjectManager.ProjectPathStore, null)
    refresh
    setDirty(true)
  }

  def addPageToProject(data : Map[String, String]) : Unit = {
    val pageStructure = addBasicPageStructure(data)
    val page = pageStructure(ProjectTypes.PAGE)
    val pages = projectStructure(ProjectTypes.PAGES)
    if (!page.propertyExist(APRJNames.name))
      page.property(APRJNames.name, page.uniqueName)
    pageStore(page.property(APRJNames.name).toString)
    pages.addChild(pageStructure(ProjectTypes.PAGE))
    //dumpProjectResourceToFile(ProjectManager.ProjectPathStore, null)
    refresh
    setDirty(true)
  }

  private def refresh : Unit = {
    viewer.asInstanceOf[StructuredViewer].refresh(true)
  }

  private def addBasicPageStructure(pageData : Map[String, Any]) : Map[String, TreeParent] = {
    val pageNode = new TreeParent(pageData(APRJNames.title).toString, ProjectTypes.PAGE)
    pageNode.store = pageData

    val images = new TreeParent(ProjectTypes.IMAGES, ProjectTypes.IMAGES)
    val scripts = new TreeParent(ProjectTypes.SCRIPTS, ProjectTypes.SCRIPTS)
    val summaries = new TreeParent(ProjectTypes.SUMMARIES, ProjectTypes.SUMMARIES)
    val persons = new TreeParent(ProjectTypes.PERSONS, ProjectTypes.PERSONS)
    val locations = new TreeParent(ProjectTypes.LOCATIONS, ProjectTypes.LOCATIONS)
    val notes = new TreeParent(ProjectTypes.NOTES, ProjectTypes.NOTES)
    val tags = new TreeParent(ProjectTypes.TAGS, ProjectTypes.TAGS)
    val comments = new TreeParent(ProjectTypes.COMMENTS, ProjectTypes.COMMENTS)
    pageNode.addChild(images)
    pageNode.addChild(scripts)
    pageNode.addChild(summaries)
    pageNode.addChild(persons)
    pageNode.addChild(locations)
    pageNode.addChild(notes)
    pageNode.addChild(tags)
    pageNode.addChild(comments)
    Map(ProjectTypes.PAGE -> pageNode,
      ProjectTypes.IMAGES -> images,
      ProjectTypes.SCRIPTS -> scripts,
      ProjectTypes.SUMMARIES -> summaries,
      ProjectTypes.PERSONS -> persons,
      ProjectTypes.LOCATIONS -> locations,
      ProjectTypes.NOTES -> notes,
      ProjectTypes.TAGS -> tags,
      ProjectTypes.COMMENTS -> comments)
  }

  private def addBasicProjectStructure(projectData : Map[String, Any]) : Map[String, TreeParent] = {

    val appDataNode = new TreeParent(ProjectTypes.APPDATA_CONTAINER, ProjectTypes.APPDATA_CONTAINER)
    ProjectManager.projectRoot = new TreeParent(projectData(APRJNames.title).toString, ProjectTypes.PROJECT)

    ProjectManager.projectRoot.store = projectData
    ProjectManager.projectRoot.uniqueName
    ProjectManager.projectRoot.property(TreeObjectProperties.PROJECT_PATH_URI, ProjectManager.ProjectPath)
    currentNode = ProjectManager.projectRoot

    pagesContainer = new TreeParent(ProjectTypes.PAGES, ProjectTypes.PAGES)
    filtersContainer = new TreeParent(ProjectTypes.FILTERS, ProjectTypes.FILTERS)

    scriptsContainer = new TreeParent(ProjectTypes.SCRIPTS, ProjectTypes.SCRIPTS)
    scriptsContainer.subTypes = ProjectTypes.SUBTYPE_REFERENCE
    scriptsContainer.childrenIsDumpable = false

    personsContainer = new TreeParent(ProjectTypes.PERSONS, ProjectTypes.PERSONS)
    personsContainer.subTypes = ProjectTypes.SUBTYPE_REFERENCE
    personsContainer.childrenIsDumpable = false

    locationsContainer = new TreeParent(ProjectTypes.LOCATIONS, ProjectTypes.LOCATIONS)
    locationsContainer.subTypes = ProjectTypes.SUBTYPE_REFERENCE
    locationsContainer.childrenIsDumpable = false

    summariesContainer = new TreeParent(ProjectTypes.SUMMARIES, ProjectTypes.SUMMARIES)
    summariesContainer.subTypes = ProjectTypes.SUBTYPE_REFERENCE
    summariesContainer.childrenIsDumpable = false

    notesContainer = new TreeParent(ProjectTypes.NOTES, ProjectTypes.NOTES)
    notesContainer.subTypes = ProjectTypes.SUBTYPE_REFERENCE
    notesContainer.childrenIsDumpable = false

    tagsContainer = new TreeParent(ProjectTypes.TAGS, ProjectTypes.TAGS)
    tagsContainer.subTypes = ProjectTypes.SUBTYPE_REFERENCE
    tagsContainer.childrenIsDumpable = false

    commentsContainer = new TreeParent(ProjectTypes.COMMENTS, ProjectTypes.COMMENTS)
    commentsContainer.subTypes = ProjectTypes.SUBTYPE_REFERENCE
    commentsContainer.childrenIsDumpable = false

    appDataNode.addChild(ProjectManager.projectRoot)
    ProjectManager.projectRoot.addChild(pagesContainer)
    ProjectManager.projectRoot.addChild(scriptsContainer)
    ProjectManager.projectRoot.addChild(summariesContainer)
    ProjectManager.projectRoot.addChild(personsContainer)
    ProjectManager.projectRoot.addChild(locationsContainer)
    ProjectManager.projectRoot.addChild(notesContainer)
    ProjectManager.projectRoot.addChild(tagsContainer)
    ProjectManager.projectRoot.addChild(commentsContainer)
    ProjectManager.projectRoot.addChild(filtersContainer)
    Map(ProjectTypes.ROOT -> appDataNode,
      ProjectTypes.PROJECT -> ProjectManager.projectRoot,
      ProjectTypes.PAGES -> pagesContainer,
      ProjectTypes.SCRIPTS -> scriptsContainer,
      ProjectTypes.FILTERS -> filtersContainer,
      ProjectTypes.SUMMARIES -> summariesContainer,
      ProjectTypes.PERSONS -> personsContainer,
      ProjectTypes.LOCATIONS -> locationsContainer,
      ProjectTypes.NOTES -> notesContainer,
      ProjectTypes.TAGS -> tagsContainer,
      ProjectTypes.COMMENTS -> commentsContainer)
  }

  def dumpProjectResourceToRdf(node : TreeObject, projectModel : ProjectRdfModel, resource : Resource) : ProjectRdfModel = {
    val anaProject = if (projectModel == null) new ProjectRdfModel else projectModel
    var currentResource : Resource = resource
    if (ProjectNodeTools.isTreeParent(node)) {
      for (
        child <- node.asInstanceOf[TreeParent].getChildren if (node.asInstanceOf[TreeParent].hasChildren);
        if (node.asInstanceOf[TreeParent].childrenIsDumpable)
      ) {
        node.nodeType match {
          /* assume first node as Project */
          case ProjectTypes.PROJECT => {
            anaProject.createProject(node.name, node.store)
          }
          case ProjectTypes.PAGE => {
            currentResource = anaProject.createPage(node.name, node.store)
          }
          case _ => {}
        }
        dumpProjectResourceToRdf(child, anaProject, currentResource)
      }
    } else {
      if (node.parent != null && node.parent.childrenIsDumpable) {
        Activator.Logger.debug(node.nodeType + " - " + node.name)
        node.nodeType match {
          case ProjectTypes.IMAGE    => { anaProject.addImageToPage(node.name, node.store, currentResource) }
          case ProjectTypes.SCRIPT   => { anaProject.addScriptToPage(node.name, node.store, currentResource) }
          case ProjectTypes.SUMMARY  => { anaProject.addSummaryToPage(node.name, node.store, currentResource) }
          case ProjectTypes.PERSON   => { anaProject.addPersonToPage(node.name, node.store, currentResource) }
          case ProjectTypes.LOCATION => { anaProject.addLocationToPage(node.name, node.store, currentResource) }
          case ProjectTypes.NOTE     => { anaProject.addNoteToPage(node.name, node.store, currentResource) }
          case ProjectTypes.TAG      => { anaProject.addTagToPage(node.name, node.store, currentResource) }
          case ProjectTypes.COMMENT  => { anaProject.addCommentToPage(node.name, node.store, currentResource) }
          case _                     => {}
        }
      }
    }
    anaProject
  }

  private def refreshAndDirty(dirty : Boolean = false, workerName : String = "refreshAndDirty") : Unit = {
    workerUI(workerName) { monitor : IProgressMonitor =>
      setDirty(dirty)
      refresh
      Status.OK_STATUS
    }
  }

  private def refreshLoadAndDirty(tree : TreeObject, dirty : Boolean = true, workerName : String = "refreshLoadAndDirty") : Unit = {
    workerUI(workerName) { monitor : IProgressMonitor =>
      viewer.setInput(tree)
      setDirty(dirty)
      refresh
      Status.OK_STATUS
    }
  }

  def saveProject : Unit = {
    if (ProjectManager.ProjectPathStore != null) {
      worker("dumpProjectResourceToFile", "dumpProjectResourceToFile") { monitor : IProgressMonitor =>
        Perspective.projectManager.dumpProjectResourceToFile(ProjectManager.ProjectPathStore, null)
        refreshAndDirty(false, "saveProject")
        Status.OK_STATUS
      }
    }
  }

  def dumpProjectResourceToFile(pathStore : IPath, projectSource : String) : Unit = {
    if (isDirty) {
      deleteResources
      var projectSourceRdf = ""
      if (projectSource == null)
        projectSourceRdf = dumpProjectResourceToRdf(ProjectManager.projectRoot, null, null).dumpIn(ProjectManager.dumpingFormat)
      else
        projectSourceRdf = projectSource
      val openOutputStream = EFS.getLocalFileSystem.getStore(
        pathStore.append(ProjectTypes.PROJECT_FILE_NAME + resolveProjectExtByFormat)).openOutputStream(SWT.NONE, null)
      val outputStreamWriter = new OutputStreamWriter(openOutputStream)
      outputStreamWriter.write(projectSourceRdf, 0, projectSourceRdf.length)
      outputStreamWriter.close
      openOutputStream.close
      setDirty(false)
    }
  }

  private def closeAllOpenEditor : Unit = {
    val activePage = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getActivePage
    val editors = activePage.getEditorReferences
    editors foreach { editor =>
      activePage.closeEditor(editor.getEditor(false), true)
    }
  }

  def closeProject(force : Boolean = false) : Unit = {
    closeAllOpenEditor
    worker("closeProjectManager", "closeProjectManager") { monitor : IProgressMonitor =>
      if (isLoaded) {
        if (isDirty && force)
          dumpProjectResourceToFile(ProjectManager.ProjectPathStore, null)
        ProjectManager.projectRoot.parent.removeChildren
        ProjectManager.ProjectPathStore = null
        ProjectManager.ProjectPath = null
        ProjectManager.ProjectPathParentStore = null
        ProjectManager.ProjectPathStore = null
        ProjectManager.projectRoot = null
        ProjectManager.Project = null
      }
      refreshAndDirty(false, "closeProject")
      ProjectManagerStatusMessage.set(ProjectManagerStatusMessage.CLOSED)
      setStatusMessage
      Status.OK_STATUS
    }
  }

  def openProject(projectPathURI : String) : Unit = {
    if (projectPathURI != null) {
      worker("populateProjectManagerFromUri", "populateProjectManagerFromUri") { monitor : IProgressMonitor =>
        val proj = loadProjectFromUri(projectPathURI)
        refreshLoadAndDirty(proj, false, "populateProjectManagerFromUri")
        Status.OK_STATUS
      }
    }
  }

  private def readObjectNodeProperties : Unit = {
    val value = viewer.getLabelProvider.asInstanceOf[ViewLabelProvider].getText(currentNode)
    val currentPage = ProjectNodeTools.parentPage(currentNode)
    setStatusCurrentNodeMessage(currentNode.name)

    //readPropertyToView
  }

  private def openResourceByPath(partName : String, descr : String, resourcePath : String, lineNum : Int = 0) : Unit = {
    val path = new Path(resourcePath)
    val edId = ProjectManager.resolveEditorIdByExt(path.getFileExtension)
    val store = EFS.getLocalFileSystem.getStore(path)
    val workbenchPage = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getActivePage
    val editor = workbenchPage.openEditor(new FileStoreEditorInput(store), edId)

    if (editor.isInstanceOf[ImageEditor]) {
      editor.asInstanceOf[ImageEditor].setPartName(partName)
      editor.asInstanceOf[ImageEditor].projectNode = currentNode
      //editor.asInstanceOf[ImageEditor].setContentDescription(descr)
    } else if (editor.isInstanceOf[ScriptEditor]) {
      editor.asInstanceOf[ScriptEditor].setPartName(partName)
      editor.asInstanceOf[ScriptEditor].projectNode = currentNode
      //editor.asInstanceOf[ScriptEditor].setContentDescription(descr)
      if (lineNum > 0)
        editor.asInstanceOf[ScriptEditor].setLineSelection(lineNum)
    } else if (editor.isInstanceOf[PersonEditor]) {
      editor.asInstanceOf[PersonEditor].setPartName(partName)
      editor.asInstanceOf[PersonEditor].projectNode = currentNode
      //editor.asInstanceOf[StructuredEditor].setContentDescription(descr)
    } else if (editor.isInstanceOf[LocationEditor]) {
      editor.asInstanceOf[LocationEditor].setPartName(partName)
      editor.asInstanceOf[LocationEditor].projectNode = currentNode
      //editor.asInstanceOf[StructuredEditor].setContentDescription(descr)
    }
  }

  def openResource(node : TreeObject, lineNum : Int = 0) : Unit = {
    openResourceByPath(ProjectNodeTools.resourceTitle(node),
      ProjectNodeTools.resourceDescription(node),
      ProjectNodeTools.resourcePath(node), lineNum)
  }

  private def prepareEventListeners : Unit = {

    getSite.setSelectionProvider(viewer)

    viewer.addOpenListener(new IOpenListener {
      override def open(event : OpenEvent) : Unit = {
        if (!ProjectNodeTools.isTreeParent(currentNode)) {
          openResource(currentNode)
        }
      }
    })

    /* Selection on Item node */
    viewer.addSelectionChangedListener(new ISelectionChangedListener {
      override def selectionChanged(event : SelectionChangedEvent) : Unit = {
        if (event.getSelection.isEmpty) {
          Activator.Logger.warning("empty node")
          return
        }

        if (event.getSelection.isInstanceOf[IStructuredSelection]) {
          val selection = event.getSelection.asInstanceOf[IStructuredSelection]
          val selectionIterator = selection.iterator
          while (selectionIterator.hasNext) {
            currentNode = selectionIterator.next.asInstanceOf[TreeObject]
            val status = Perspective.actionBar.menuProject.reloadIconsForElements
            Perspective.actionBar.enableToolbarProjectElements(status)
            /* node selection event manager */
            readObjectNodeProperties

            val PropView = Perspective.getPropertyView
            if (PropView != null) {
              val page = PropView.getCurrentPage.asInstanceOf[PropertySheetPage]
              if (page != null) page.refresh
            }
            //page.selectionChanged(Perspective.projectManager, null)
          }
        }
      }
    })
  }

  def getLabelProvider : ViewLabelProvider = {
    viewer.getLabelProvider.asInstanceOf[ViewLabelProvider]
  }

  override def createPartControl(parent : Composite) : Unit = {
    viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER)
    viewer.setAutoExpandLevel(2)
    viewer.setContentProvider(new ViewContentProvider)
    viewer.setLabelProvider(new ViewLabelProvider)
    //getSite.setSelectionProvider(viewer)
    /*TODO: load project for test */
    //viewer.setInput(loadProjectFromUri("file:///home/nissl/develop/eclipse-workspace-plugins/org.pragmas.anacleto/work/new_project.rdf"))
    openProject(null)
    prepareEventListeners
  }

  override def setFocus : Unit = {
    viewer.getControl.setFocus
  }

  override def dispose : Unit = {
  }

  override def promptToSaveOnClose : Int = {
    ISaveablePart2.DEFAULT
  }

  override def isSaveAsAllowed : Boolean = { true }
  override def isSaveOnCloseNeeded : Boolean = { true }
  override def doSave(monitor : IProgressMonitor) : Unit = {
    saveProject
  }
  override def doSaveAs : Unit = {}

}
