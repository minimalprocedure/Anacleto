/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui

import org.eclipse.jface.action.Action
import org.eclipse.jface.action.IAction
import org.eclipse.jface.action.GroupMarker
import org.eclipse.jface.action.ICoolBarManager
import org.eclipse.jface.action.IMenuManager
//import org.eclipse.jface.action.IToolBarManager
//import org.eclipse.jface.action.MenuManager
//import org.eclipse.jface.action.Separator
import org.eclipse.jface.action.ToolBarContributionItem
import org.eclipse.jface.action.ToolBarManager
import org.eclipse.swt.SWT
import org.eclipse.ui.IWorkbenchActionConstants
import org.eclipse.ui.IWorkbenchWindow
//import org.eclipse.ui.actions.ActionFactory
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction
import org.eclipse.ui.application.ActionBarAdvisor
import org.eclipse.ui.application.IActionBarConfigurer
import org.pragmas.anacleto.commands._

class ApplicationActionBarAdvisor(val configurer : IActionBarConfigurer)
    extends ActionBarAdvisor(configurer) {

  Perspective.actionBar = this

  var menuFile : ApplicationFileMenuManager = _
  var menuEdit : ApplicationEditMenuManager = _
  var menuNavigate : ApplicationNavigateMenuManager = _
  var menuProject : ApplicationProjectMenuManager = _
  var menuWindow : ApplicationWindowMenuManager = _
  var menuAdditions : GroupMarker = _
  var menuAbout : ApplicationAboutMenuManager = _

  var menuBar : IMenuManager = _
  var coolBar : ICoolBarManager = _

  val toolbarFile : ToolBarManager = new ToolBarManager(SWT.FLAT | SWT.RIGHT)
  val toolbarUndoRedo : ToolBarManager = new ToolBarManager(SWT.FLAT | SWT.RIGHT)
  val toolbarEdit : ToolBarManager = new ToolBarManager(SWT.FLAT | SWT.RIGHT)
  val toolbarProject : ToolBarManager = new ToolBarManager(SWT.FLAT | SWT.RIGHT)
  val toolbarProjectElements : ToolBarManager = new ToolBarManager(SWT.FLAT | SWT.RIGHT)

  override def register(action : IAction) : Unit = {
    super.register(action)
  }

  override protected def makeActions(window : IWorkbenchWindow) : Unit = {
    Perspective.window = window
  }

  override protected def fillMenuBar(menuBar : IMenuManager) : Unit = {
    this.menuBar = menuBar
    menuFile = new ApplicationFileMenuManager
    menuEdit = new ApplicationEditMenuManager
    menuProject = new ApplicationProjectMenuManager
    menuWindow = new ApplicationWindowMenuManager
    menuNavigate = new ApplicationNavigateMenuManager
    //menuAdditions = new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS)
    //menuBar.add(menuAdditions)
    menuAbout = new ApplicationAboutMenuManager
  }

  override protected def fillCoolBar(coolBar : ICoolBarManager) : Unit = {
    this.coolBar = coolBar

    coolBar.add(new ToolBarContributionItem(toolbarFile, "toolbarFile"))
    coolBar.add(new ToolBarContributionItem(toolbarUndoRedo, "toolbarUndoRedo"))
    coolBar.add(new ToolBarContributionItem(toolbarEdit, "toolbarEdit"))
    coolBar.add(new ToolBarContributionItem(toolbarProject, "toolbarProject"))
    coolBar.add(new ToolBarContributionItem(toolbarProjectElements, "toolbarProjectElements"))

    toolbarFile.add(menuFile.actionFileSave)
    toolbarFile.add(menuFile.actionFileSaveAs)
    toolbarFile.add(menuFile.actionFileSaveAll)

    toolbarUndoRedo.add(menuEdit.actionEditUndo)
    toolbarUndoRedo.add(menuEdit.actionEditRedo)

    toolbarEdit.add(menuEdit.actionEditCut)
    toolbarEdit.add(menuEdit.actionEditCopy)
    toolbarEdit.add(menuEdit.actionEditPaste)

    toolbarProject.add(menuProject.actionProjectNew)
    toolbarProject.add(menuProject.actionProjectOpen)
    toolbarProject.add(menuProject.actionProjectSave)
    toolbarProject.add(menuProject.actionProjectClose)

    toolbarProjectElements.add(menuProject.actionProjectAdd)
    toolbarProjectElements.add(menuProject.actionProjectRemove)
    toolbarProjectElements.add(menuProject.actionProjectRename)

  }

  def enableToolbarProjectElements(status : Boolean) : Unit = {
    val toolbar = toolbarProjectElements.getControl
    if (toolbar != null) {
      for (item <- toolbar.getItems) item.setEnabled(status)
      toolbarProjectElements.update(true)
    }
  }

}
