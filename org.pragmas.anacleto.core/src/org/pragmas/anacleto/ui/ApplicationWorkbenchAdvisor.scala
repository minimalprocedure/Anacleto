/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui

import org.eclipse.ui.application.IWorkbenchConfigurer
import org.eclipse.ui.application.IWorkbenchWindowConfigurer
import org.eclipse.ui.application.WorkbenchAdvisor
import org.eclipse.ui.application.WorkbenchWindowAdvisor
import org.eclipse.jface.preference.IPreferenceStore

object ApplicationWorkbenchAdvisor {
  val ID = "org.pragmas.anacleto.ui.ApplicationWorkbench"
}

class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

  val ID = ApplicationWorkbenchAdvisor.ID

  override def createWorkbenchWindowAdvisor(configurer : IWorkbenchWindowConfigurer) : WorkbenchWindowAdvisor = {
    Perspective.configurer = configurer
    new ApplicationWorkbenchWindowAdvisor(configurer)
  }

  override def getInitialWindowPerspectiveId : String = {
    Perspective.ID
  }

  override def initialize(configurer : IWorkbenchConfigurer) : Unit = {
    super.initialize(configurer)
    Application.configurer = configurer
    //TODO: configurer.setSaveAndRestore(true)
    configurer.setSaveAndRestore(true)
  }
}
