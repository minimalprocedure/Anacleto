/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui

import org.eclipse.ui.views.properties.PropertySheet
import org.eclipse.ui.IViewPart
import org.eclipse.ui.IPageLayout
import org.eclipse.ui.IFolderLayout
import org.eclipse.ui.IPerspectiveFactory
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.IWorkbenchPage
import org.eclipse.core.filesystem._
import org.pragmas.anacleto.ui.views.ProjectManager
import org.eclipse.ui.views.contentoutline._
import org.eclipse.ui.application.IWorkbenchWindowConfigurer
import org.eclipse.jface.preference.IPreferenceStore
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.jface.action.IStatusLineManager
import org.eclipse.jface.action._
//import org.osgi.framework.Bundle
import org.eclipse.ui.IPageLayout

object Perspective {
  def ID = "org.pragmas.anacleto.ui.perspective"

  //val ID_PROPERTY_SHEET = "org.pragmas.anacleto.ui.PropertySheetPages.ApplicationPropertySheetsPage"
  val ID_SEARCH_VIEW = "org.pragmas.anacleto.search.ui.views.SearchView"
  val MACROS_MANAGER_ID = "org.pragmas.anacleto.search.ui.views.MacrosManagerView"

  var topLeft : IFolderLayout = _
  var bottomLeft : IFolderLayout = _
  var bottomEditor : IFolderLayout = _
  var topRight : IFolderLayout = _
  var editorArea : String = _

  var projectManager : ProjectManager = _

  var actionBar : ApplicationActionBarAdvisor = _
  var statusLineManager : IStatusLineManager = _
  var windowAdvisor : ApplicationWorkbenchWindowAdvisor = _
  var window : IWorkbenchWindow = _
  var configurer : IWorkbenchWindowConfigurer = _
  var preferenceStore : IPreferenceStore = _

  def getActivePage : IWorkbenchPage = {
    PlatformUI.getWorkbench.getActiveWorkbenchWindow.getActivePage
  }

  def getPropertyView : PropertySheet = {
    getActivePage.findView(IPageLayout.ID_PROP_SHEET).asInstanceOf[PropertySheet]
  }
}

class Perspective extends IPerspectiveFactory {

  def ID = Perspective.ID

  override def createInitialLayout(layout : IPageLayout) : Unit = {
    layout.setEditorAreaVisible(true)
    Perspective.editorArea = layout.getEditorArea

    Perspective.topLeft =
      layout.createFolder("topLeft", IPageLayout.LEFT,
        IPageLayout.DEFAULT_FASTVIEW_RATIO,
        Perspective.editorArea)
    //    Perspective.bottomLeft =
    //      layout.createFolder("bottomLeft", IPageLayout.BOTTOM,
    //        IPageLayout.RATIO_MAX - IPageLayout.DEFAULT_FASTVIEW_RATIO,
    //        "topLeft")
    Perspective.topLeft.addPlaceholder(ProjectManager.ID + ":*")
    Perspective.topLeft.addView(ProjectManager.ID)
    layout.getViewLayout(ProjectManager.ID).setCloseable(false)

    Perspective.bottomEditor =
      layout.createFolder("bottomEditor", IPageLayout.BOTTOM,
        IPageLayout.RATIO_MAX - IPageLayout.DEFAULT_FASTVIEW_RATIO,
        Perspective.editorArea)

    Perspective.bottomEditor.addPlaceholder(Perspective.ID_SEARCH_VIEW + ":*")
    Perspective.bottomEditor.addView(Perspective.ID_SEARCH_VIEW)

    Perspective.bottomEditor.addPlaceholder(Perspective.MACROS_MANAGER_ID + ":*")
    Perspective.bottomEditor.addView(Perspective.MACROS_MANAGER_ID)

    Perspective.bottomEditor.addView(IPageLayout.ID_PROP_SHEET)
    Perspective.bottomEditor.addPlaceholder(IPageLayout.ID_PROP_SHEET + ":*")

    Perspective.topRight =
      layout.createFolder("topRight", IPageLayout.RIGHT,
        IPageLayout.RATIO_MAX - IPageLayout.DEFAULT_FASTVIEW_RATIO,
        Perspective.editorArea)
    Perspective.topRight.addPlaceholder(IPageLayout.ID_OUTLINE + ":*")
    Perspective.topRight.addView(IPageLayout.ID_OUTLINE)

  }
}
