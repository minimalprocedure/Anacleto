/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.dialogs

import org.eclipse.jface.wizard.Wizard
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets._
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout._
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.layout.RowLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Group
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.jface.wizard.WizardPage
import org.eclipse.swt.widgets.Shell
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.runtime.Path
import org.eclipse.core.filesystem.URIUtil
import org.pragmas.anacleto.ProjectResources.ProjectTypes
import org.pragmas.anacleto.ui.views.ProjectManager
import org.pragmas.anacleto.models._
import org.pragmas.anacleto.ui.editors.ImageEditor
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.extensions.ExtensionType

class AddScriptletWizardPage(scriptletType : String)
  extends WizardPage("org.pragmas.anacleto.ui.dialogs.AddScriptletWizardPage") {

  var scriptletScriptName : Text = _
  var scriptletScriptClass : Text = _
  var scriptletScriptSuperClass : Text = _
  var scriptletScriptRetType : Text = _
  var scriptletScriptVersion : Text = _
  var scriptletCreator : Text = _
  var scriptletDescription : Text = _
  var pageContainer : Composite = _

  var scriptletScriptClassText : String = _
  var scriptletScriptSuperClassText : String = _
  var scriptletScriptRetTypeText : String = _
  var scriptletScriptRetVariableText : String = _
  var scriptletScriptMethodBodyText : String = ""
  var scriptletScriptVersionText : String = "1.0"

  var scriptletSkeleton = """
=begin

@extensionType %1$s
@returnType %3$s
@description %4$s
@author %5$s
@version %6$s

=end

class %1$s < %2$s
    def execute
      %7$s
      %8$s
    end
end
"""

  def buildScriptletSkeleton : String = {
    scriptletSkeleton.format(
      scriptletScriptClass.getText,
      scriptletScriptSuperClassText,
      scriptletScriptRetTypeText,
      scriptletDescription.getText,
      scriptletCreator.getText,
      scriptletScriptVersion.getText,
      scriptletScriptMethodBodyText,
      scriptletScriptRetVariableText)
  }

  //var scriptletSelectSubType : Combo = _

  scriptletType match {
    case ExtensionType.IMAGE_MAGICK_MANIPULATOR => {
      scriptletScriptClassText = "ImageMagickFilter"
      scriptletScriptSuperClassText = "ImageFilter"
      scriptletScriptRetTypeText = "MagickImage"
      scriptletScriptRetVariableText = "image"
    }
    case _ => {
      scriptletScriptClassText = "ImageMagickFilter"
      scriptletScriptSuperClassText = "ImageFilter"
      scriptletScriptRetTypeText = "MagickImage"
      scriptletScriptRetVariableText = "image"
    }
  }

  setTitle("Add Scriptlet: " + scriptletType)
  setDescription("Specify scriptlet settings")

  override def createControl(parent : Composite) : Unit = {
    this.pageContainer = new Composite(parent, SWT.NONE)
    val layout = new RowLayout
    layout.`type` = SWT.VERTICAL
    this.pageContainer.setLayout(layout)
    new Label(this.pageContainer, SWT.NONE).setText("Scriptlet settings")

    initFirstGroup
    initInformationsGroup
    setControl(this.pageContainer)
    this.pageContainer.getShell.pack
  }

  private def fileSelectionChoice(filterExtensions : Map[String, String]) : String = {
    var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
    val dialog = new FileDialog(shell, SWT.OPEN)
    val extensions = filterExtensions.keys.toList.toArray
    val extensionNames = filterExtensions.values.toList.toArray
    dialog.setFilterExtensions(extensions)
    dialog.setFilterNames(extensionNames)
    val fileSelected = dialog.open
    if (fileSelected != null) fileSelected else null
  }

  private def initFirstGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 4

    val dataGridTitle = new GridData
    dataGridTitle.widthHint = 240
    dataGridTitle.heightHint = 16
    dataGridTitle.horizontalAlignment = GridData.FILL
    dataGridTitle.horizontalSpan = 2

    val data = new GridData
    data.widthHint = 240
    data.heightHint = 16
    data.horizontalAlignment = GridData.FILL
    data.horizontalSpan = 1

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Scriptlet:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("Name")
    scriptletScriptName = new Text(group, SWT.BORDER)
    scriptletScriptName.setLayoutData(dataGridTitle)
    scriptletScriptName.setText(scriptletScriptClassText)
    new Label(group, SWT.NONE).setText("")

    new Label(group, SWT.NONE).setText("Class")
    scriptletScriptClass = new Text(group, SWT.BORDER)
    scriptletScriptClass.setLayoutData(dataGridTitle)
    scriptletScriptClass.setText(scriptletScriptClassText)
    new Label(group, SWT.NONE).setText("")

    //new Label(group, SWT.NONE).setText("Return type")
    //scriptletScriptRetType = new Text(group, SWT.BORDER)
    //scriptletScriptRetType.setLayoutData(dataGridTitle)
    //scriptletScriptRetType.setText("add_" + scriptletType)
    //new Label(group, SWT.NONE).setText("")

    new Label(group, SWT.NONE).setText("Version")
    scriptletScriptVersion = new Text(group, SWT.BORDER)
    scriptletScriptVersion.setLayoutData(dataGridTitle)
    scriptletScriptVersion.setText(scriptletScriptVersionText)
    new Label(group, SWT.NONE).setText("")
  }

  private def initInformationsGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2
    val data = new GridData
    data.widthHint = 240
    data.heightHint = 16

    val dataDescription = new GridData
    dataDescription.widthHint = 244
    dataDescription.heightHint = 100

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Informations:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("Description")
    scriptletDescription = new Text(group, SWT.BORDER)
    scriptletDescription.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Creator")
    scriptletCreator = new Text(group, SWT.BORDER)
    scriptletCreator.setLayoutData(data)
  }

}

class AddScriptletWizard(scriptletType : String) extends Wizard {

  val addScriptletPage = new AddScriptletWizardPage(scriptletType)

  private def computeData : Map[String, String] = {
    //    scriptletType match {
    //      case ProjectTypes.TAG => Map(APRJNames.subtypes ->  addScriptletPage.scriptletSelectSubType.getText)
    //      case _ => Map()
    //    }
    null
  }

  override def performCancel : Boolean = {
    println("cancel adding scriptlet")
    true
  }

  override def addPages : Unit = {
    addPage(addScriptletPage)
  }

  override def performFinish : Boolean = {
    println("creating scriptlet")
    val source = addScriptletPage.buildScriptletSkeleton
    Perspective.projectManager.addImageFilter(addScriptletPage.scriptletScriptName.getText, "rb", source)
    true
  }

  override def dispose : Unit = { super.dispose }
}
