/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.dialogs

import org.eclipse.jface.wizard.Wizard
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets._
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout._
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.layout.RowLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Group
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.jface.wizard.WizardPage
import org.eclipse.swt.widgets.Shell
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.runtime.Path
import org.eclipse.core.filesystem.URIUtil
import org.pragmas.anacleto.ProjectResources.ProjectTypes
import org.pragmas.anacleto.ui.views.ProjectManager
import org.pragmas.anacleto.models._
import org.pragmas.anacleto.ui.editors.ImageEditor
import org.pragmas.anacleto.ui.Perspective

class AddImageWizardPage
extends WizardPage("org.pragmas.anacleto.ui.dialogs.AddImageWizardPage") {

  var imageLocation : Text = _
  //var imageName : Text = _
  var buttonSearchLocation : Button = _
  var imageTitle : Text = _
  var imageSubject : Text = _
  var imageCreator : Text = _
  var imageDescription : Text = _
  var pageContainer : Composite = _

  setTitle("Add Image")
  setDescription("Specify image settings")

  override def createControl(parent : Composite) : Unit = {
    this.pageContainer = new Composite(parent, SWT.NONE)
    val layout = new RowLayout
    layout.`type` = SWT.VERTICAL
    this.pageContainer.setLayout(layout)
    new Label(this.pageContainer, SWT.NONE).setText("Image settings")

    initFirstGroup
    initInformationsGroup
    setControl(this.pageContainer)
    this.pageContainer.getShell.pack
  }

  private def fileSelectionChoice(filterExtensions : Map[String, String]) : String = {
    var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
    val dialog = new FileDialog(shell, SWT.OPEN)
    val extensions = filterExtensions.keys.toList.toArray
    val extensionNames = filterExtensions.values.toList.toArray
    dialog.setFilterExtensions(extensions)
    dialog.setFilterNames(extensionNames)
    val fileSelected = dialog.open
    if (fileSelected != null) fileSelected else null
  }

  private def initFirstGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 4

    val dataGridTitle = new GridData
    dataGridTitle.widthHint = 240
    dataGridTitle.heightHint = 16
    dataGridTitle.horizontalAlignment = GridData.FILL
    dataGridTitle.horizontalSpan = 2

    val data = new GridData
    data.widthHint = 240
    data.heightHint = 16
    data.horizontalAlignment = GridData.FILL
    data.horizontalSpan = 1

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Image:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("Title")
    imageTitle = new Text(group, SWT.BORDER)
    imageTitle.setLayoutData(dataGridTitle)
    imageTitle.setText("add_image")
    new Label(group, SWT.NONE).setText("")

    new Label(group, SWT.NONE).setText("Location")
    imageLocation = new Text(group, SWT.BORDER)
    imageLocation.setLayoutData(data)

    buttonSearchLocation = new Button(group, SWT.BORDER)
    buttonSearchLocation.setText("search")
    buttonSearchLocation.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e : SelectionEvent) {
        var shell = pageContainer.getShell
        val filePath = fileSelectionChoice(ImageEditor.Extensions)
        if (filePath != null) {
          imageLocation.setText(filePath)
        }
      }
    })
  }

  private def initInformationsGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2
    val data = new GridData
    data.widthHint = 240
    data.heightHint = 16

    val dataDescription = new GridData
    dataDescription.widthHint = 244
    dataDescription.heightHint = 100

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Informations:")
    group.setLayout(layout)

    //new Label(group, SWT.NONE).setText("Title")
    //imageTitle = new Text(group, SWT.BORDER)
    //imageTitle.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Subject")
    imageSubject = new Text(group, SWT.BORDER)
    imageSubject.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Creator")
    imageCreator = new Text(group, SWT.BORDER)
    imageCreator.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Description")
    imageDescription = new Text(group, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL)
    imageDescription.setLayoutData(dataDescription)
  }

}

class AddImageWizard extends Wizard {

  val addImagePage = new AddImageWizardPage

  override def performCancel : Boolean = {
    println("cancel adding image")
    true
  }

  override def addPages : Unit = {
    addPage(addImagePage)
  }

  override def performFinish : Boolean = {
    println("creating image")
    val imageLocation = addImagePage.imageLocation.getText
    val imageTitle = addImagePage.imageTitle.getText
    if(!imageLocation.isEmpty && !imageTitle.isEmpty ){
      val data = Map(
        //APRJNames.name -> addImagePage.imageName.getText,
        APRJNames.creator -> addImagePage.imageCreator.getText,
        APRJNames.description -> addImagePage.imageDescription.getText,
        APRJNames.subject -> addImagePage.imageSubject.getText,
        APRJNames.title -> imageTitle
      )
      Perspective.projectManager.addImageToPage(imageLocation, data)
      true
    } else false
  }

  override def dispose : Unit = {super.dispose}
}
