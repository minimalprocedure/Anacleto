/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.dialogs

import java.util.UUID
import org.eclipse.jface.wizard.Wizard
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets._
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout._
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.layout.RowLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Group
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.jface.wizard.WizardPage
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.runtime.Path
import org.eclipse.core.filesystem.URIUtil
import org.pragmas.anacleto.ProjectResources.ProjectTypes
import org.pragmas.anacleto.ui.views.ProjectManager
import org.pragmas.anacleto.models._
import org.pragmas.anacleto.ui.Perspective


class NewPageWizardPage
extends WizardPage("org.pragmas.anacleto.ui.dialogs.NewPageWizardPage") {

  var pageLocation : Text = _
  //var pageName : Text = _
  var buttonSearchLocation : Button = _
  var pageTitle : Text = _
  var pageSubject : Text = _
  var pageCreator : Text = _
  var pageDescription : Text = _
  var pageContainer : Composite = _

  setTitle("New Page")
  setDescription("Specify new page creation settings")

  override def createControl(parent : Composite) : Unit = {
    this.pageContainer = new Composite(parent, SWT.NONE)
    val layout = new RowLayout
    layout.`type` = SWT.VERTICAL
    this.pageContainer.setLayout(layout)
    new Label(this.pageContainer, SWT.NONE).setText("Page settings")

    initFirstGroup
    initInformationsGroup
    setControl(this.pageContainer)
    this.pageContainer.getShell.pack
  }

  private def initFirstGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2

    val data = new GridData
    data.widthHint = 300
    data.heightHint = 16

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Project:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("Title")
    pageTitle = new Text(group, SWT.BORDER)
    pageTitle.setLayoutData(data)
    pageTitle.setText("new_page")
  }

  private def initInformationsGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2

    val data = new GridData
    data.widthHint = 240
    data.heightHint = 16

    val dataDescription = new GridData
    dataDescription.widthHint = 244
    dataDescription.heightHint = 100

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Informations:")
    group.setLayout(layout)

    //new Label(group, SWT.NONE).setText("Title")
    //pageTitle = new Text(group, SWT.BORDER)
    //pageTitle.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Subject")
    pageSubject = new Text(group, SWT.BORDER)
    pageSubject.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Creator")
    pageCreator = new Text(group, SWT.BORDER)
    pageCreator.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Description")
    pageDescription = new Text(group, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL)
    pageDescription.setLayoutData(dataDescription)
  }

}

class NewPageWizard extends Wizard {

  val newPagePage = new NewPageWizardPage

  override def performCancel : Boolean = {
    println("cancel creating page")
    true
  }

  override def addPages : Unit = {
    addPage(newPagePage)
  }

  override def performFinish : Boolean = {
    val pageTitle = newPagePage.pageTitle.getText
    if(!pageTitle.isEmpty ){
      val data = Map(
        APRJNames.name -> UUID.randomUUID.toString,
        APRJNames.creator -> newPagePage.pageCreator.getText,
        APRJNames.description -> newPagePage.pageDescription.getText,
        APRJNames.subject -> newPagePage.pageSubject.getText,
        APRJNames.title -> pageTitle
      )
      Perspective.projectManager.addPageToProject(data)
      true
    } else false
  }

  override def dispose : Unit = {super.dispose}
}
