/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.dialogs

import java.util.UUID
import org.eclipse.jface.wizard.Wizard
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets._
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout._
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.layout.RowLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Group
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.jface.wizard.WizardPage
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.runtime.Path
import org.eclipse.core.filesystem.URIUtil
import org.pragmas.anacleto.ProjectResources.ProjectTypes
import org.pragmas.anacleto.ui.views.ProjectManager
import org.pragmas.anacleto.models._
import org.pragmas.anacleto.ui.Perspective

class NewProjectWizardPage
extends WizardPage("org.pragmas.anacleto.ui.dialogs.NewProjectWizardPage") {

  var projectLocation : Text = _
  //var projectName : Text = _
  var buttonSearchLocation : Button = _
  var projectTitle : Text = _
  var projectSubject : Text = _
  var projectCreator : Text = _
  var projectDescription : Text = _
  var pageContainer : Composite = _

  setTitle("New Project")
  setDescription("Specify new project creation settings")

  override def createControl(parent : Composite) : Unit = {
    this.pageContainer = new Composite(parent, SWT.NONE)
    val layout = new RowLayout
    layout.`type` = SWT.VERTICAL
    this.pageContainer.setLayout(layout)
    new Label(this.pageContainer, SWT.NONE).setText("Project settings")

    initFirstGroup
    initInformationsGroup
    setControl(this.pageContainer)
    this.pageContainer.getShell.pack
  }

  private def initFirstGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 4

    val dataGridTitle = new GridData
    dataGridTitle.widthHint = 240
    dataGridTitle.heightHint = 16
    dataGridTitle.horizontalAlignment = GridData.FILL
    dataGridTitle.horizontalSpan = 2

    val data = new GridData
    data.widthHint = 240
    data.heightHint = 16
    data.horizontalAlignment = GridData.FILL
    data.horizontalSpan = 1

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Project:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("Title")
    projectTitle = new Text(group, SWT.BORDER)
    projectTitle.setLayoutData(dataGridTitle)
    projectTitle.setText("new_project")
    new Label(group, SWT.NONE).setText("")

    new Label(group, SWT.NONE).setText("Location")
    projectLocation = new Text(group, SWT.BORDER)
    projectLocation.setLayoutData(data)

    buttonSearchLocation = new Button(group, SWT.BORDER)
    buttonSearchLocation.setText("search")
    buttonSearchLocation.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e : SelectionEvent) {
        var shell = pageContainer.getShell
        val dialog = new DirectoryDialog(shell, SWT.OPEN)
        val fileSelected = dialog.open
        if (fileSelected != null) {
          projectLocation.setText(fileSelected)
        }
      }
    })
  }

  private def initInformationsGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2
    val data = new GridData
    data.widthHint = 240
    data.heightHint = 16

    val dataDescription = new GridData
    dataDescription.widthHint = 244
    dataDescription.heightHint = 100

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Informations:")
    group.setLayout(layout)

    //new Label(group, SWT.NONE).setText("Title")
    //projectTitle = new Text(group, SWT.BORDER)
    //projectTitle.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Subject")
    projectSubject = new Text(group, SWT.BORDER)
    projectSubject.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Creator")
    projectCreator = new Text(group, SWT.BORDER)
    projectCreator.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Description")
    projectDescription = new Text(group, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL)
    projectDescription.setLayoutData(dataDescription)
  }

}

class NewProjectWizard extends Wizard {

  val newProjectPage = new NewProjectWizardPage

  override def performCancel : Boolean = {
    println("cancel creating project")
    true
  }

  override def addPages : Unit = {
    addPage(newProjectPage)
  }

  override def performFinish : Boolean = {
    println("creating project")
    val projectLocation = newProjectPage.projectLocation.getText
    val projectTitle = newProjectPage.projectTitle.getText
    if(!projectLocation.isEmpty && !projectTitle.isEmpty ){
      val data = Map(
        APRJNames.name -> projectTitle.replace(" ", "_"),
        APRJNames.creator -> newProjectPage.projectCreator.getText,
        APRJNames.description -> newProjectPage.projectDescription.getText,
        APRJNames.subject -> newProjectPage.projectSubject.getText,
        APRJNames.title -> projectTitle
      )
      Perspective.projectManager.initEmptyProject(projectLocation, data)
      true
    } else false
  }

  override def dispose : Unit = {super.dispose}
}
