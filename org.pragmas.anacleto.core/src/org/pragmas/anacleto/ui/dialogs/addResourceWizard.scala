/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.dialogs

import org.eclipse.jface.wizard.Wizard
import org.eclipse.swt.SWT
//import org.eclipse.swt.widgets._
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout._
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.layout.RowLayout
import org.eclipse.swt.widgets.FileDialog
import org.eclipse.swt.widgets.Combo
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Group
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.jface.wizard.WizardPage
import org.eclipse.swt.widgets.Shell
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.runtime.Path
import org.eclipse.core.filesystem.URIUtil
import org.pragmas.anacleto.ProjectResources.ProjectTypes
import org.pragmas.anacleto.ui.views.ProjectManager
import org.pragmas.anacleto.models._
import org.pragmas.anacleto.ui.editors.ImageEditor
import org.pragmas.anacleto.ui.Perspective

class AddResourceWizardPage(resourceType : String)
extends WizardPage("org.pragmas.anacleto.ui.dialogs.AddResourceWizardPage") {

  var resourceTitle : Text = _
  var resourceSubject : Text = _
  var resourceCreator : Text = _
  var resourceDescription : Text = _
  var pageContainer : Composite = _

  var resourcePersonGivenName : Text = _
  var resourcePersonFamilyName : Text = _

  var resourceSelectSubType : Combo = _

  setTitle("Add Resource: " + resourceType)
  setDescription("Specify resource settings")

  override def createControl(parent : Composite) : Unit = {
    this.pageContainer = new Composite(parent, SWT.NONE)
    val layout = new RowLayout
    layout.`type` = SWT.VERTICAL
    this.pageContainer.setLayout(layout)
    new Label(this.pageContainer, SWT.NONE).setText("Resource settings")

    initFirstGroup
    initOptionalGroup
    initInformationsGroup
    setControl(this.pageContainer)
    this.pageContainer.getShell.pack
  }

  private def initOptionalGroup : Boolean = {
    resourceType match {
      case ProjectTypes.TAG => initSelectorControl; true
      case ProjectTypes.PERSON => initPersonControl; true
      case _ => false
    }
  }

  private def initPersonControl : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2

    val dataGridTitle = new GridData
    dataGridTitle.widthHint = 320
    dataGridTitle.heightHint = 16
    dataGridTitle.horizontalAlignment = GridData.FILL
    dataGridTitle.horizontalSpan = 2

    val data = new GridData
    data.widthHint = 320
    data.heightHint = 16
    data.horizontalAlignment = GridData.FILL
    data.horizontalSpan = 1

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Names:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("First name")
    resourcePersonGivenName = new Text(group, SWT.BORDER)
    resourcePersonGivenName.setLayoutData(dataGridTitle)
    resourcePersonGivenName.setText("")

    new Label(group, SWT.NONE).setText("Last name")
    resourcePersonFamilyName = new Text(group, SWT.BORDER)
    resourcePersonFamilyName.setLayoutData(dataGridTitle)
    resourcePersonFamilyName.setText("")
    //new Label(group, SWT.NONE).setText("")
  }

  private def initSelectorControl : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2

    val data = new GridData
    data.widthHint = 240
    data.horizontalAlignment = GridData.FILL
    data.horizontalSpan = 1

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Importance:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("Select color")
    resourceSelectSubType = new Combo(group, SWT.READ_ONLY)
    resourceSelectSubType.setLayoutData(data)
    resourceSelectSubType.setItems(ProjectTypes.RESOLVE_COLORS)
    resourceSelectSubType.select(0)
  }

  private def fileSelectionChoice(filterExtensions : Map[String, String]) : String = {
    var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
    val dialog = new FileDialog(shell, SWT.OPEN)
    val extensions = filterExtensions.keys.toList.toArray
    val extensionNames = filterExtensions.values.toList.toArray
    dialog.setFilterExtensions(extensions)
    dialog.setFilterNames(extensionNames)
    val fileSelected = dialog.open
    if (fileSelected != null) fileSelected else null
  }

  private def initFirstGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2

    val dataGridTitle = new GridData
    dataGridTitle.widthHint = 320
    dataGridTitle.heightHint = 16
    dataGridTitle.horizontalAlignment = GridData.FILL
    dataGridTitle.horizontalSpan = 2

    val data = new GridData
    data.widthHint = 320
    data.heightHint = 16
    data.horizontalAlignment = GridData.FILL
    data.horizontalSpan = 1

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Resource:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("Title")
    resourceTitle = new Text(group, SWT.BORDER)
    resourceTitle.setLayoutData(dataGridTitle)
    resourceTitle.setText("add_" + resourceType)
    //new Label(group, SWT.NONE).setText("")
  }

  private def initInformationsGroup : Unit = {
    val layout = new GridLayout
    layout.numColumns = 2
    val data = new GridData
    data.widthHint = 240
    data.heightHint = 16

    val dataDescription = new GridData
    dataDescription.widthHint = 244
    dataDescription.heightHint = 100

    val group = new Group(pageContainer, SWT.NONE)
    group.setText("Informations:")
    group.setLayout(layout)

    new Label(group, SWT.NONE).setText("Subject")
    resourceSubject = new Text(group, SWT.BORDER)
    resourceSubject.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Creator")
    resourceCreator = new Text(group, SWT.BORDER)
    resourceCreator.setLayoutData(data)

    new Label(group, SWT.NONE).setText("Description")
    resourceDescription = new Text(group, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL)
    resourceDescription.setLayoutData(dataDescription)
  }

}

class AddResourceWizard(resourceType : String) extends Wizard {

  val addResourcePage = new AddResourceWizardPage(resourceType)

  private def computeData : Map[String, String] = {
    resourceType match {
      case ProjectTypes.TAG => Map(APRJNames.subtypes ->  addResourcePage.resourceSelectSubType.getText)
      case ProjectTypes.PERSON => Map(
          APRJPersonNames.givenName ->  addResourcePage.resourcePersonGivenName.getText,
          APRJPersonNames.familyName ->  addResourcePage.resourcePersonFamilyName.getText
          )
      case _ => Map()
    }
  }

  override def performCancel : Boolean = {
    println("cancel adding resource")
    true
  }

  override def addPages : Unit = {
    addPage(addResourcePage)
  }

  override def performFinish : Boolean = {
    println("creating resource")
    val resourceTitle = addResourcePage.resourceTitle.getText
    if(!resourceTitle.isEmpty ){
      val additionalData = null
      val data = Map(
        APRJNames.creator -> addResourcePage.resourceCreator.getText,
        APRJNames.description -> addResourcePage.resourceDescription.getText,
        APRJNames.subject -> addResourcePage.resourceSubject.getText,
        APRJNames.title -> resourceTitle
      ) ++ computeData
      Perspective.projectManager.addResourceToPage(resourceType, data, additionalData)
      true
    } else false
  }

  override def dispose : Unit = {super.dispose}
}
