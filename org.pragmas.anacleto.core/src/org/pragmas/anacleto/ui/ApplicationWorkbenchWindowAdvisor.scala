/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui

import org.eclipse.swt.graphics.Point
import org.eclipse.ui.application.ActionBarAdvisor
import org.eclipse.ui.application.IActionBarConfigurer
import org.eclipse.ui.application.IWorkbenchWindowConfigurer
import org.eclipse.ui.application.WorkbenchWindowAdvisor
import org.eclipse.jface.preference.IPreferenceStore
import org.eclipse.ui.IWorkbenchPreferenceConstants
import org.eclipse.ui.PlatformUI

class ApplicationWorkbenchWindowAdvisor(val configurer : IWorkbenchWindowConfigurer)
    extends WorkbenchWindowAdvisor(configurer) {

  Perspective.windowAdvisor = this

  override def createActionBarAdvisor(configurer : IActionBarConfigurer) : ActionBarAdvisor = {
    new ApplicationActionBarAdvisor(configurer)
  }

  override def preWindowOpen : Unit = {

    //Perspective.configurer = getWindowConfigurer()
    Perspective.configurer.setInitialSize(new Point(800, 600))
    Perspective.configurer.setShowCoolBar(true)
    Perspective.configurer.setShowStatusLine(true)
    Perspective.configurer.setShowPerspectiveBar(true)
    Perspective.configurer.setTitle(Application.TITLE + " - " + Application.VERSION)

    // Set the preference toolbar to the left place
    // If other menus exists then this will be on the left of them
    Perspective.preferenceStore = PlatformUI.getPreferenceStore
    Perspective.preferenceStore.setValue(IWorkbenchPreferenceConstants.DOCK_PERSPECTIVE_BAR, "TOP_RIGHT")
    Perspective.preferenceStore.setValue(IWorkbenchPreferenceConstants.SHOW_TRADITIONAL_STYLE_TABS, true)

  }

  import org.eclipse.jface.preference.IPreferenceNode

  private def removedPreferences(id : String) : Boolean = {
    id match {
      case "org.eclipse.ui.preferencePages.LinkedResources"
        | "org.eclipse.ui.preferencePages.FileStates"
        | "org.eclipse.ui.preferencePages.BuildOrder"
        | "org.eclipse.ui.preferencePages.Decorators"
        | "org.eclipse.ui.preferencePages.Perspectives"
        | "org.eclipse.ui.editors.preferencePages.HyperlinkDetectorsPreferencePage"
        | "org.eclipse.ui.editors.preferencePages.LinkedModePreferencePage"
        | "org.eclipse.ui.editors.preferencePages.QuickDiff"
        | "org.eclipse.ui.editors.preferencePages.Annotations"
        | "org.eclipse.compare.internal.ComparePreferencePage"
        | "org.eclipse.ui.editors.preferencePages.Spelling"
        | "org.eclipse.ui.editors.preferencePages.Accessibility"
        | "org.eclipse.ui.preferencePages.Workspace" => true
      case _ => false
    }
  }

  private def deletePreferences(node : IPreferenceNode = null) : Unit = {
    node.getSubNodes foreach { n =>
      if (removedPreferences(n.getId)) node.remove(n)
      deletePreferences(n)
    }
  }

  override def postWindowOpen : Unit = {
    Perspective.actionBar.menuProject.deactivateElementActions
    Perspective.actionBar.enableToolbarProjectElements(false)

    PlatformUI.getWorkbench.getPreferenceManager.getRootSubNodes foreach { node =>
      deletePreferences(node)
    }

  }
}
