/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui.editors.imageEditorContributors

import scala.math._
//import java.util.ArrayList
import java.io.File

import org.eclipse.swt.SWT
import org.eclipse.swt.events.ControlAdapter
import org.eclipse.swt.events.ControlEvent
import org.eclipse.swt.events.MouseEvent
import org.eclipse.swt.events.MouseListener
import org.eclipse.swt.events.KeyListener
import org.eclipse.swt.events.KeyEvent
import org.eclipse.swt.events.MouseMoveListener
import org.eclipse.swt.events.PaintEvent
import org.eclipse.swt.events.PaintListener
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.graphics.Color
import org.eclipse.swt.graphics.Cursor
import org.eclipse.swt.graphics.GC
import org.eclipse.swt.graphics.Image
import org.eclipse.swt.graphics.ImageData
//import org.eclipse.swt.graphics.PaletteData
//import org.eclipse.swt.graphics.Point
import org.eclipse.swt.widgets.Canvas
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.ScrollBar

import org.eclipse.core.runtime.jobs._
import org.eclipse.ui.progress.UIJob
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.IStatus
import org.eclipse.core.runtime.Status
//import org.eclipse.core.runtime.CoreException

import java.awt.Dimension
import java.io.ByteArrayInputStream

import magick.{ MagickImage, ImageInfo, QuantizeInfo, ColorspaceType }

import org.pragmas.anacleto.tools.ResourceManager
import org.pragmas.anacleto.extensions._
import org.pragmas.anacleto.ui.editors.ImageEditor

//import org.pragmas.anacleto.implicits.Events._
import org.pragmas.anacleto.implicits.Jobs._

class JMagickCanvas(parent : Composite, style : Int)
    extends Canvas(parent, style | SWT.BORDER | SWT.NO_BACKGROUND | SWT.V_SCROLL | SWT.H_SCROLL) {

  //System.setProperty("jmagick.systemclassloader","false")

  var image : MagickImage = null
  var imageTemp : MagickImage = null
  var imageInfo : ImageInfo = null
  var filtersPath : String = _

  var imageSWT : Image = null

  private var originalDimension : Dimension = null

  private var redraw_after_filter = false

  private var clientw : Int = 1
  private var clienth : Int = 1
  private var imgx : Int = 1
  private var imgy : Int = 1
  private var imgw : Int = 1
  private var imgh : Int = 1
  private var scrolly : Int = 1
  private var scrollx : Int = 1
  var mousex : Int = 1
  var mousey : Int = 1

  private def ZOOM_SCALE_STEP : Double = 0.2
  private var zoomScaleStep : Double = 1.0

  private def ROTATION_STEP : Double = 90.0
  private var rotationStep : Double = 0.0

  //private var listenForMouseMovement : Boolean = false
  private var listenMouseIsDown : Boolean = false
  private var listenMouseIsUp : Boolean = true
  private var keyAltIsPressed : Boolean = false

  private var COLOR_WIDGET_BACKGROUND : Color = getDisplay.getSystemColor(SWT.COLOR_WHITE)

  private val handOpen : Cursor = ResourceManager.getCursor(getDisplay, "silk/hand.png")
  //private val handClosed : Cursor = ResourceManager.getCursor(getDisplay, "silk/hand_close.png")
  //setCursor(handOpen)

  addControlListener(new ControlAdapter {
    override def controlResized(e : ControlEvent) : Unit = {
      updateScrollbarPosition
    }
  })

  addKeyListener(new KeyListener {
    override def keyPressed(e : KeyEvent) : Unit = {
      //println(e)
      if (e.keyCode == SWT.ALT) {
        keyAltIsPressed = true
        setCursor(handOpen)
      }
    }

    override def keyReleased(e : KeyEvent) : Unit = {
      //println(e)
      if (e.keyCode == SWT.ALT) {
        keyAltIsPressed = false
        setCursor(null)
      }
    }
  })

  addMouseListener(new MouseListener {
    override def mouseDoubleClick(e : MouseEvent) : Unit = {
      eventMouseDoubleClick(e)
    }

    override def mouseDown(e : MouseEvent) : Unit = {
      eventMouseDown(e)
    }

    override def mouseUp(e : MouseEvent) : Unit = {
      eventMouseUp(e)
    }
  })

  addMouseMoveListener(new MouseMoveListener {
    override def mouseMove(e : MouseEvent) : Unit = {
      if (scrollByMouse) {
        followMouseDownForScroll(e)
      } else if (listenMouseIsUp) {
        followMouseUp(e)
      }
    }
  })

  addPaintListener(new PaintListener {
    override def paintControl(event : PaintEvent) : Unit = {
      paint(event.gc)
    }
  })

  getHorizontalBar.setEnabled(true)
  getHorizontalBar.addSelectionListener(new SelectionAdapter {
    override def widgetSelected(event : SelectionEvent) : Unit = {
      updateHorizontalScroll(event.widget.asInstanceOf[ScrollBar])
    }
  })

  getVerticalBar.setEnabled(true)
  getVerticalBar.addSelectionListener(new SelectionAdapter {
    override def widgetSelected(event : SelectionEvent) : Unit = {
      updateVerticalScroll(event.widget.asInstanceOf[ScrollBar])
    }
  })

  private def scrollByMouse : Boolean = { listenMouseIsDown && keyAltIsPressed }

  private def followMouseUp(e : MouseEvent) : Unit = {
    mousex = e.x
    mousey = e.y
    ImageEditor.editor.status.updateWithCurrent
  }

  private def followMouseDownForScroll(e : MouseEvent) : Unit = {
    if (clientw < imgw) {
      var mouseDiffX = (mousex - e.x)
      mousex = e.x
      imgx -= mouseDiffX
      getHorizontalBar.setSelection(getHorizontalBar.getSelection + mouseDiffX)
      var minx = clientw - imgw
      var maxx = 0
      if (imgx < minx)
        imgx = minx
      if (imgx > maxx)
        imgx = maxx
      scrollx = getHorizontalBar.getSelection
    }
    if (clienth < imgh) {
      var mouseDiffY = (mousey - e.y)
      mousey = e.y
      imgy -= mouseDiffY
      getVerticalBar.setSelection(getVerticalBar.getSelection + mouseDiffY)
      var miny = clienth - imgh
      var maxy = 0
      if (imgy < miny)
        imgy = miny
      if (imgy > maxy)
        imgy = maxy

      scrolly = getVerticalBar.getSelection
    }

    redraw
  }

  private def eventMouseDown(e : MouseEvent) : Unit = {
    listenMouseIsDown = true
    listenMouseIsUp = false
    mousex = e.x
    mousey = e.y
    //setCursor(handClosed)
  }

  private def eventMouseUp(e : MouseEvent) : Unit = {
    listenMouseIsDown = false
    listenMouseIsUp = true
    //mousex = e.x
    //mousey = e.y
    //setCursor(handOpen)
  }

  def eventMouseDoubleClick(e : MouseEvent) : Unit = {
    //ImageEditor.status.updateWithCurrent
    // redraw
  }

  private def updateScrollbarPosition : Unit = {
    clientw = getClientArea.width
    clienth = getClientArea.height
    if (clientw < 1) clientw = 1
    if (clienth < 1) clienth = 1

    val dim =
      if (redraw_after_filter && imageTemp != null)
        imageTemp.getDimension
      else image.getDimension
    imgh = dim.height
    imgw = dim.width

    updateScrollVisibility
    getVerticalBar.setSelection(0)
    getHorizontalBar.setSelection(0)
    imgx = clientw / 2 - imgw / 2
    imgy = clienth / 2 - imgh / 2

    if (imgx < 0) imgx = 0
    if (imgy < 0) imgy = 0

    scrollx = getHorizontalBar.getSelection
    scrolly = getVerticalBar.getSelection

    var vertical = getVerticalBar
    vertical.setMaximum(imgh)
    vertical.setThumb(min(clienth, imgh))
    vertical.setIncrement(40)
    vertical.setPageIncrement(clienth)

    var horizontal = getHorizontalBar
    horizontal.setMaximum(imgw)
    horizontal.setThumb(min(clientw, imgw))
    horizontal.setIncrement(40)
    horizontal.setPageIncrement(clientw)

    //redraw
  }

  private def updateScrollVisibility : Unit = {
    // only show when neccessary
    getHorizontalBar.setVisible(clientw < imgw)
    getVerticalBar.setVisible(clienth < imgh)
  }

  private def updateVerticalScroll(bar : ScrollBar) : Unit = {
    imgy -= bar.getSelection - scrolly
    scrolly = bar.getSelection
    redraw
  }

  private def updateHorizontalScroll(bar : ScrollBar) : Unit = {
    imgx -= bar.getSelection - scrollx
    scrollx = bar.getSelection
    redraw
  }

  def loadImage(filepath : String) : Unit = {
    if (new File(filepath).exists) {
      disposeImages
      imageInfo = new ImageInfo(filepath)
      image = new MagickImage(imageInfo)
      originalDimension = image.getDimension
      magickToImage(image, imageInfo)
      updateScrollbarPosition
      redraw
    }
  }

  def saveImage(filepath : String) : Unit = {
    val image_info = new ImageInfo
    image.setFileName(filepath)
    image.writeImage(image_info)
  }

  def magickToImage(magickImage : MagickImage, imageInfo : ImageInfo) : Image = {
    val blob = magickImage.imageToBlob(imageInfo)
    val imageSWTData = new ImageData(new ByteArrayInputStream(blob))
    imageSWT = new Image(getDisplay, imageSWTData)
    imageSWT
  }

  private def redrawImage(afterFilter : Boolean) : Image = {
    val retImage : Image = if (scrollByMouse) {
      if (imageSWT != null) imageSWT else magickToImage(image, imageInfo)
    } else if (afterFilter) {
      val image = magickToImage(imageTemp, imageInfo)
      disposeMagickTemp
      image
    } else if (imageSWT != null) imageSWT else magickToImage(image, imageInfo)
    retImage
  }

  def redrawAfterFilter : Unit = {
    workerUI("redrawAfterFilter") { monitor : IProgressMonitor =>
      redraw_after_filter = true
      redraw
      updateScrollbarPosition
      Status.OK_STATUS
    }
  }

  def updateFullsizeData : Unit = {
    disposeImages
    zoomScaleStep = 0
    updateScrollbarPosition
  }

  def paint(gc : GC) : Unit = {

    val canvasSWTImage = new Image(getDisplay, clientw, clienth)
    val canvasSWTImageContext = new GC(canvasSWTImage)

    canvasSWTImageContext.setBackground(COLOR_WIDGET_BACKGROUND)
    canvasSWTImageContext.setClipping(getClientArea)
    canvasSWTImageContext.fillRectangle(getClientArea)

    canvasSWTImageContext.drawImage(redrawImage(redraw_after_filter), imgx, imgy)
    gc.drawImage(canvasSWTImage, 0, 0)
    //gc.drawImage(redrawImage(redraw_after_filter), imgx, imgy)

    canvasSWTImage.dispose
    canvasSWTImageContext.dispose
    redraw_after_filter = false
  }

  private def disposeImages : Unit = {
    disposeSWT
    disposeMagick
  }

  private def disposeSWT : Unit = {
    if (imageSWT != null) {
      imageSWT.dispose
      imageSWT = null
    }
  }

  private def disposeMagick : Unit = {
    originalDimension = null
    disposeMagickTemp
    if (image != null) {
      image.destroyImages
      image = null
      imageInfo = null
    }
  }

  private def disposeMagickTemp : Unit = {
    if (imageTemp != null) {
      imageTemp.destroyImages
      imageTemp = null
    }
  }

  override def dispose : Unit = {
    disposeSWT
    disposeMagick
    super.dispose
  }

  def rotate : Unit = {
    rotationStep += ROTATION_STEP
    imageTemp = image.rotateImage(-rotationStep)
    redrawAfterFilter
  }

  def zoomIn : Unit = {
    zoomScaleStep += ZOOM_SCALE_STEP
    val dim = image.getDimension
    val deltaX = dim.width.asInstanceOf[Double] * zoomScaleStep
    val deltaY = dim.height.asInstanceOf[Double] * zoomScaleStep
    imageTemp = image.zoomImage(deltaX.asInstanceOf[Int], deltaY.asInstanceOf[Int])
    redrawAfterFilter
  }

  def zoomOut : Unit = {
    zoomScaleStep -= ZOOM_SCALE_STEP
    if (zoomScaleStep >= ZOOM_SCALE_STEP) {
      val dim = image.getDimension
      val deltaX = dim.width.asInstanceOf[Double] * zoomScaleStep
      val deltaY = dim.height.asInstanceOf[Double] * zoomScaleStep
      imageTemp = image.zoomImage(deltaX.asInstanceOf[Int], deltaY.asInstanceOf[Int])
      redrawAfterFilter
    } else zoomScaleStep = 1.0
  }

  def zoomFit : Unit = {
    imageTemp = image.zoomImage(clientw, clienth)
    redrawAfterFilter
  }

  def zoomOriginal : Unit = {
    //imageTemp = image
    zoomScaleStep = 1.0
    redraw
    //redrawAfterFilter
  }

  //  def sharpImage : Unit = {
  //    //(raduis : Double, sigma : Double)
  //    //imageTemp = image.sharpenImage(raduis, sigma)
  //    redrawAfterFilter
  //  }

  /* destructive */
  def unsharpMask : Unit = {
    //-unsharp 6x3+1+0
    image = image.unsharpMaskImage(6, 3, 1, 0)
    imageTemp = image.cloneImage(0, 0, true)
    redrawAfterFilter
  }

  /* destructive */
  def sharp : Unit = {
    image = image.sharpenImage(6, 3)
    imageTemp = image.cloneImage(0, 0, true)
    redrawAfterFilter
  }

  /* destructive */
  def enhance : Unit = {
    image = image.enhanceImage
    imageTemp = image.cloneImage(0, 0, true)
    redrawAfterFilter
  }

  /* destructive */
  def negate : Unit = {
    if (image.negateImage(0)) {
      imageTemp = image.cloneImage(0, 0, true)
      redrawAfterFilter
    }
  }

  /* destructive */
  def normalize : Unit = {
    if (image.normalizeImage) {
      imageTemp = image.cloneImage(0, 0, true)
      redrawAfterFilter
    }
  }

  /* destructive */
  def solarize : Unit = {
    image.solarizeImage(100)
    imageTemp = image.cloneImage(0, 0, true)
    redrawAfterFilter
  }

  /* destructive */
  def contrast : Unit = {
    if (image.contrastImage(true)) {
      imageTemp = image.cloneImage(0, 0, true)
      redrawAfterFilter
    }
  }

  /* destructive */
  def grayscale : Unit = {
    val qi = new QuantizeInfo
    qi.setColorspace(ColorspaceType.GRAYColorspace)
    if (image.quantizeImage(qi)) {
      imageTemp = image.cloneImage(0, 0, true)
      redrawAfterFilter
    }
  }

  def executeExtensionFilters(scriptlets : Array[String]) : Unit = {
    var resultStatus = Status.OK_STATUS

      def execute(scriptlets : Array[String], monitor : IProgressMonitor) : MagickImage = {
        var buff = image.cloneImage(0, 0, true)
        for (scriptlet <- scriptlets) {
          resultStatus =
            if (!monitor.isCanceled) {
              buff = ExtensionManager.execute(
                ExtensionManager.readScriptletFile(scriptlet),
                buff, ExtensionType.IMAGE_MAGICK_MANIPULATOR).asInstanceOf[MagickImage]
              Status.OK_STATUS
            } else Status.CANCEL_STATUS
        }
        buff
      }

    workerWithDone("Execute scriptlets filter", "Execute scriptlets filter") { monitor : IProgressMonitor =>
      imageTemp = execute(scriptlets, monitor).cloneImage(0, 0, true)
      resultStatus
    }{ event : IJobChangeEvent =>
      if (imageTemp != null) {
        redrawAfterFilter
      }
    }

  }

}
