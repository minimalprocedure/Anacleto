/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.editors.imageEditorContributors

import org.eclipse.ui.views.contentoutline._
import org.eclipse.core.runtime.ListenerList
import org.eclipse.core.runtime.SafeRunner
import org.eclipse.jface.util.SafeRunnable
import org.eclipse.jface.viewers.ISelection
import org.eclipse.jface.viewers.ISelectionChangedListener
import org.eclipse.jface.viewers.SelectionChangedEvent
import org.eclipse.jface.viewers.StructuredSelection
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredViewer
import org.eclipse.jface.viewers.TreeViewer
import org.eclipse.jface.viewers.IOpenListener
import org.eclipse.jface.viewers.OpenEvent
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Control
import org.eclipse.ui.part.IPageSite
import org.eclipse.ui.part.Page
import org.eclipse.ui.IPageLayout
import org.pragmas.anacleto.ProjectResources._
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.ui.editors.ImageEditor

class CustomContentOutlinePage
  extends Page
  with IContentOutlinePage
  with ISelectionChangedListener
  with IOpenListener {
  private val selectionChangedListeners : ListenerList = new ListenerList

  def ID = IPageLayout.ID_OUTLINE

  var outlineViewer : TreeViewer = null
  var currentNode : TreeObject = _

  var rootFilters : TreeParent = _
  private var parent : Composite = _

  def setFiltersNode(rootNode : TreeParent) : Unit = {
    rootFilters = rootNode
    if (outlineViewer != null)
      outlineViewer.setInput(rootFilters)
  }

  private def refresh : Unit = {
    outlineViewer.asInstanceOf[StructuredViewer].refresh(true)
  }

  def addSelectionChangedListener(listener : ISelectionChangedListener) : Unit = {
    selectionChangedListeners.add(listener)
  }

  def createControl(parent : Composite) : Unit = {
    this.parent = parent
    initOutlineViewer(parent)
  }

  private def initOutlineViewer(parent : Composite) : Unit = {
    if (outlineViewer == null) {
      outlineViewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL)
      outlineViewer.addSelectionChangedListener(this)
      outlineViewer.addOpenListener(this)
    }
    outlineViewer.setContentProvider(new ViewContentProvider)
    outlineViewer.setLabelProvider(new ViewLabelProvider)
    outlineViewer.setInput(rootFilters)
  }

  override def open(event : OpenEvent) : Unit = {
    val node = Perspective.projectManager.currentNode
    if (node.propertyExist(ProjectTypes.FILTERS)) {
      if (node.listValueExist(ProjectTypes.FILTERS, currentNode.name))
        node.removeProperty(ProjectTypes.FILTERS, currentNode.name)
      else
        node.property(ProjectTypes.FILTERS, currentNode.name)
    } else {
      node.property(ProjectTypes.FILTERS, List[String]())
      node.property(ProjectTypes.FILTERS, currentNode.name)
    }
    setFiltersNode(ImageEditor.editor.loadFilters)
    ImageEditor.editor.setDirty(true)
  }

  protected def updateCurrentNode(selection : ISelection) : Unit = {
    if (!selection.isEmpty && selection.isInstanceOf[IStructuredSelection]) {
      val selectionIterator = selection.asInstanceOf[IStructuredSelection].iterator
      /* selected only first */
      if (selectionIterator.hasNext)
        currentNode = selectionIterator.next.asInstanceOf[TreeObject]
    }
  }

  def selectionChanged(event : SelectionChangedEvent) : Unit = {
    fireSelectionChanged(event.getSelection)
  }

  protected def fireSelectionChanged(selection : ISelection) : Unit = {
    val listeners = selectionChangedListeners.getListeners
    for (listener <- listeners) {
      SafeRunner.run(new SafeRunnable {
        def run : Unit = {
          updateCurrentNode(selection)
        }
      })
    }
  }

  def getControl : Control = {
    if (outlineViewer == null)
      null
    else
      outlineViewer.getControl
  }

  def getSelection : ISelection = {
    if (outlineViewer == null)
      StructuredSelection.EMPTY
    else
      outlineViewer.getSelection
  }

  protected def getTreeViewer : TreeViewer = {
    outlineViewer
  }

  override def init(pageSite : IPageSite) : Unit = {
    super.init(pageSite)
    pageSite.setSelectionProvider(this)
  }

  def removeSelectionChangedListener(listener : ISelectionChangedListener) : Unit = {
    selectionChangedListeners.remove(listener)
  }

  def setFocus : Unit = {
    outlineViewer.getControl.setFocus
  }

  def setSelection(selection : ISelection) : Unit = {
    if (outlineViewer != null) outlineViewer.setSelection(selection)
  }

  def refreshOutline : Unit = refresh
}
