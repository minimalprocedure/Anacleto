/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.editors.imageEditorContributors

import java.io.File
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.Image
import org.eclipse.swt.widgets.Combo
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.ToolBar
import org.eclipse.swt.widgets.ToolItem
import org.eclipse.ui.PlatformUI
import org.eclipse.jface.wizard.WizardDialog
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.dialogs.InputDialog
import org.eclipse.core.filesystem.EFS
import org.eclipse.swt.events.{
  ControlEvent,
  ControlAdapter,
  SelectionAdapter,
  SelectionEvent,
  SelectionListener
}
import org.pragmas.anacleto.tools.ResourceManager
import org.pragmas.anacleto.ui.editors.{ ImageEditor, ImageEditorImage }
import org.pragmas.anacleto.ui.dialogs.AddScriptletWizard
import org.pragmas.anacleto.extensions.ExtensionType
import org.eclipse.swt.SWT
import org.eclipse.swt.events.ModifyEvent
import org.eclipse.swt.events.ModifyListener
import org.eclipse.core.runtime.{
  Platform,
  IPath,
  Path,
  IProgressMonitor
}
import org.pragmas.anacleto.models.APRJNames
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.implicits.Events._

class ImageEditorToolbarManager(parent : Composite) {

  val toolBar : ToolBar = new ToolBar(parent, SWT.FLAT)
  var selectionImageSize = new Combo(toolBar, SWT.NONE | SWT.DROP_DOWN | SWT.READ_ONLY)

  initItems

  private def initSelectionImageSizeCombo : Unit = {
    selectionImageSize.setItems(ImageEditor.editor.loadSizeTags.toArray)
    selectionImageSize.setTextLimit(10)
    val index =
      if (ImageEditor.editor.projectNode.propertyExist(APRJNames.active))
        selectionImageSize.indexOf(ImageEditor.editor.projectNode.property(APRJNames.active).toString)
      else
        -1
    selectionImageSize.select(if (index > -1) index else 0)
  }

  private def initItems : Unit = {

    /* comboToolbarItem */
    val tool = new ToolItem(toolBar, SWT.SEPARATOR)
    initSelectionImageSizeCombo
    selectionImageSize.addModifyListener(new ModifyListener {
      override def modifyText(e : ModifyEvent) : Unit = {
        val editor = ImageEditor.editor
        editor.projectNode.property(APRJNames.active, selectionImageSize.getText)
        Perspective.projectManager.setDirty(true)
        ImageEditor.editor.setDirty(true)
        editor.resolveImageBySizeTag(editor.imageFile, selectionImageSize.getText)
        ImageEditor.editor.canvas.loadImage(editor.imageFile.getAbsolutePath)
      }
    })

    tool.setControl(selectionImageSize)
    tool.setWidth(selectionImageSize.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x)

    new ToolItem(toolBar, SWT.SEPARATOR)

    val rotate = new ToolItem(toolBar, SWT.FLAT)
    rotate.setToolTipText("rotate image")
    rotate.setImage(ResourceManager.getIcon("silk/arrow_rotate_anticlockwise.png"))
    rotate.addSelectionListener({ e : SelectionEvent =>
      rotate.setSelection(false)
      ImageEditor.editor.canvas.rotate
    })

    new ToolItem(toolBar, SWT.SEPARATOR)

    val zoomIn = new ToolItem(toolBar, SWT.FLAT)
    zoomIn.setToolTipText("zoom in")
    zoomIn.setImage(ResourceManager.getIcon("silk/zoom_in.png"))
    zoomIn.addSelectionListener({ e : SelectionEvent =>
      zoomIn.setSelection(false)
      ImageEditor.editor.canvas.zoomIn
    })

    val zoomOut = new ToolItem(toolBar, SWT.FLAT)
    zoomOut.setToolTipText("zoom out")
    zoomOut.setImage(ResourceManager.getIcon("silk/zoom_out.png"))
    zoomOut.addSelectionListener({ e : SelectionEvent =>
      zoomOut.setSelection(false)
      ImageEditor.editor.canvas.zoomOut
    })

    val zoomOrg = new ToolItem(toolBar, SWT.FLAT)
    zoomOrg.setToolTipText("zoom original size")
    zoomOrg.setImage(ResourceManager.getIcon("silk/zoom.png"))
    zoomOrg.addSelectionListener({ e : SelectionEvent =>
      zoomOrg.setSelection(false)
      ImageEditor.editor.canvas.zoomOriginal
    })

    val zoomFit = new ToolItem(toolBar, SWT.CHECK)
    zoomFit.setToolTipText("fit image in window")
    zoomFit.setImage(ResourceManager.getIcon("silk/zoom_fit.png"))
    zoomFit.addSelectionListener({ e : SelectionEvent =>
      zoomFit.setSelection(false)
      ImageEditor.editor.canvas.zoomFit
    })

    val unsharp = new ToolItem(toolBar, SWT.CHECK)
    unsharp.setToolTipText("unsharp mask image")
    unsharp.setImage(ResourceManager.getIcon("silk/image_unsharp.png"))
    unsharp.addSelectionListener({ e : SelectionEvent =>
      unsharp.setSelection(false)
      ImageEditor.editor.canvas.unsharpMask
    })

    val sharp = new ToolItem(toolBar, SWT.CHECK)
    sharp.setToolTipText("sharp image")
    sharp.setImage(ResourceManager.getIcon("silk/image_sharp.png"))
    sharp.addSelectionListener({ e : SelectionEvent =>
      sharp.setSelection(false)
      ImageEditor.editor.canvas.sharp
    })

    val contrast = new ToolItem(toolBar, SWT.CHECK)
    contrast.setToolTipText("contrast image")
    contrast.setImage(ResourceManager.getIcon("silk/image_contrast.png"))
    contrast.addSelectionListener({ e : SelectionEvent =>
      contrast.setSelection(false)
      ImageEditor.editor.canvas.contrast
    })

    val enhance = new ToolItem(toolBar, SWT.CHECK)
    enhance.setToolTipText("enhance image")
    enhance.setImage(ResourceManager.getIcon("silk/image_enhance.png"))
    enhance.addSelectionListener({ e : SelectionEvent =>
      enhance.setSelection(false)
      ImageEditor.editor.canvas.enhance
    })

    val grayscale = new ToolItem(toolBar, SWT.CHECK)
    grayscale.setToolTipText("grayscale image")
    grayscale.setImage(ResourceManager.getIcon("silk/image_bw.png"))
    grayscale.addSelectionListener({ e : SelectionEvent =>
      grayscale.setSelection(false)
      ImageEditor.editor.canvas.grayscale
    })

    val negate = new ToolItem(toolBar, SWT.CHECK)
    negate.setToolTipText("negate image")
    negate.setImage(ResourceManager.getIcon("silk/image_negate.png"))
    negate.addSelectionListener({ e : SelectionEvent =>
      negate.setSelection(false)
      ImageEditor.editor.canvas.negate
    })

    val solarize = new ToolItem(toolBar, SWT.CHECK)
    solarize.setToolTipText("solarize image")
    solarize.setImage(ResourceManager.getIcon("silk/image_solarize.png"))
    solarize.addSelectionListener({ e : SelectionEvent =>
      solarize.setSelection(false)
      ImageEditor.editor.canvas.solarize
    })

    val normalize = new ToolItem(toolBar, SWT.CHECK)
    normalize.setToolTipText("normalize image")
    normalize.setImage(ResourceManager.getIcon("silk/image_normalize.png"))
    normalize.addSelectionListener({ e : SelectionEvent =>
      normalize.setSelection(false)
      ImageEditor.editor.canvas.normalize
    })

    new ToolItem(toolBar, SWT.SEPARATOR)

    val saveFilteredImage = new ToolItem(toolBar, SWT.CHECK)
    saveFilteredImage.setToolTipText("save filtered image")
    saveFilteredImage.setImage(ResourceManager.getIcon("silk/image_save.png"))
    saveFilteredImage.addSelectionListener({ e : SelectionEvent =>
      var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
      val inputDialog = new InputDialog(shell, "Size tag", "size tag", "", null)
      inputDialog.open
      var text : String = inputDialog.getValue
      text = if (text != null) text.trim else ""
      if (!text.isEmpty) {
        val simplename = ImageEditor.editor.getProjectNodeSimpleName
        val fImage = new File(ImageEditor.editor.imageFile.getParent,
          simplename + ImageEditorImage.SEP_TAG + text +
            ImageEditorImage.SEP_EXT + ImageEditorImage.DEFAULT_EXT)
        ImageEditor.editor.canvas.saveImage(fImage.toString)
        initSelectionImageSizeCombo
      }
    })

    val deleteFilteredImage = new ToolItem(toolBar, SWT.CHECK)
    deleteFilteredImage.setToolTipText("delete filtered image")
    deleteFilteredImage.setImage(ResourceManager.getIcon("silk/image_delete.png"))
    deleteFilteredImage.addSelectionListener({ e : SelectionEvent =>
      if (ImageEditor.editor.isImageDeletable(selectionImageSize.getText)) {
        var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
        if (MessageDialog.openConfirm(shell, "delete", "delete image")) {
          val simplename = ImageEditor.editor.getProjectNodeSimpleName
          val path = new Path(ImageEditor.editor.imageFile.getParent).append(simplename + ImageEditorImage.SEP_TAG + selectionImageSize.getText +
            ImageEditorImage.SEP_EXT + ImageEditorImage.DEFAULT_EXT)
          val store = EFS.getLocalFileSystem.getStore(path)
          if (store.fetchInfo.exists) store.delete(EFS.NONE, null)
        }
        initSelectionImageSizeCombo
      }
    })

    new ToolItem(toolBar, SWT.SEPARATOR)

    val scriptFilterAdd = new ToolItem(toolBar, SWT.CHECK)
    scriptFilterAdd.setToolTipText("new script filter")
    scriptFilterAdd.setImage(ResourceManager.getIcon("silk/image_filter_add.png"))
    scriptFilterAdd.addSelectionListener({ e : SelectionEvent =>
      scriptFilterAdd.setSelection(false)
      var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
      val dialog = new WizardDialog(shell, new AddScriptletWizard(ExtensionType.IMAGE_MAGICK_MANIPULATOR))
      dialog.setTitle("Add Scriptlet Wizard")
      val ret = dialog.open
      if (ret == 0) {
        val loadFilters = ImageEditor.editor.loadFilters
        ImageEditor.editor.contentOutlinePage.setFiltersNode(loadFilters)
      }
    })

    val scriptFilterRemove = new ToolItem(toolBar, SWT.CHECK)
    scriptFilterRemove.setToolTipText("remove script filter")
    scriptFilterRemove.setImage(ResourceManager.getIcon("silk/image_filter_delete.png"))
    scriptFilterRemove.addSelectionListener({ e : SelectionEvent =>

      scriptFilterRemove.setSelection(false)
      val contentOutlinePage = ImageEditor.editor.contentOutlinePage
      var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
      if (MessageDialog.openConfirm(shell, "delete", "delete filter: " + contentOutlinePage.currentNode.name)) {
        Perspective.projectManager.filtersContainer.getChildren foreach { f =>
          if (f.name == contentOutlinePage.currentNode.name) {
            val oldnode = Perspective.projectManager.currentNode
            Perspective.projectManager.currentNode = f
            Perspective.projectManager.removeCurrentNode
            Perspective.projectManager.currentNode = oldnode
          }
        }
        contentOutlinePage.rootFilters.removeChild(contentOutlinePage.currentNode)
        ImageEditor.editor.contentOutlinePage.refreshOutline
      }

    })

    val scriptFilterEdit = new ToolItem(toolBar, SWT.CHECK)
    scriptFilterEdit.setToolTipText("edit script filter")
    scriptFilterEdit.setImage(ResourceManager.getIcon("silk/image_filter_edit.png"))
    scriptFilterEdit.addSelectionListener({ e : SelectionEvent =>
      scriptFilterEdit.setSelection(false)
      Perspective.projectManager.openResource(ImageEditor.editor.contentOutlinePage.currentNode)
    })

    val scriptFilterRefresh = new ToolItem(toolBar, SWT.CHECK)
    scriptFilterRefresh.setToolTipText("refresh script filter")
    scriptFilterRefresh.setImage(ResourceManager.getIcon("silk/image_filter_refresh.png"))
    scriptFilterRefresh.addSelectionListener({ e : SelectionEvent =>
      scriptFilterRefresh.setSelection(false)
      val loadFilters = ImageEditor.editor.loadFilters
      ImageEditor.editor.contentOutlinePage.setFiltersNode(loadFilters)
    })

    val scriptFilterGo = new ToolItem(toolBar, SWT.CHECK)
    scriptFilterGo.setToolTipText("run scripts filter stack")
    scriptFilterGo.setImage(ResourceManager.getIcon("silk/image_filter_go.png"))
    scriptFilterGo.addSelectionListener({ e : SelectionEvent =>
      scriptFilterGo.setSelection(false)
      ImageEditor.editor.executeFilters
    })
  }

}
