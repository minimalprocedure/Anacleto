/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.editors.imageEditorContributors

//import java.text.DecimalFormat
import org.eclipse.swt.SWT
import org.eclipse.swt.events.PaintEvent
import org.eclipse.swt.events.PaintListener
import org.eclipse.swt.graphics.Color
import org.eclipse.swt.graphics.GC
//import org.eclipse.swt.graphics.Image
import org.eclipse.swt.widgets.Canvas
import org.eclipse.swt.widgets.Composite
import org.pragmas.anacleto.ui.editors.ImageEditor


class ImageStatusCanvas(parent : Composite, style : Int )
extends Canvas(parent, style | SWT.FLAT) {

  //private var df : DecimalFormat = new DecimalFormat("0.000")
  private var COLOR_DARK_GRAY : Color = parent.getDisplay.getSystemColor(SWT.COLOR_DARK_GRAY)

  this.addPaintListener(new PaintListener {
      override def paintControl(event : PaintEvent) : Unit = {
        paint(event.gc)
      }
    })

  def updateWithCurrent : Unit = {
    if (!isDisposed) redraw
  }

  def paint(gc : GC) : Unit = {
    if (ImageEditor.editor.canvas.image != null && ImageEditor.editor.canvas.imageInfo != null) {
      gc.drawString("Mouse: x:" + ImageEditor.editor.canvas.mousex + " y:" + ImageEditor.editor.canvas.mousey, 5, 2)
      gc.drawString("Depth: " + ImageEditor.editor.canvas.image.getDepth.toString, 160, 2)
      val dim = ImageEditor.editor.canvas.image.getDimension
      gc.drawString(dim.width + " x " + dim.height, 245, 2)
      gc.drawString("Size (kb): " + ImageEditor.editor.canvas.imageInfo.getSize, 345, 2)
      gc.drawString("Name: " + ImageEditor.editor.canvas.imageInfo.getFileName, 445, 2)
      gc.setForeground(COLOR_DARK_GRAY)
      gc.drawLine(135, 0, 135, 24)
      gc.drawLine(210, 0, 210, 24)
      gc.drawLine(320, 0, 320, 24)
      gc.drawLine(440, 0, 440, 24)
    } else {
      gc.setForeground(COLOR_DARK_GRAY)
      gc.drawLine(135, 0, 135, 24)
      gc.drawLine(210, 0, 210, 24)
      gc.drawLine(320, 0, 320, 24)
      gc.drawLine(440, 0, 440, 24)
    }
  }
}
