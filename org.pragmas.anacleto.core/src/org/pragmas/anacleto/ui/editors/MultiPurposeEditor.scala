/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ui.editors

import net.sf.colorer.eclipse.editors.ColorerEditor
import org.pragmas.anacleto.ProjectResources.TreeObject
import org.pragmas.anacleto.ui.Perspective

object MultiPurposeEditor {
  def ID = "org.pragmas.anacleto.ui.editors.MultiPurposeEditor"
}

class MultiPurposeEditor extends ColorerEditor {
  val ID = MultiPurposeEditor.ID

  var projectNode : TreeObject = Perspective.projectManager.currentNode

  override def setPartName(partName : String) : Unit = {
    super.setPartName(partName)
  }

  override def setContentDescription(description : String) : Unit = {
    super.setContentDescription(description)
  }

}
