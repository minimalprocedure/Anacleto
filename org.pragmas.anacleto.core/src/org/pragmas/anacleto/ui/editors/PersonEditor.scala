/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui.editors

import scala.collection.mutable.ListBuffer
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.ModifyListener
import org.eclipse.swt.events.ModifyEvent
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.filesystem.IFileStore
import org.eclipse.swt.SWT
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Table
import org.eclipse.swt.widgets.TableItem
import org.eclipse.swt.widgets.TableColumn
import org.eclipse.swt.custom._
import org.eclipse.ui.IEditorInput
import org.eclipse.ui.IEditorSite
import org.eclipse.ui.IEditorPart
import org.eclipse.ui.part.EditorPart
import org.eclipse.swt.widgets.Text
import org.eclipse.ui.forms.widgets.FormToolkit
import org.eclipse.swt.widgets.Label
import org.eclipse.core.filesystem.URIUtil
import org.eclipse.ui.forms._
import org.eclipse.ui.forms.widgets._
import org.eclipse.ui.forms.events._
import org.pragmas.anacleto.ProjectResources._
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.models._
import com.hp.hpl.jena.rdf.model._
import org.pragmas.anacleto.ui.views.ProjectManager
import org.eclipse.ui.PlatformUI
import org.eclipse.swt.widgets.FileDialog
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Combo
import org.eclipse.swt.widgets.Control
import org.eclipse.ui.ISaveablePart2
import org.pragmas.anacleto.models.LocationRdfModel

object PersonEditor {
  def ID = "org.pragmas.anacleto.ui.editors.PersonEditor"
  var editor : PersonEditor = _
}

class PersonEditor extends EditorPart with IExpansionListener with EditorTools with ISaveablePart2 {

  import org.pragmas.anacleto.implicits.Implicits._

  val ID = PersonEditor.ID

  val KEY_DATA_NODE_STORE = "nodestored"
  val KEY_EDITOR_STORE_PREFIX = "editor_"

  val LOCATION_REF_TYPE_CELL = 3
  val LOCATION_REF_GRADE_CELL = 4
  val PERSON_REF_TYPE_CELL = 3
  val PERSON_REF_GRADE_CELL = 4

  val personModel = new PersonRdfModel

  var textSubject : Text = null
  var textCreator : Text = null
  var textDescription : Text = null

  var textGivenName : Text = null
  var textFamilyName : Text = null
  var textNickName : Text = null
  var textBio : Text = null

  var textBirthDateDay : Text = null
  var textBirthDateMonth : Text = null
  var textBirthDateYear : Text = null
  var textBirthDataLocation : Text = null
  var textBirthDataCountry : Text = null
  var textBirthDataState : Text = null
  var chkBirthDateType : Button = null

  var textDeathDateDay : Text = null
  var textDeathDateMonth : Text = null
  var textDeathDateYear : Text = null
  var textDeathDataLocation : Text = null
  var textDeathDataCountry : Text = null
  var textDeathDataState : Text = null
  var chkDeathDateType : Button = null

  var tableLocationsRelations : Table = null
  var tablePersonsRelations : Table = null

  var chkDeathAliveStatus : Button = null

  private var _isDirty : Boolean = false
  private var _isSaveAsAllowed : Boolean = true

  var projectNode : TreeObject = Perspective.projectManager.currentNode
  private var personsContainer : TreeParent = Perspective.projectManager.personsContainer
  private var locationsContainer : TreeParent = Perspective.projectManager.locationsContainer

  var parent : Composite = _
  var form : ScrolledForm = _
  var formBody : Composite = _

  private def initPersonalPart : Unit = {
    val group = toolkit.createComposite(formBody, SWT.NONE)
    val layout = new ColumnLayout
    group.setLayout(createFixedColumnLayout(2))

    textGivenName = labeledText(group, "Name")
    textFamilyName = labeledText(group, "Family name")
    textNickName = labeledText(group, "Nick name")
    textBio = labeledTextArea(group, "Bio")
  }

  private def initNodeMetadataPart : Unit = {
    val expandable = toolkit.createSection(formBody, ExpandableComposite.TWISTIE)
    expandable.setText("Metadata")

    expandable.addExpansionListener(this)

    val group = toolkit.createComposite(expandable, SWT.NONE)
    group.setLayout(createFixedColumnLayout(2))

    textSubject = labeledText(group, "Subject", projectNode.property(APRJNames.subject).toString)
    textCreator = labeledText(group, "Creator", projectNode.property(APRJNames.creator).toString)
    textDescription = labeledTextArea(group, "Description", projectNode.property(APRJNames.description).toString)

    expandable.setClient(group)
  }

  private def initBirth(group : Composite) : Unit = {
    val expandableBirth = toolkit.createSection(group, ExpandableComposite.TWISTIE)
    expandableBirth.setText("Birth")
    expandableBirth.addExpansionListener(this)

    val birthCompo = toolkit.createComposite(expandableBirth, SWT.NONE)
    birthCompo.setLayout(createFixedColumnLayout(1))

    val birthDateGroup = toolkit.createComposite(birthCompo, SWT.NONE)
    val birthDataGroup = toolkit.createComposite(birthCompo, SWT.NONE)

    birthDateGroup.setLayout(createFixedColumnLayout(4))
    birthDataGroup.setLayout(createFixedColumnLayout(1))

    toolkit.createLabel(birthDateGroup, "Date")
    textBirthDateDay = labeledText(birthDateGroup, "Day", "")
    textBirthDateMonth = labeledText(birthDateGroup, "Month", "")
    textBirthDateYear = labeledText(birthDateGroup, "Year", "")
    chkBirthDateType = toolkit.createButton(birthDateGroup, "Julian", SWT.CHECK)

    textBirthDataLocation = labeledText(birthDataGroup, "Location", "")
    textBirthDataCountry = labeledText(birthDataGroup, "Country", "")
    textBirthDataState = labeledText(birthDataGroup, "State", "")

    expandableBirth.setClient(birthCompo)
  }

  private def initDeath(group : Composite) : Unit = {
    val expandableDeath = toolkit.createSection(group, ExpandableComposite.TWISTIE)
    expandableDeath.setText("Death")
    expandableDeath.addExpansionListener(this)

    val deathCompo = toolkit.createComposite(expandableDeath, SWT.NONE)
    deathCompo.setLayout(createFixedColumnLayout(1))

    val deathDateGroup = toolkit.createComposite(deathCompo, SWT.NONE)
    val deathDataGroup = toolkit.createComposite(deathCompo, SWT.NONE)

    deathDateGroup.setLayout(createFixedColumnLayout(4))
    deathDataGroup.setLayout(createFixedColumnLayout(1))

    toolkit.createLabel(deathDateGroup, "Date")
    chkDeathAliveStatus = toolkit.createButton(deathDateGroup, "Alive", SWT.CHECK)

    chkDeathAliveStatus.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e : SelectionEvent) : Unit = {
        val selected = chkDeathAliveStatus.getSelection
        textDeathDateDay.setEnabled(!selected)
        textDeathDateMonth.setEnabled(!selected)
        textDeathDateYear.setEnabled(!selected)
        chkDeathDateType.setEnabled(!selected)
        textDeathDataLocation.setEnabled(!selected)
        textDeathDataCountry.setEnabled(!selected)
        textDeathDataState.setEnabled(!selected)
      }
    })

    textDeathDateDay = labeledText(deathDateGroup, "Day", "")
    textDeathDateMonth = labeledText(deathDateGroup, "Month", "")
    textDeathDateYear = labeledText(deathDateGroup, "Year", "")
    chkDeathDateType = toolkit.createButton(deathDateGroup, "Julian", SWT.CHECK)

    textDeathDataLocation = labeledText(deathDataGroup, "Location", "")
    textDeathDataCountry = labeledText(deathDataGroup, "Country", "")
    textDeathDataState = labeledText(deathDataGroup, "State", "")

    expandableDeath.setClient(deathCompo)
  }

  private def initBirthDeathPart : Unit = {

    val expandable = toolkit.createSection(formBody, ExpandableComposite.TWISTIE)
    expandable.setText("Birth and Death")

    expandable.addExpansionListener(this)

    val group = toolkit.createComposite(expandable, SWT.NONE)
    group.setLayout(createFixedColumnLayout(2))

    initBirth(group)
    initDeath(group)

    expandable.setClient(group)
  }

  private def attachComboEditor(editor : TableEditor, pos : Int, list : List[String], selection : Int = 0) : Unit = {
    val combo = new Combo(editor.getItem.getParent, SWT.NONE | SWT.READ_ONLY)
    combo.setItems(list.toArray)
    combo.select(selection)
    combo.addModifyListener(new ModifyListener {
      def modifyText(e : ModifyEvent) : Unit = {
        if (combo.getItems.size > 0) {
          val combo = editor.getEditor.asInstanceOf[Combo]
          editor.getItem.setText(pos, combo.getText)
          setDirty(true)
        }
      }
    })
    editor.grabHorizontal = true
    editor.setEditor(combo, editor.getItem, pos)
    editor.getItem.setText(pos, combo.getText)
  }

  private def createTable(
    parent : Composite, style : Int, headers : List[String],
    rows : List[List[Any]], comboDataNum : Int = 0,
    width : Int = WIDGET_BASE_WIDTH /*SWT.DEFAULT*/ , height : Int = 200) : Table = {

    val table = new Table(parent, style)

    table.setLinesVisible(true)
    table.setHeaderVisible(true)
    val layoutData = new ColumnLayoutData
    layoutData.heightHint = height
    layoutData.widthHint = width
    table.setLayoutData(layoutData)
    val headersNum = headers.size - 1
    for (header <- headers) new TableColumn(table, SWT.BORDER).setText(header)

    for (
      row <- rows;
      tItem = new TableItem(table, SWT.NONE);
      cell <- headers;
      limit = headers.size - comboDataNum;
      index <- 0 until limit + 1
    ) {
      if (index < limit) tItem.setText(index, row(index).asInstanceOf[String])
      else
        for (indexCombo <- 0 until comboDataNum) {
          val editor = new TableEditor(table)
          editor.setItem(tItem)
          tItem.setData(KEY_EDITOR_STORE_PREFIX + (index + indexCombo), editor)
        }
      tItem.setData(KEY_DATA_NODE_STORE, row(row.size - 1))
    }

    for (index <- 0 to headers.size - 1) table.getColumn(index).pack

    toolkit.adapt(table, true, true)
    table
  }

  //TODO: loadLocationModelByPropertyValue
  private def loadLocationModelByPropertyValue(prop : String, value : String) : LocationRdfModel = {
    val projectNode = ProjectNodeTools.getChildByPropertyValue(prop, value, ProjectManager.projectRoot)
    val loc = new LocationRdfModel
    loc.loadModelFromUri(projectNode.currentURI.toString)
    loc
  }

  private def listLocations : List[List[Any]] = {
    val locations = locationsContainer.children
    var result = ListBuffer[List[Any]]()
    for (location <- locations) {
      val locationName = location.property(APRJLocationNames.name)
      val locationSubject = location.property(APRJLocationNames.subject)
      val locationModel = loadLocationModelByPropertyValue(APRJNames.name, locationName.toString)
      result += List[Any](
        locationModel.loadValue(locationName.toString, APRJLocationNames.name),
        locationModel.loadValue(locationName.toString, APRJLocationNames.country),
        locationModel.loadValue(locationName.toString, APRJLocationNames.state),
        if (locationSubject != null) locationSubject.toString else "-",
        location)
    }
    result.toList
  }

  private def initLocationsRelations(group : Composite) : Unit = {
    val expandable = toolkit.createSection(group, ExpandableComposite.TWISTIE)
    expandable.setText("Locations")
    expandable.addExpansionListener(this)

    val compo = toolkit.createComposite(expandable, SWT.NONE)
    compo.setLayout(createFixedColumnLayout(1))

    tableLocationsRelations = createTable(compo, SWT.CHECK | SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION,
      List[String](
        APRJLocationNames.name.humanize,
        APRJLocationNames.country.humanize,
        APRJLocationNames.state.humanize,
        APRJPersonNames.referenceGrade.humanize,
        APRJLocationNames.relationTypes.humanize),
      listLocations, 2)
    expandable.setClient(compo)
  }

  private def loadPersonModelByPropertyValue(prop : String, value : String) : PersonRdfModel = {
    val projectNode = ProjectNodeTools.getChildByPropertyValue(prop, value, ProjectManager.projectRoot)
    val person = new PersonRdfModel
    person.loadModelFromUri(projectNode.currentURI.toString)
    person
  }

  //TODO: errore in listPersons
  private def listPersons : List[List[Any]] = {
    val persons = personsContainer.children
    var result = ListBuffer[List[Any]]()
    for (person <- persons) {
      //val givenName = person.property(APRJPersonNames.givenName)
      val givenName = person.property(APRJPersonNames.name)
      println(givenName)
      val familyName = person.property(APRJPersonNames.familyName)
      val personModel = loadPersonModelByPropertyValue(APRJNames.name, givenName.toString)
//            result += List[Any](
//              person.name,
//              if (givenName != null) givenName.toString else "-",
//              if (familyName != null) familyName.toString else "-",
//              person)
      result += List[Any](
        personModel.loadValue(givenName.toString, APRJPersonNames.givenName),
        personModel.loadValue(givenName.toString, APRJPersonNames.familyName),
        personModel.loadValue(givenName.toString, APRJPersonNames.nick),
        person)
    }
    result.toList
  }

  private def initPersonRelations(group : Composite) : Unit = {
    val expandable = toolkit.createSection(group, ExpandableComposite.TWISTIE)
    expandable.setText("Persons")
    expandable.addExpansionListener(this)

    val compo = toolkit.createComposite(expandable, SWT.NONE)
    compo.setLayout(createFixedColumnLayout(1))

    tablePersonsRelations = createTable(compo, SWT.CHECK | SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION,
      List[String](
        APRJPersonNames.givenName.humanize,
        APRJPersonNames.familyName.humanize,
        APRJPersonNames.nick.humanize,
        APRJPersonNames.referenceType.humanize,
        APRJPersonNames.referenceGrade.humanize),
      listPersons, 2)
    expandable.setClient(compo)
  }

  private def initRelationalPart : Unit = {

    val expandable = toolkit.createSection(formBody, ExpandableComposite.TWISTIE)
    expandable.setText("Relations")

    expandable.addExpansionListener(this)

    val group = toolkit.createComposite(expandable, SWT.NONE)
    group.setLayout(createFixedColumnLayout(2))

    initPersonRelations(group)
    initLocationsRelations(group)

    expandable.setClient(group)
  }

  override def createPartControl(parent : Composite) : Unit = {
    this.parent = parent
    PersonEditor.editor = this
    resolveEditorInput(getEditorInput)
    openResource(projectNode.currentPhisicalPath)
    form = toolkit.createScrolledForm(parent)
    form.setText("Person Editor")
    this.formBody = form.getBody
    formBody.setLayout(createFixedColumnLayout(1))
    initPersonalPart
    initBirthDeathPart
    initRelationalPart
    initNodeMetadataPart
    populateControls
    attachModifyListenerToAllToControls(parent) { setDirty(true) }
  }

  override def setPartName(partName : String) : Unit = {
    super.setPartName(partName)
  }

  override def setContentDescription(description : String) : Unit = {
    super.setContentDescription(description)
  }

  override def init(site : IEditorSite, input : IEditorInput) : Unit = {
    setSite(site)
    setInput(input)
  }
  override def setFocus : Unit = {}

  def setSaveAsAllowed(value : Boolean) : Unit = _isSaveAsAllowed = value
  override def isSaveAsAllowed : Boolean = _isSaveAsAllowed

  def setDirty(value : Boolean) : Unit = {
    _isDirty = value
    firePropertyChange(IEditorPart.PROP_DIRTY)
  }
  override def isDirty : Boolean = _isDirty

  private def fileSelectionChoice(filterExtensions : Map[String, String]) : String = {
    var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
    val dialog = new FileDialog(shell, SWT.SAVE)
    val extensions = filterExtensions.keys.toList.toArray
    val extensionNames = filterExtensions.values.toList.toArray
    dialog.setFilterExtensions(extensions)
    dialog.setFilterNames(extensionNames)
    val fileSelected = dialog.open
    if (fileSelected != null) fileSelected else null
  }

  override def doSaveAs : Unit = {
    saveResource(null)
  }

  private def codeLocationModel(name : String) : LocationRdfModel = {
    val locationModel = new LocationRdfModel
    val data = projectNode.store ++ Map(
      APRJLocationNames.name -> (if (name == APRJPersonNames.birthLocation) textBirthDataLocation.getText else textDeathDataLocation.getText),
      APRJLocationNames.title -> APRJLocationNames.name,
      APRJLocationNames.country -> (if (name == APRJPersonNames.birthLocation) textBirthDataCountry.getText else textDeathDataCountry.getText),
      APRJLocationNames.state -> (if (name == APRJPersonNames.birthLocation) textBirthDataState.getText else textDeathDataState.getText))

    val locationResource = locationModel.createLocation(name, data)
    //locationModel.addGeoLocation(locationResource, textLocationGeoLat.getText, textLocationGeoLong.getText)
    locationModel
  }

  private def recreateModelDump : String = {
    val personModel = new PersonRdfModel
    val personResource = personModel.createPerson(projectNode.property(APRJPersonNames.name).toString, projectNode.store)
    personModel.dump
  }

  private def prepareModelDump : String = {
    val personModel = new PersonRdfModel

    val data = projectNode.store ++ Map(
      APRJPersonNames.givenName -> textGivenName.getText,
      APRJPersonNames.familyName -> textFamilyName.getText,
      APRJPersonNames.nick -> textNickName.getText,
      APRJPersonNames.bio -> textBio.getText)

    val personResource = personModel.createPerson(projectNode.property(APRJPersonNames.name).toString, data)

    personModel.addBirthDate(personResource,
      textBirthDateYear.getText.toIntSafe,
      textBirthDateMonth.getText.toIntSafe,
      textBirthDateDay.getText.toIntSafe,
      if (chkBirthDateType.getSelection) APRJDateTimeNames.typeJulian else APRJDateTimeNames.typeGregorian)

    if (chkDeathAliveStatus.getSelection) {
      personModel.addDeathDate(personResource,
        -1,
        -1,
        -1,
        null,
        APRJDateTimeNames.statusAlive)
    } else {
      personModel.addDeathDate(personResource,
        textDeathDateYear.getText.toIntSafe,
        textDeathDateMonth.getText.toIntSafe,
        textDeathDateDay.getText.toIntSafe,
        if (chkDeathDateType.getSelection) APRJDateTimeNames.typeJulian else APRJDateTimeNames.typeGregorian)
    }

    personModel.addLocation(personResource, codeLocationModel(APRJPersonNames.birthLocation), APRJPersonNames.birthLocation)
    personModel.addLocation(personResource, codeLocationModel(APRJPersonNames.deathLocation), APRJPersonNames.deathLocation)

    var tableItems = tablePersonsRelations.getItems
    val personsRelations = ListBuffer[Map[String, String]]()
    tableItems foreach { tableItem : TableItem =>
      val data = tableItem.getData(KEY_DATA_NODE_STORE).asInstanceOf[TreeObject]
      if (tableItem.getChecked) {
        personsRelations += Map[String, String](
          APRJPersonNames.reference -> data.currentRelativePhisicalPath,
          APRJPersonNames.referenceType -> tableItem.getText(PERSON_REF_TYPE_CELL),
          APRJPersonNames.referenceGrade -> tableItem.getText(PERSON_REF_GRADE_CELL))
      }
    }
    personModel.addReferences(personResource, APRJPersonNames.Persons, personsRelations.toList)

    tableItems = tableLocationsRelations.getItems
    val locationsRelations = ListBuffer[Map[String, String]]()
    tableItems foreach { tableItem : TableItem =>
      val data = tableItem.getData(KEY_DATA_NODE_STORE).asInstanceOf[TreeObject]
      if (tableItem.getChecked) {
        locationsRelations += Map[String, String](
          APRJPersonNames.reference -> data.currentRelativePhisicalPath,
          APRJPersonNames.referenceType -> tableItem.getText(LOCATION_REF_TYPE_CELL),
          APRJPersonNames.referenceGrade -> tableItem.getText(LOCATION_REF_GRADE_CELL))
      }
    }
    personModel.addReferences(personResource, APRJPersonNames.Locations, locationsRelations.toList)

    personModel.dump
  }

  private def loadValue(prop : String) : String = {
    val value = personModel.findStatementValue(projectNode.property(APRJPersonNames.name).toString, prop)
    if (value != null) value.toString else ""
  }

  private def populateControls : Unit = {
    val rootName = projectNode.property(APRJPersonNames.name).toString
    //TODO: migliorare il check!!!
    //if (personModel.findStatementValue(rootName, APRJPersonNames.nick) != null) {
    textGivenName.setText(personModel.getGivenName(rootName))
    textFamilyName.setText(personModel.getFamilyName(rootName))
    textNickName.setText(personModel.getNickName(rootName))
    textBio.setText(personModel.getBio(rootName))

    var date = personModel.getDate(rootName)
    if (date.contains(APRJDateTimeNames.year)) textBirthDateYear.setText(date(APRJDateTimeNames.year))
    if (date.contains(APRJDateTimeNames.month)) textBirthDateMonth.setText(date(APRJDateTimeNames.month))
    if (date.contains(APRJDateTimeNames.day)) textBirthDateDay.setText(date(APRJDateTimeNames.day))
    if (date.contains(APRJDateTimeNames.dateType)) {
      chkBirthDateType.setSelection(
        if (date(APRJDateTimeNames.dateType) == APRJDateTimeNames.typeJulian) true else false)
      chkBirthDateType.notifyListeners(SWT.Selection, null)
    }

    date = personModel.getDate(rootName, APRJPersonNames.deathDate)
    if (date.contains(APRJDateTimeNames.year)) textDeathDateYear.setText(date(APRJDateTimeNames.year))
    if (date.contains(APRJDateTimeNames.month)) textDeathDateMonth.setText(date(APRJDateTimeNames.month))
    if (date.contains(APRJDateTimeNames.day)) textDeathDateDay.setText(date(APRJDateTimeNames.day))
    if (date.contains(APRJDateTimeNames.dateType)) {
      chkDeathDateType.setSelection(
        if (date(APRJDateTimeNames.dateType) == APRJDateTimeNames.typeJulian) true else false)
      chkDeathDateType.notifyListeners(SWT.Selection, null)
    }
    if (date.contains(APRJDateTimeNames.status)) {
      chkDeathAliveStatus.setSelection(
        if (date(APRJDateTimeNames.status) == APRJDateTimeNames.statusAlive) true else false)
      chkDeathAliveStatus.notifyListeners(SWT.Selection, null)
    }

    var location = personModel.getLocation(rootName)
    if (location.contains(APRJLocationNames.name)) textBirthDataLocation.setText(location(APRJLocationNames.name))
    if (location.contains(APRJLocationNames.country)) textBirthDataCountry.setText(location(APRJLocationNames.country))
    if (location.contains(APRJLocationNames.state)) textBirthDataState.setText(location(APRJLocationNames.state))

    location = personModel.getLocation(rootName, APRJPersonNames.deathDate)
    if (location.contains(APRJLocationNames.name)) textDeathDataLocation.setText(location(APRJLocationNames.name))
    if (location.contains(APRJLocationNames.country)) textDeathDataCountry.setText(location(APRJLocationNames.country))
    if (location.contains(APRJLocationNames.state)) textDeathDataState.setText(location(APRJLocationNames.state))

    val personsReferences = personModel.getReferences(rootName, APRJPersonNames.Persons)
    tablePersonsRelations.getItems foreach { item =>
      val data = item.getData(KEY_DATA_NODE_STORE).asInstanceOf[TreeObject]
      val columns = tablePersonsRelations.getColumns.size

      for (ref <- personsReferences if (ref(APRJPersonNames.reference) == data.currentRelativePhisicalPath)) {
        item.setChecked(true)
        val selectionType = APRJPersonNames.referenceTypesList.indexOf(ref(APRJPersonNames.referenceType))
        val selectionGrade = APRJPersonNames.referenceGradeList.indexOf(ref(APRJPersonNames.referenceGrade))

        var pos = columns - 2
        var editor = item.getData(KEY_EDITOR_STORE_PREFIX + pos).asInstanceOf[TableEditor]
        attachComboEditor(editor, pos, APRJPersonNames.referenceTypesList, selectionType)

        pos = columns - 1
        editor = item.getData(KEY_EDITOR_STORE_PREFIX + pos).asInstanceOf[TableEditor]
        attachComboEditor(editor, pos, APRJPersonNames.referenceGradeList, selectionGrade)
      }

      if (!item.getChecked) {
        var pos = columns - 2
        var editor = item.getData(KEY_EDITOR_STORE_PREFIX + pos).asInstanceOf[TableEditor]
        attachComboEditor(editor, pos, APRJPersonNames.referenceTypesList, 0)

        pos = columns - 1
        editor = item.getData(KEY_EDITOR_STORE_PREFIX + pos).asInstanceOf[TableEditor]
        attachComboEditor(editor, pos, APRJPersonNames.referenceGradeList, 0)
      }
    }

    val locationsReferences = personModel.getReferences(rootName, APRJPersonNames.Locations)
    tableLocationsRelations.getItems foreach { item =>
      val data = item.getData(KEY_DATA_NODE_STORE).asInstanceOf[TreeObject]
      val columns = tableLocationsRelations.getColumns.size

      for (ref <- locationsReferences if (ref(APRJPersonNames.reference) == data.currentRelativePhisicalPath)) {
        item.setChecked(true)
        val selectionType = APRJLocationNames.referenceTypesList.indexOf(ref(APRJPersonNames.referenceType))
        val selectionGrade = APRJLocationNames.referenceGradeList.indexOf(ref(APRJPersonNames.referenceGrade))

        var pos = columns - 2
        var editor = item.getData(KEY_EDITOR_STORE_PREFIX + pos).asInstanceOf[TableEditor]
        attachComboEditor(editor, pos, APRJLocationNames.referenceTypesList, selectionType)

        pos = columns - 1
        editor = item.getData(KEY_EDITOR_STORE_PREFIX + pos).asInstanceOf[TableEditor]
        attachComboEditor(editor, pos, APRJLocationNames.referenceGradeList, selectionGrade)
      }

      if (!item.getChecked) {
        var pos = columns - 2
        var editor = item.getData(KEY_EDITOR_STORE_PREFIX + pos).asInstanceOf[TableEditor]
        attachComboEditor(editor, pos, APRJLocationNames.referenceTypesList, 0)

        pos = columns - 1
        editor = item.getData(KEY_EDITOR_STORE_PREFIX + pos).asInstanceOf[TableEditor]
        attachComboEditor(editor, pos, APRJLocationNames.referenceGradeList, 0)
      }
    }
  }

  private def openResource(path : String = null) : Unit = {
    if (path != null) {
      val uri = URIUtil.toURI(path).toString
      val store = EFS.getLocalFileSystem.getStore(new Path(path))
      if (store.fetchInfo.exists == false) saveResource(path, true)
      personModel.loadModelFromUri(uri)
    }
  }

  private def saveResource(path : String = null, recreate : Boolean = false) : Unit = {
    var fileSelected = if (path == null) fileSelectionChoice(Map("person" -> "*.person")) else path
    val store = EFS.getLocalFileSystem.getStore(new Path(fileSelected))
    val input = store.openOutputStream(EFS.NONE, null)
    if (recreate)
      input.write(recreateModelDump.getBytes)
    else
      input.write(prepareModelDump.getBytes)
    input.close
    setDirty(false)
  }

  override def doSave(monitor : IProgressMonitor) : Unit = {
    saveResource(projectNode.currentPhisicalPath)
  }

  override def promptToSaveOnClose : Int = {
    ISaveablePart2.DEFAULT
  }

  override def dispose : Unit = {
    toolkit.dispose
    super.dispose
  }

  override def expansionStateChanged(e : ExpansionEvent) : Unit = {
    form.reflow(true)
  }

  override def expansionStateChanging(e : ExpansionEvent) : Unit = {}
}
