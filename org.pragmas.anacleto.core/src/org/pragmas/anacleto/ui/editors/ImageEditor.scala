/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui.editors

import java.io.File
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.filesystem.IFileStore
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.{
  Platform,
  IPath,
  Path,
  IProgressMonitor
}
import org.eclipse.swt.SWT
import org.eclipse.swt.layout.FormAttachment
import org.eclipse.swt.layout.FormData
import org.eclipse.swt.layout.FormLayout
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.ToolBar
import org.eclipse.ui.IEditorInput
import org.eclipse.ui.IEditorSite
import org.eclipse.ui.part.EditorPart
import org.eclipse.ui.IEditorPart
import org.eclipse.ui.part.FileEditorInput
import org.eclipse.ui.ide.FileStoreEditorInput
import org.eclipse.ui.editors.text.ILocationProvider
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.tools.ResourceManager
import org.pragmas.anacleto.ui.editors.imageEditorContributors._
import org.pragmas.anacleto.ProjectResources._
import org.pragmas.anacleto.extensions._
import org.pragmas.anacleto.models.APRJNames
import org.pragmas.anacleto.ui.views.ProjectManager
import org.eclipse.ui.ISaveablePart2

object ImageEditorImage {
  val SEP_TAG = '@'
  val SEP_EXT = '.'
  val ORIGINAL = "original"
  val DEFAULT = "default"
  val SMALL = "small"
  val MEDIUM = "medium"
  val LARGE = "large"
  val DEFAULT_TAG = SEP_TAG + DEFAULT
  val SMALL_TAG = SEP_TAG + SMALL
  val MEDIUM_TAG = SEP_TAG + MEDIUM
  val LARGE_TAG = SEP_TAG + LARGE
  val DEFAULT_EXT = "png"
  val BASIC_RESOLUTION = 96
  val DEFAULT_RESOLUTION = 0.0
  val SMALL_SIZE = 0.30
  val MEDIUM_SIZE = 0.50
  val LARGE_SIZE = 0.80
  val DEFAULT_SIZE = 100.0

  def DEFAULT_EXT_AND_DOT = "." + DEFAULT_EXT
}

object ImageEditor {
  def ID = "org.pragmas.anacleto.ui.editors.ImageEditor"
  val Extensions = Map(
    "*.*" -> "all files",
    "*.png" -> "image png",
    "*.jpg" -> "image jpg",
    "*.tif" -> "image tif",
    "*.tiff" -> "image tiff")
  var editor : ImageEditor = _
}

class ImageEditor extends EditorPart with ISaveablePart2 {
  def ID = ImageEditor.ID

  var projectNode : TreeObject = Perspective.projectManager.currentNode

  var filterNode : TreeObject = _
  var filtersPath : IPath = null

  var imageFile : File = _
  var sizeTags : List[String] = _
  var canvasData : FormData = _
  private var parent : Composite = _

  var toolBar : ToolBar = _
  var canvas : JMagickCanvas = _
  var status : ImageStatusCanvas = _

  var contentOutlinePage : CustomContentOutlinePage = new CustomContentOutlinePage

  private var _isDirty : Boolean = false
  private var _isSaveAsAllowed : Boolean = true

  def executeFilters : Unit = {
    val filters = contentOutlinePage.rootFilters.getChildren
    val scriptlets =
      for (
        filter <- filters if filter.propertyExist(APRJNames.active);
        if filter.property(APRJNames.active).asInstanceOf[Boolean] == true
      ) yield filter.currentPhisicalPath

    canvas.executeExtensionFilters(scriptlets)
  }

  def loadFilters : TreeParent = {
    val filtersPath = ProjectManager.ProjectPathParentStore.append(ProjectTypes.APPDATA_CONTAINER).append(ProjectTypes.FILTERS)
    val filters = new TreeParent(ProjectTypes.ROOT, ProjectTypes.ROOT)
    if (filtersPath != null) {

      val filtersStore = EFS.getLocalFileSystem.getStore(filtersPath)
      val activeFilters = projectNode.property(ProjectTypes.FILTERS).asInstanceOf[List[String]]
      val filterPathList = ResourceManager.getFilePaths(filtersStore)

      for (filter <- filterPathList) {
        var node : TreeObject = null
        if (activeFilters != null && activeFilters.size > 0) {
          if (activeFilters.contains(filter.lastSegment)) {
            node = new TreeObject(filter.lastSegment, ProjectTypes.IMAGE_FILTER_GO)
            node.property(APRJNames.active, true)
          } else node = new TreeObject(filter.lastSegment, ProjectTypes.IMAGE_FILTER)
        } else node = new TreeObject(filter.lastSegment, ProjectTypes.IMAGE_FILTER)
        node.property(APRJNames.name, filter.lastSegment)
        filters.addChild(node)
      }
    }
    filters
  }

  def isImageDeletable(tag : String) : Boolean = {
    tag match {
      case ImageEditorImage.ORIGINAL
        | ImageEditorImage.DEFAULT
        | ImageEditorImage.SMALL
        | ImageEditorImage.MEDIUM
        | ImageEditorImage.LARGE => false
      case _ => true
    }
  }

  override def getAdapter(required : Class[_]) : AnyRef = {
    var result : AnyRef = null
    val reqName = required.getName
    if (reqName == "org.eclipse.ui.views.contentoutline.IContentOutlinePage") {
      if (contentOutlinePage == null) {
        super.getAdapter(required)
      } else {
        contentOutlinePage.setFiltersNode(loadFilters)
        contentOutlinePage
      }
    } else {
      super.getAdapter(required)
    }
  }

  def resolveImageBySizeTag(imagePath : File, sizetag : String = null) : Unit = {

    val savedSizeTag = if (projectNode.propertyExist(APRJNames.active))
      projectNode.property(APRJNames.active).toString
    else if (sizetag != null) {
      projectNode.property(APRJNames.active, sizetag)
      sizetag
    } else ImageEditorImage.ORIGINAL

    imageFile =
      if (savedSizeTag != ImageEditorImage.ORIGINAL) {
        new File(imagePath.getParent, getProjectNodeSimpleName + ImageEditorImage.SEP_TAG + savedSizeTag + ImageEditorImage.SEP_EXT + ImageEditorImage.DEFAULT_EXT)
      } else {
        new File(imagePath.getParent, projectNode.property(APRJNames.name).asInstanceOf[String])
      }
    if (!imageFile.exists) imageFile = imagePath
  }

  def loadSizeTags : List[String] = {
    val imageContainer = imageFile.getParentFile
    val imageStore = EFS.getLocalFileSystem.getStore(imageContainer.toURI)
    println(getProjectNodeSimpleName)
    val imagesPathList = ResourceManager.getFilePaths(imageStore, getProjectNodeSimpleName)
    sizeTags = imagesPathList.map { e =>
      val segments = e.lastSegment.split(Array(ImageEditorImage.SEP_TAG, ImageEditorImage.SEP_EXT))
      if (segments.size > 2) segments(segments.size - 2) else ImageEditorImage.ORIGINAL
    }
    sizeTags
  }

  def getProjectNodeSimpleName : String = {
    var fileName = projectNode.property(APRJNames.name).asInstanceOf[String]
    val index = fileName.lastIndexOf(".")
    fileName.slice(0, index)
  }

  private def resolveEditorInput : Unit = {
    var editorInput = getEditorInput
    if (editorInput.isInstanceOf[FileStoreEditorInput]) {
      var fileStoreEditorinput = getEditorInput.asInstanceOf[FileStoreEditorInput]
      //imageFile = new File(fileStoreEditorinput.getURI)
      resolveImageBySizeTag(new File(fileStoreEditorinput.getURI))
    } else if (editorInput.isInstanceOf[FileEditorInput]) {
      var fileEditorinput = getEditorInput.asInstanceOf[FileEditorInput]
      var file = getEditorInput
      //imageFile = new File(fileEditorinput.getPath.toOSString)
      resolveImageBySizeTag(new File(fileEditorinput.getPath.toOSString))
    } else if (editorInput.getAdapter(classOf[ILocationProvider]) != null) {
      var location = editorInput.getAdapter(classOf[ILocationProvider]).asInstanceOf[ILocationProvider]
      var path = location.getPath(editorInput).asInstanceOf[IPath]
      //imageFile = new File(path.toOSString)
      resolveImageBySizeTag(new File(path.toOSString))
    } else if (editorInput.getAdapter(classOf[IFile]) != null) {
      var file = editorInput.getAdapter(classOf[IFile]).asInstanceOf[IFile]
      //imageFile = new File(file.getLocation.toOSString)
      resolveImageBySizeTag(new File(file.getLocation.toOSString))
    } else { /* never, maybe */ }
  }

  override def createPartControl(parent : Composite) : Unit = {
    this.parent = parent
    ImageEditor.editor = this
    resolveEditorInput
    initElements
    canvas.loadImage(imageFile.getAbsolutePath)
    status.updateWithCurrent
  }

  private def initToolBar(parent : Composite) : Unit = {
    toolBar = new ImageEditorToolbarManager(parent).toolBar
    var toolBarData = new FormData
    toolBar.setLayoutData(toolBarData)
    toolBarData.top = new FormAttachment(0, 0)
    toolBarData.left = new FormAttachment(0, 0)
    toolBarData.right = new FormAttachment(100, 0)
    //toolBarData
  }

  private def initCanvas(parent : Composite) : Unit = {
    canvas = new JMagickCanvas(parent, SWT.NONE)
    canvasData = new FormData
    canvasData.top = new FormAttachment(toolBar, 0)
    canvasData.bottom = new FormAttachment(100, -18)
    canvasData.right = new FormAttachment(100, 0)
    canvasData.left = new FormAttachment(0, 0)
    canvas.setLayoutData(canvasData)
  }

  private def initStatusBar(parent : Composite) : Unit = {
    status = new ImageStatusCanvas(parent, SWT.NONE)
    var statusData = new FormData
    statusData.top = new FormAttachment(canvas, 0)
    statusData.bottom = new FormAttachment(100, 0)
    statusData.right = new FormAttachment(100, 0)
    statusData.left = new FormAttachment(0, 0)
    status.setLayoutData(statusData)
  }

  private def initElements : Unit = {
    var layout = new FormLayout
    var composite = new Composite(parent, SWT.NONE)
    composite.setLayout(layout)
    composite.setLayoutData(new FormData)
    initToolBar(composite)
    initCanvas(composite)
    initStatusBar(composite)
  }

  override def dispose : Unit = {
    canvas.dispose
    status.dispose
    super.dispose
  }

  override def setPartName(partName : String) : Unit = {
    super.setPartName(partName)
  }

  override def setContentDescription(description : String) : Unit = {
    super.setContentDescription(description)
  }

  def init(site : IEditorSite, input : IEditorInput) : Unit = {
    setSite(site)
    setInput(input)
  }

  def setFocus() : Unit = {
    ImageEditor.editor = this
    if (canvas != null) {
      canvas.setFocus
    }
  }

  def doSave(monitor : IProgressMonitor) {
    Perspective.projectManager.saveProject
    setDirty(false)
  }

  def doSaveAs() : Unit = {}

  def setSaveAsAllowed(value : Boolean) : Unit = _isSaveAsAllowed = value
  override def isSaveAsAllowed : Boolean = _isSaveAsAllowed

  def setDirty(value : Boolean) : Unit = {
    _isDirty = value
    firePropertyChange(IEditorPart.PROP_DIRTY)
  }
  override def isDirty : Boolean = _isDirty

  override def promptToSaveOnClose : Int = {
    ISaveablePart2.DEFAULT
  }

}
