/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui.editors

//import org.eclipse.swt.events.ControlEvent
//import org.eclipse.swt.events.ControlAdapter
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
//import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.events.ModifyListener
import org.eclipse.swt.events.ModifyEvent
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Table
import org.eclipse.swt.widgets.Text
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Combo
import org.eclipse.swt.SWT
import org.eclipse.ui.forms.widgets.ColumnLayout
import org.eclipse.ui.forms.widgets.ColumnLayoutData
import org.eclipse.ui.forms.widgets.FormToolkit
import org.eclipse.swt.widgets.Display
//import org.eclipse.core.filesystem.IFileStore
import org.eclipse.ui.part.FileEditorInput
import org.eclipse.ui.ide.FileStoreEditorInput
import org.eclipse.ui.editors.text.ILocationProvider
import org.eclipse.ui.IEditorInput
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.IPath
import java.io.File

trait EditorTools {

  val WIDGET_BASE_WIDTH = 60 //SWT.DEFAULT

  var inputFile : File = _

  val toolkit = new FormToolkit(Display.getCurrent)
  toolkit.setBorderStyle(SWT.BORDER)


  def resolveEditorInput(getEditorInput : IEditorInput) : Unit = {
    var editorInput = getEditorInput
    if (editorInput.isInstanceOf[FileStoreEditorInput]) {
      var fileStoreEditorinput = getEditorInput.asInstanceOf[FileStoreEditorInput]
      inputFile = new File(fileStoreEditorinput.getURI)
    } else if (editorInput.isInstanceOf[FileEditorInput]) {
      var fileEditorinput = getEditorInput.asInstanceOf[FileEditorInput]
      var file = getEditorInput
      inputFile = new File(fileEditorinput.getPath.toOSString)
    } else if (editorInput.getAdapter(classOf[ILocationProvider]) != null) {
      var location = editorInput.getAdapter(classOf[ILocationProvider]).asInstanceOf[ILocationProvider]
      var path = location.getPath(editorInput).asInstanceOf[IPath]
      inputFile = new File(path.toOSString)
    } else if (editorInput.getAdapter(classOf[IFile]) != null) {
      var file = editorInput.getAdapter(classOf[IFile]).asInstanceOf[IFile]
      inputFile = new File(file.getLocation.toOSString)
    } else { /* never, maybe */ }
    //setDirty(true)
  }

  def labeledText(parent : Composite, caption : String, text : String = "", height : Int = SWT.DEFAULT, width : Int = WIDGET_BASE_WIDTH) : Text = {
    val group = toolkit.createComposite(parent, SWT.NONE)
    group.setLayout(createFixedColumnLayout(1))
    val layoutData = new ColumnLayoutData
    layoutData.heightHint = height
    layoutData.widthHint = width
    toolkit.createLabel(group, caption)
    val textWidget = toolkit.createText(group, text)
    textWidget.setLayoutData(layoutData)
    textWidget
  }

  def labeledTextArea(parent : Composite, caption : String, text : String = "", height : Int = 150, width : Int = WIDGET_BASE_WIDTH) : Text = {
    val group = toolkit.createComposite(parent, SWT.NONE)
    group.setLayout(createFixedColumnLayout(1))

    val layoutData = new ColumnLayoutData
    layoutData.heightHint = height
    layoutData.widthHint = width
    toolkit.createLabel(group, caption)
    val textWidget = toolkit.createText(group, text, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL | SWT.H_SCROLL)
    textWidget.setLayoutData(layoutData)
    textWidget
  }

  def createFixedColumnLayout(fixCols : Int, spacing : Int = 2, margin : Int = 2) : ColumnLayout = {
    val layout = new ColumnLayout
    layout.maxNumColumns = fixCols
    layout.minNumColumns = fixCols
    layout.horizontalSpacing = spacing
    layout.verticalSpacing = spacing
    layout.topMargin = margin
    layout.leftMargin = margin
    layout.bottomMargin = margin
    layout.rightMargin = margin
    layout
  }

  def attachModifyListenerToAllToControls(composite : Composite)(modifyBlock : => Unit) : Unit = {

    val controls = composite.getChildren
    for (control <- controls if (control != null && !control.isDisposed)) {
      if (control.isInstanceOf[Composite]) {
        if (control.isInstanceOf[Table]) {
          control.asInstanceOf[Table].addSelectionListener(new SelectionAdapter {
            override def widgetSelected(e : SelectionEvent) : Unit = {
              if (e.detail == SWT.CHECK) modifyBlock
            }
          })
        } else attachModifyListenerToAllToControls(control.asInstanceOf[Composite]) { modifyBlock }
      } else {
        if (control.isInstanceOf[Text] || control.isInstanceOf[Combo]) {
          control.asInstanceOf[Text].addModifyListener(new ModifyListener {
            override def modifyText(e : ModifyEvent) : Unit = { modifyBlock }
          })
        } else if (control.isInstanceOf[Button]) {
          if (control.isInstanceOf[Button])
            control.asInstanceOf[Button].addSelectionListener(new SelectionAdapter {
              override def widgetSelected(e : SelectionEvent) : Unit = { modifyBlock }
            })
        }
      }
    }
    null
  }

}
