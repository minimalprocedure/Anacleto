/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui.editors

import org.eclipse.mylyn.internal.wikitext.ui.editor.MarkupEditor
import org.pragmas.anacleto.ProjectResources.TreeObject
import org.pragmas.anacleto.ui.Perspective
import org.eclipse.jface.text.BlockTextSelection
import org.eclipse.jface.text.IDocument
import org.pragmas.anacleto.models.APRJNames
import scala.collection.mutable.ListBuffer

import org.pragmas.anacleto.implicits.Jobs._
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Status
import org.pragmas.anacleto.ui.Perspective
import org.eclipse.ui.IPartListener2
import org.eclipse.ui.IEditorSite
import org.eclipse.ui.IEditorInput

object ScriptEditor {
  def ID = "org.pragmas.anacleto.ui.editors.ScriptEditor"
}

class ScriptEditor extends MarkupEditor {
  val ID = ScriptEditor.ID

  //private val NoteParser = """\[\[(.*?):(.*?):(.*?):(.*?)\]\]""".r
  private val MACRO_FINDER = """<(.*?):(.*?):(.*?):(.*?)>""".r

  var projectNode : TreeObject = Perspective.projectManager.currentNode

  override def init(site : IEditorSite, input : IEditorInput) : Unit = {
    super.init(site, input)
    val searchView = Perspective.getActivePage.findView(Perspective.ID_SEARCH_VIEW)
    val macrosView = Perspective.getActivePage.findView(Perspective.MACROS_MANAGER_ID)

    if (searchView != null && searchView.isInstanceOf[IPartListener2])
      site.getPage.addPartListener(
        searchView.asInstanceOf[IPartListener2]
      )

    if (macrosView != null && macrosView.isInstanceOf[IPartListener2])
      site.getPage.addPartListener(
        macrosView.asInstanceOf[IPartListener2]
      )
  }

  override def setFocus : Unit = { super.setFocus }

  override def setPartName(partName : String) : Unit = {
    super.setPartName(partName)
  }

  override def setContentDescription(description : String) : Unit = {
    super.setContentDescription(description)
  }

  override def getCursorPosition : String = {
    super.getCursorPosition
  }

  def getDocument : IDocument = {
    getDocumentProvider.getDocument(getEditorInput)
  }

  def getSource : String = {
    getDocument.get
  }

  def getMacros : ListBuffer[List[String]] = {
    var macroGroups = ListBuffer[List[String]]()
    val macros = MACRO_FINDER.findAllIn(getSource)
    macros.matchData foreach { mData =>
      macroGroups += List(mData.toString) ++ (mData.subgroups map { m : String => m })
    }
    macroGroups
  }

  def setLineSelection(num : Int) : Unit = {
    val document = getDocument
    val selection = new BlockTextSelection(document, num - 1, 0, num, 0, 0)
    doSetSelection(selection)
  }

}
