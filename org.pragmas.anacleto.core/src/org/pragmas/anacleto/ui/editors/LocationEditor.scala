/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui.editors

import java.io.File
//import java.util.Date
//import java.io.IOException
import scala.collection.mutable.ListBuffer
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.Platform
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.swt.events.ControlEvent
import org.eclipse.swt.events.ControlAdapter
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.events.ModifyListener
import org.eclipse.swt.events.ModifyEvent
import org.eclipse.core.filesystem.EFS
import org.eclipse.core.filesystem.IFileStore
import org.eclipse.ui.part.FileEditorInput
import org.eclipse.ui.ide.FileStoreEditorInput
import org.eclipse.ui.editors.text.ILocationProvider
//import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.SWT
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.swt.widgets.Composite
//import org.eclipse.swt.widgets.Table
//import org.eclipse.swt.widgets.TableItem
//import org.eclipse.swt.widgets.TableColumn
//import org.eclipse.swt.widgets.ExpandBar
//import org.eclipse.swt.widgets.ExpandItem
import org.eclipse.swt.custom._
import org.eclipse.ui.IEditorInput
import org.eclipse.ui.IEditorSite
import org.eclipse.ui.IEditorPart
//import org.eclipse.ui.PartInitException
import org.eclipse.ui.part.EditorPart
//import org.eclipse.swt.layout._
import org.eclipse.swt.widgets.Text
import org.eclipse.ui.forms.widgets.FormToolkit
import org.eclipse.swt.widgets.Display
//import org.eclipse.swt.layout.RowLayout
import org.eclipse.swt.widgets.Label

import org.eclipse.core.filesystem.URIUtil

import org.eclipse.ui.forms._
import org.eclipse.ui.forms.widgets._
import org.eclipse.ui.forms.events._
//import org.eclipse.swt.widgets.Layout

import org.pragmas.anacleto.ProjectResources._
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.models._
//import org.pragmas.anacleto.implicits._
import com.hp.hpl.jena.rdf.model._

import org.eclipse.ui.PlatformUI
import org.eclipse.swt.widgets.FileDialog
//import org.eclipse.swt.widgets.DateTime
//import org.eclipse.swt.events.ControlAdapter
//import org.eclipse.swt.events.ControlEvent
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.SelectionAdapter
import org.eclipse.swt.widgets.Button
//import org.eclipse.swt.widgets.Combo
//import org.eclipse.jface.window.Window

import org.eclipse.swt.widgets.Control
//import org.eclipse.ui.views.properties.IPropertySource
import org.eclipse.ui.ISaveablePart2

object LocationEditor {
  def ID = "org.pragmas.anacleto.ui.editors.LocationEditor"
  var editor : LocationEditor = _
}

class LocationEditor extends EditorPart with IExpansionListener with EditorTools with ISaveablePart2 {

  import org.pragmas.anacleto.implicits.Implicits._

  val ID = LocationEditor.ID

  val locationModel = new LocationRdfModel

  var textSubject : Text = null
  var textCreator : Text = null
  var textDescription : Text = null

  var textLocationGeoLat : Text = null
  var textLocationGeoLong : Text = null

  var textLocationName : Text = null
  var textLocationCountry : Text = null
  var textLocationState : Text = null
  var textLocationDescription : Text = null

  var textFoundationDateDay : Text = null
  var textFoundationDateMonth : Text = null
  var textFoundationDateYear : Text = null
  var chkFoundationDateType : Button = null

  var textAbandonmentDateDay : Text = null
  var textAbandonmentDateMonth : Text = null
  var textAbandonmentDateYear : Text = null
  var chkAbandonmentDateType : Button = null

  var chkAbandonmentAliveStatus : Button = null
  var btnLocationFind : Button = null

  private var _isDirty : Boolean = false
  private var _isSaveAsAllowed : Boolean = true

  var projectNode : TreeObject = Perspective.projectManager.currentNode
  private var personsContainer : TreeParent = Perspective.projectManager.personsContainer
  private var locationsContainer : TreeParent = Perspective.projectManager.locationsContainer

  var parent : Composite = _
  var form : ScrolledForm = _
  var formBody : Composite = _

  private def initLocationalPart : Unit = {
    val group = toolkit.createComposite(formBody, SWT.NONE)
    val layout = new ColumnLayout
    group.setLayout(createFixedColumnLayout(2))

    textLocationName = labeledText(group, "Name", "")
    textLocationDescription = labeledTextArea(group, "Description", "")
    textLocationCountry = labeledText(group, "Country", "")
    textLocationState = labeledText(group, "State", "")

    val groupLongLat = toolkit.createComposite(group, SWT.NONE)
    val layoutLongLat = new ColumnLayout
    groupLongLat.setLayout(createFixedColumnLayout(3))
    val point = loadPoint
    textLocationGeoLat = labeledText(groupLongLat, "Latitude", point("lat"))
    textLocationGeoLong = labeledText(groupLongLat, "Longitude", point("long"))
    btnLocationFind = toolkit.createButton(groupLongLat, "Find", SWT.NORMAL)
  }

  private def initNodeMetadataPart : Unit = {
    val expandable = toolkit.createSection(formBody, ExpandableComposite.TWISTIE)
    expandable.setText("Metadata")

    expandable.addExpansionListener(this)

    val group = toolkit.createComposite(expandable, SWT.NONE)
    group.setLayout(createFixedColumnLayout(2))

    textSubject = labeledText(group, "Subject", projectNode.property(APRJNames.subject).toString)
    textCreator = labeledText(group, "Creator", projectNode.property(APRJNames.creator).toString)
    textDescription = labeledTextArea(group, "Description", projectNode.property(APRJNames.description).toString)

    expandable.setClient(group)
  }

  private def initFoundation(group : Composite) : Unit = {
    val expandableFoundation = toolkit.createSection(group, ExpandableComposite.TWISTIE)
    expandableFoundation.setText("Foundation")
    expandableFoundation.addExpansionListener(this)

    val birthCompo = toolkit.createComposite(expandableFoundation, SWT.NONE)
    birthCompo.setLayout(createFixedColumnLayout(1))

    val birthDateGroup = toolkit.createComposite(birthCompo, SWT.NONE)
    val birthDataGroup = toolkit.createComposite(birthCompo, SWT.NONE)

    birthDateGroup.setLayout(createFixedColumnLayout(4))
    birthDataGroup.setLayout(createFixedColumnLayout(1))

    toolkit.createLabel(birthDateGroup, "Date")
    textFoundationDateDay = labeledText(birthDateGroup, "Day", "")
    textFoundationDateMonth = labeledText(birthDateGroup, "Month", "")
    textFoundationDateYear = labeledText(birthDateGroup, "Year", "")
    chkFoundationDateType = toolkit.createButton(birthDateGroup, "Julian", SWT.CHECK)

    expandableFoundation.setClient(birthCompo)
  }

  private def initAbandonment(group : Composite) : Unit = {
    val expandableAbandonment = toolkit.createSection(group, ExpandableComposite.TWISTIE)
    expandableAbandonment.setText("Abandonment")
    expandableAbandonment.addExpansionListener(this)

    val deathCompo = toolkit.createComposite(expandableAbandonment, SWT.NONE)
    deathCompo.setLayout(createFixedColumnLayout(1))

    val deathDateGroup = toolkit.createComposite(deathCompo, SWT.NONE)
    val deathDataGroup = toolkit.createComposite(deathCompo, SWT.NONE)

    deathDateGroup.setLayout(createFixedColumnLayout(4))
    deathDataGroup.setLayout(createFixedColumnLayout(1))

    toolkit.createLabel(deathDateGroup, "Date")
    chkAbandonmentAliveStatus = toolkit.createButton(deathDateGroup, "Alive", SWT.CHECK)

    chkAbandonmentAliveStatus.addSelectionListener(new SelectionAdapter {
      override def widgetSelected(e : SelectionEvent) : Unit = {
        val selected = chkAbandonmentAliveStatus.getSelection
        textAbandonmentDateDay.setEnabled(!selected)
        textAbandonmentDateMonth.setEnabled(!selected)
        textAbandonmentDateYear.setEnabled(!selected)
        chkAbandonmentDateType.setEnabled(!selected)
      }
    })

    textAbandonmentDateDay = labeledText(deathDateGroup, "Day", "")
    textAbandonmentDateMonth = labeledText(deathDateGroup, "Month", "")
    textAbandonmentDateYear = labeledText(deathDateGroup, "Year", "")
    chkAbandonmentDateType = toolkit.createButton(deathDateGroup, "Julian", SWT.CHECK)

    expandableAbandonment.setClient(deathCompo)
  }

  private def initFoundationAbandonmentPart : Unit = {

    val expandable = toolkit.createSection(formBody, ExpandableComposite.TWISTIE)
    expandable.setText("Foundation")

    expandable.addExpansionListener(this)

    val group = toolkit.createComposite(expandable, SWT.NONE)
    group.setLayout(createFixedColumnLayout(2))

    initFoundation(group)
    initAbandonment(group)

    expandable.setClient(group)
  }

  private def listLocations : List[List[String]] = {
    val locations = locationsContainer.children
    var result = ListBuffer[List[String]]()
    for (location <- locations) {
      val locationName = location.property(APRJLocationNames.name)
      val locationSubject = location.property(APRJLocationNames.subject)
      result += List[String](location.name,
        if (locationName != null) locationName.toString else "-",
        if (locationSubject != null) locationSubject.toString else "-")
    }
    result.toList
  }

  override def createPartControl(parent : Composite) : Unit = {
    this.parent = parent
    LocationEditor.editor = this
    resolveEditorInput(getEditorInput)
    openResource(projectNode.currentPhisicalPath)
    form = toolkit.createScrolledForm(parent)
    form.setText("Location Editor")
    this.formBody = form.getBody
    formBody.setLayout(createFixedColumnLayout(1))
    initLocationalPart
    initFoundationAbandonmentPart
    initNodeMetadataPart
    //attachModifyListenerToAllToControls(parent) {setDirty(true)}
    populateControls
    attachModifyListenerToAllToControls(parent) { setDirty(true) }
  }

  override def setPartName(partName : String) : Unit = {
    super.setPartName(partName)
  }

  override def setContentDescription(description : String) : Unit = {
    super.setContentDescription(description)
  }

  override def init(site : IEditorSite, input : IEditorInput) : Unit = {
    setSite(site)
    setInput(input)
  }
  override def setFocus : Unit = {}

  def setSaveAsAllowed(value : Boolean) : Unit = _isSaveAsAllowed = value
  override def isSaveAsAllowed : Boolean = _isSaveAsAllowed

  def setDirty(value : Boolean) : Unit = {
    _isDirty = value
    firePropertyChange(IEditorPart.PROP_DIRTY)
  }
  override def isDirty : Boolean = _isDirty

  private def fileSelectionChoice(filterExtensions : Map[String, String]) : String = {
    var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
    val dialog = new FileDialog(shell, SWT.SAVE)
    val extensions = filterExtensions.keys.toList.toArray
    val extensionNames = filterExtensions.values.toList.toArray
    dialog.setFilterExtensions(extensions)
    dialog.setFilterNames(extensionNames)
    val fileSelected = dialog.open
    if (fileSelected != null) fileSelected else null
  }

  override def doSaveAs : Unit = {
    saveResource(null)
  }

  private def prepareModelDump : String = {
    val locationModel = new LocationRdfModel

    val data = projectNode.store ++ Map(
      APRJLocationNames.name -> textLocationName.getText,
      APRJLocationNames.title -> textLocationName.getText,
      APRJLocationNames.country -> textLocationCountry.getText,
      APRJLocationNames.state -> textLocationState.getText)

    val locationResource = locationModel.createLocation(projectNode.property(APRJLocationNames.name).toString, data)
    locationModel.addGeoLocation(locationResource, textLocationGeoLat.getText, textLocationGeoLong.getText)

    locationModel.addFoundationDate(locationResource,
      textFoundationDateYear.getText.toIntSafe,
      textFoundationDateMonth.getText.toIntSafe,
      textFoundationDateDay.getText.toIntSafe,
      if (chkFoundationDateType.getSelection) APRJDateTimeNames.typeJulian else APRJDateTimeNames.typeGregorian)

    if (chkAbandonmentAliveStatus.getSelection) {
      locationModel.addAbandonmentDate(locationResource,
        -1,
        -1,
        -1,
        null,
        APRJDateTimeNames.statusAlive)
    } else {
      locationModel.addAbandonmentDate(locationResource,
        textAbandonmentDateYear.getText.toIntSafe,
        textAbandonmentDateMonth.getText.toIntSafe,
        textAbandonmentDateDay.getText.toIntSafe,
        if (chkAbandonmentDateType.getSelection) APRJDateTimeNames.typeJulian else APRJDateTimeNames.typeGregorian)
    }

    locationModel.dump
  }

  private def populateControls : Unit = {

    val rootName = projectNode.property(APRJLocationNames.name).toString

    textLocationName.setText(loadValue(APRJLocationNames.title))
    textLocationDescription.setText(loadValue(APRJLocationNames.description))
    textLocationCountry.setText(loadValue(APRJLocationNames.country))
    textLocationState.setText(loadValue(APRJLocationNames.state))

    val point = loadPoint
    textLocationGeoLat.setText(point("lat"))
    textLocationGeoLong.setText(point("long"))

    var date = locationModel.getDate(rootName)
    if (date.contains(APRJDateTimeNames.year)) textFoundationDateYear.setText(date(APRJDateTimeNames.year))
    if (date.contains(APRJDateTimeNames.month)) textFoundationDateMonth.setText(date(APRJDateTimeNames.month))
    if (date.contains(APRJDateTimeNames.day)) textFoundationDateDay.setText(date(APRJDateTimeNames.day))
    if (date.contains(APRJDateTimeNames.dateType)) {
      chkFoundationDateType.setSelection(
        if (date(APRJDateTimeNames.dateType) == APRJDateTimeNames.typeJulian) true else false)
      chkFoundationDateType.notifyListeners(SWT.Selection, null)
    }

    date = locationModel.getDate(rootName, APRJLocationNames.abandonmentDate)
    if (date.contains(APRJDateTimeNames.year)) textAbandonmentDateYear.setText(date(APRJDateTimeNames.year))
    if (date.contains(APRJDateTimeNames.month)) textAbandonmentDateMonth.setText(date(APRJDateTimeNames.month))
    if (date.contains(APRJDateTimeNames.day)) textAbandonmentDateDay.setText(date(APRJDateTimeNames.day))
    if (date.contains(APRJDateTimeNames.dateType)) {
      chkAbandonmentDateType.setSelection(
        if (date(APRJDateTimeNames.dateType) == APRJDateTimeNames.typeJulian) true else false)
      chkAbandonmentDateType.notifyListeners(SWT.Selection, null)
    }
    if (date.contains(APRJDateTimeNames.status)) {
      chkAbandonmentAliveStatus.setSelection(
        if (date(APRJDateTimeNames.status) == APRJDateTimeNames.statusAlive) true else false)
      chkAbandonmentAliveStatus.notifyListeners(SWT.Selection, null)
    }
  }

  private def openResource(path : String = null) : Unit = {
    if (path != null) {
      val uri = URIUtil.toURI(path).toString
      locationModel.loadModelFromUri(uri)
    }
  }

  private def loadValue(prop : String) : String = {
    //val value = locationModel.findStatementValue(projectNode.property(APRJLocationNames.name).toString, prop)
    //if (value != null) value.toString else ""
    locationModel.loadValue(projectNode.property(APRJLocationNames.name).toString, prop)
  }

  private def loadPoint : Map[String, String] = {
    val point = locationModel.findStatementValue(projectNode.property(APRJLocationNames.name).toString, APRJLocationNames.Point)
    var pointData = Map[String, String](APRJLocationNames.lat -> "0.00", APRJLocationNames.long -> "0.00")
    if (point != null) {
      pointData = pointData.updated(APRJLocationNames.lat, locationModel.findStatementValue(point, APRJLocationNames.lat).toString)
      pointData = pointData.updated(APRJLocationNames.long, locationModel.findStatementValue(point, APRJLocationNames.long).toString)
    }
    pointData
  }

  private def saveResource(path : String = null) : Unit = {
    var fileSelected = if (path == null) fileSelectionChoice(Map("location" -> "*.location")) else path
    val store = EFS.getLocalFileSystem.getStore(new Path(fileSelected))
    val input = store.openOutputStream(EFS.NONE, null)
    input.write(prepareModelDump.getBytes)
    input.close
    setDirty(false)
  }

  override def doSave(monitor : IProgressMonitor) : Unit = {
    saveResource(projectNode.currentPhisicalPath)
  }

  override def promptToSaveOnClose : Int = {
    ISaveablePart2.DEFAULT
  }

  override def isSaveOnCloseNeeded : Boolean = { true }

  override def dispose : Unit = {
    toolkit.dispose
    super.dispose
  }

  override def expansionStateChanged(e : ExpansionEvent) : Unit = {
    form.reflow(true)
  }

  override def expansionStateChanging(e : ExpansionEvent) : Unit = {}

}
