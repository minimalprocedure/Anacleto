/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ui.PropertySheetPages

import org.eclipse.ui.views.properties.PropertySheet
import org.eclipse.ui.IWorkbenchPart
import org.eclipse.ui.IPageLayout
import org.eclipse.jface.action.ActionContributionItem
import org.eclipse.ui.views.properties.PinPropertySheetAction
import org.eclipse.swt.widgets.Composite
//import org.pragmas.anacleto.logger.Logger

object ApplicationPropertySheetsPage {
  val ID = "org.pragmas.anacleto.ui.PropertySheetPages.ApplicationPropertySheetsPage"
}

class ApplicationPropertySheetsPage extends PropertySheet {

  def ID = ApplicationPropertySheetsPage.ID

  override def isImportant(part : IWorkbenchPart) : Boolean = {
    //Logger.info("[ApplicationPropertySheetsPage] PropertyPage id : " + part.getSite.getId)
    if (part.getSite.getId.equals(IPageLayout.ID_PROJECT_EXPLORER)) {
      true
    } else {
      false
    }
  }

  override def isPinned : Boolean = { false }

  override def createPartControl(parent : Composite) : Unit = {
    super.createPartControl(parent)
    val menuManager = getViewSite.getActionBars.getMenuManager
    val menuItems = menuManager.getItems
    for (
      item <- menuItems if item.isInstanceOf[ActionContributionItem];
      if item.asInstanceOf[ActionContributionItem].getAction.isInstanceOf[PinPropertySheetAction]
    ) {
      menuManager.remove(item)
    }

    val toolBarManager = getViewSite.getActionBars.getToolBarManager
    val toolBarItems = toolBarManager.getItems
    for (
      item <- toolBarItems if item.isInstanceOf[ActionContributionItem];
      if item.asInstanceOf[ActionContributionItem].getAction.isInstanceOf[PinPropertySheetAction]
    ) {
      menuManager.remove(item)
    }
  }

}
