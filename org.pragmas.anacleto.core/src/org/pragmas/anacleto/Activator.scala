/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto

import org.pragmas.anacleto.ui.Perspective
import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.ui.plugin.AbstractUIPlugin
import org.osgi.framework.BundleContext
import org.eclipse.core.runtime.Platform
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.Path
import org.pragmas.anacleto.logger.Logger

object Activator extends AbstractUIPlugin {
  var plugin : Activator = _
  def ID = "org.pragmas.anacleto.core"

  val Logger = new Logger(ID)

  def getBundleLocationPath : String = {
    val bundle = Platform.getBundle(ID)
    val locationUrl = FileLocator.find(bundle, new Path("/"), null)
    val fileUrl = FileLocator.toFileURL(locationUrl)
    fileUrl.getFile
  }

  def getImageDescriptor(path : String) : ImageDescriptor = {
    AbstractUIPlugin.imageDescriptorFromPlugin(ID, path)
  }

}

class Activator extends AbstractUIPlugin {

  // The plug-in ID
  def ID = Activator.ID
  Activator.plugin = this

  //NOTE: jmagick setting
  System.setProperty("jmagick.systemclassloader", "false")

  override def start(context : BundleContext) = {
    super.start(context)
    Activator.Logger.debug("Bundle: start")
  }

  override def stop(context : BundleContext) = {
    Activator.plugin = null
    Activator.Logger.debug("Bundle: stop")
    super.stop(context)
  }

  def getDefault : Activator = {
    Activator.plugin
  }

  def getImageDescriptor(path : String) : ImageDescriptor = {
    Activator.getImageDescriptor(path)
  }

}
