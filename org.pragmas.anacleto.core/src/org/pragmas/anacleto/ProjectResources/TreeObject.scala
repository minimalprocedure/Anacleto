/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ProjectResources

import org.eclipse.ui.views.properties.IPropertySourceProvider
import org.eclipse.ui.views.properties.PropertyDescriptor
import org.eclipse.core.runtime.IAdaptable
import java.util.UUID
import org.pragmas.anacleto.models.APRJNames
import org.pragmas.anacleto.ui.Perspective
import scala.collection.mutable.ListBuffer
import org.eclipse.ui.views.properties.IPropertySource
import org.eclipse.ui.views.properties.IPropertyDescriptor
import org.eclipse.ui.views.properties.TextPropertyDescriptor
import org.eclipse.core.filesystem.URIUtil
import java.net.URI

object NodeProperties {
  val NAME = "name"
}

class TreeObject(var name : String) extends IAdaptable {

  private val UUID_TAG = APRJNames.uuid
  private val SUBTYPES = APRJNames.subtypes

  def currentURI : URI = { URIUtil.toURI(ProjectNodeTools.resourcePath(this))}
  def currentPhisicalPath : String = { ProjectNodeTools.resourcePath(this) }
  def currentRelativePhisicalPath : String = { ProjectNodeTools.resourcePath(this, true) }
  def currentParentPhisicalPath : String = { ProjectNodeTools.resourceContainerPath(this) }

  var nodeType : String = ProjectTypes.NODE
  var parent : TreeParent = null
  var isDumpable : Boolean = true

  private var _store : Map[String, Any] = Map()
  private var _referencesBy : ListBuffer[TreeObject] = _
  private var _referencesTo : ListBuffer[TreeObject] = _

  def referencesBy : ListBuffer[TreeObject] = {
    if (_referencesBy == null) ListBuffer[TreeObject]() else _referencesBy
  }

  def referenceBy(ref : TreeObject) : ListBuffer[TreeObject] = {
    if (_referencesBy == null) _referencesBy = ListBuffer[TreeObject](ref)
    else if (!_referencesBy.contains(ref)) _referencesBy += ref
    _referencesBy
  }

  def dereferenceBy(ref : TreeObject) : ListBuffer[TreeObject] = {
    if (_referencesBy == null) ListBuffer[TreeObject]()
    else if (_referencesBy.contains(ref)) _referencesBy -= ref
    if (_referencesBy.size == 0) {
      _referencesBy = null
      ListBuffer[TreeObject]()
    } else _referencesBy
  }

  def referencesTo : ListBuffer[TreeObject] = {
    if (_referencesTo == null) ListBuffer[TreeObject]() else _referencesTo
  }

  def referenceTo(ref : TreeObject) : ListBuffer[TreeObject] = {
    if (_referencesTo == null) _referencesTo = ListBuffer[TreeObject](ref)
    else if (!_referencesTo.contains(ref)) _referencesTo += ref
    _referencesTo
  }

  def dereferenceTo(ref : TreeObject) : ListBuffer[TreeObject] = {
    if (_referencesTo == null) ListBuffer[TreeObject]()
    else if (_referencesTo.contains(ref)) _referencesTo -= ref
    if (_referencesTo.size == 0) {
      _referencesTo = null
      ListBuffer[TreeObject]()
    } else _referencesTo
  }

  def deChildFromReferenceParent(ref : TreeParent) : Unit = {
    if (referencesBy.contains(ref)) {
      //dereferenceBy(ref)
      ref.dereferenceChild(this)
    }
  }

  def deChildFromAllReferenceParent : Unit = {
    for (ref <- referencesBy if ref.isInstanceOf[TreeParent]) {
      //dereferenceBy(ref)
      ref.asInstanceOf[TreeParent].dereferenceChild(this)
    }
  }

  def rawSubTypes_=(subtype : String) : Unit = property(SUBTYPES, subtype)
  def rawSubTypes : String = property(SUBTYPES).asInstanceOf[String]

  def subTypes : List[String] = {
    if (!propertyExist(SUBTYPES))
      List[String]()
    else
      property(SUBTYPES).asInstanceOf[String].split(":").toList
  }
  def subTypes_=(subtype : String) : Unit = {
    var st = subTypes
    if (!st.contains(subtype)) {
      st ++= List(subtype)
      property(SUBTYPES, st.mkString(":"))
    }
  }

  def uniqueName : String = {
    if (!_store.contains(UUID_TAG))
      _store += (UUID_TAG -> UUID.randomUUID.toString)
    _store(UUID_TAG).asInstanceOf[String]
  }

  def uniqueName_=(uuid : String) : Unit = _store.updated(UUID_TAG, uuid) //_store(UUID_TAG) = uuid

  def this(name : String, nodeType : String) = {
    this(name)
    this.nodeType = nodeType
  }

  def this(name : String, nodeType : String, nodeSubType : String) = {
    this(name)
    this.nodeType = nodeType
    this.subTypes = nodeSubType
  }

  def hasParent : Boolean = if (parent == null) false else true

  override def toString : String = {
    name
  }

  def removeProperty(property : String, value : Any = null) : Unit = {
    if (_store.contains(property)) {
      if (_store(property).isInstanceOf[List[Any]]) {
        var prop = _store(property).asInstanceOf[List[Any]]
        if (prop.isEmpty) {
          _store -= property
        } else {
          if (value != null && prop.indexOf(value) > -1) {
            _store = _store.updated(property, prop.filterNot(_ == value))
          }
        }
      } else _store -= property
    }
  }

  def property(property : String, value : Any) : Unit = {
    if (_store.contains(property))
      if (_store(property).isInstanceOf[List[Any]]) {
        var prop = _store(property).asInstanceOf[List[Any]]
        if (!prop.contains(value)) {
          prop ::= value
          _store = _store.updated(property, prop.reverse)
        }
      } else _store = _store.updated(property, value)
    else
      _store += (property -> value)
  }

  def properties : Map[String, Any] = _store

  def property(property : String) : Any = {
    if (_store.contains(property)) _store(property)
    else if (property == NodeProperties.NAME) {
      //property(NodeProperties.NAME, self.name)
      _store += (property -> this.name)
      this.name
      //nodeType
    } else null
  }

  def propertyExist(property : String) : Boolean = {
    _store.contains(property)
  }

  def listValueExist(property : String, value : Any = null) : Boolean = {
    if (_store.contains(property) && _store(property).isInstanceOf[List[Any]]) {
      var prop = _store(property).asInstanceOf[List[Any]]
      if (value != null && prop.contains(value)) true else false
    } else false
  }

  def store : Map[String, Any] = _store
  def store_=(store : Map[String, Any]) : Unit = {
    _store = store
    uniqueName
  }

  def rename(name : String) : Unit = {
    this.name = name
    this.property(APRJNames.title, this.name)
  }

  /*IAdaptable*/

  override def getAdapter(adapter : Class[_]) : Object = {
    if (adapter == classOf[IPropertySource]) {
      new TreeObjectPropertySource
    } else null
  }

}

class TreeObjectPropertySource extends IPropertySource {

  val node = Perspective.projectManager.currentNode

  def getEditableValue : Object = {
    "getEditableValue"
  }

  private def fields(key : String, value : Any) : PropertyDescriptor = {
    var descrName =
      key match {
        case APRJNames.title
          | APRJNames.subject
          | APRJNames.creator
          | APRJNames.description => new TextPropertyDescriptor(key, key)
        case _ => new PropertyDescriptor(key, key)
      }
    descrName.setDescription(value.toString)
    descrName
  }

  def getPropertyDescriptors : Array[IPropertyDescriptor] = {
    val propertyDescriptors = ListBuffer[IPropertyDescriptor]()
    node.properties foreach {
      case (key, value) =>
        propertyDescriptors += fields(key, value)
    }
    propertyDescriptors.toArray
  }

  def getPropertyValue(id : Object) : Object = {
    if (node.propertyExist(id.toString))
      node.property(id.toString).toString
    else ""
  }

  def isPropertySet(id : Object) : Boolean = { false }
  def resetPropertyValue(id : Object) : Unit = {}

  def setPropertyValue(id : Object, value : Object) : Unit = {
    node.property(id.toString, value)
  }
}
