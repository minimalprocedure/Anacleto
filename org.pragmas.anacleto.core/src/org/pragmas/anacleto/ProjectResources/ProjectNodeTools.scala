/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ProjectResources

import org.pragmas.anacleto.models.APRJNames
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.ui.views.ProjectManager
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.IPath

object ProjectNodeTools {

  def getChildByPropertyValue(pname : String, pvalue : String, parent : TreeParent) : TreeObject = {
    var nodeResult : TreeObject = null
    if (parent.hasChildren) {
      for (child <- parent.children if nodeResult == null) {
        if (!isTreeParent(child)) {
          if (child.propertyExist(pname)) {
            if (child.property(pname).asInstanceOf[String] == pvalue)
              nodeResult = child
          }
        } else nodeResult = getChildByPropertyValue(pname, pvalue, child.asInstanceOf[TreeParent])
      }
    }
    nodeResult
  }

  def parentPage(node : TreeObject) : TreeParent = {
    var nodeParent : TreeParent = null
    node.nodeType match {
      case ProjectTypes.PAGE    => nodeParent = node.asInstanceOf[TreeParent]
      case ProjectTypes.ROOT    => nodeParent = null
      case ProjectTypes.PROJECT => nodeParent = null
      case _                    => nodeParent = if (node.hasParent) parentPage(node.parent) else null
    }
    nodeParent
  }

  def projectResourcePathByName(node : TreeObject) : String = {
    var path = "/" + node.name
    var nodeParent = node.parent
    if (nodeParent != null) {
      path = projectResourcePathByName(nodeParent) + path
    }
    path
  }

  def projectResourcePathByType(node : TreeObject) : String = {
    var path = "/" + node.nodeType
    var nodeParent = node.parent
    if (nodeParent != null) {
      path = projectResourcePathByType(nodeParent) + path
    }
    path
  }

  def isTreeParent(node : TreeObject) : Boolean = {
    if (node.isInstanceOf[TreeParent]) true else false
  }

  def removableResource(node : TreeObject) : Boolean = {
    node.nodeType match {
      case ProjectTypes.PAGE         => true
      case ProjectTypes.IMAGE        => true
      case ProjectTypes.SCRIPT       => true
      case ProjectTypes.LOCATION     => true
      case ProjectTypes.PERSON       => true
      case ProjectTypes.NOTE         => true
      case ProjectTypes.TAG          => true
      case ProjectTypes.COMMENT      => true
      case ProjectTypes.SUMMARY      => true
      case ProjectTypes.IMAGE_FILTER => true
      case _                         => false
    }
  }

  def renamableResource(node : TreeObject) : Boolean = {
    node.nodeType match {
      case ProjectTypes.PAGE         => true
      case ProjectTypes.IMAGE        => true
      case ProjectTypes.SCRIPT       => true
      case ProjectTypes.LOCATION     => true
      case ProjectTypes.PERSON       => true
      case ProjectTypes.NOTE         => true
      case ProjectTypes.TAG          => true
      case ProjectTypes.COMMENT      => true
      case ProjectTypes.SUMMARY      => true
      case ProjectTypes.IMAGE_FILTER => true
      case _                         => false
    }
  }

  def resourceTitle(node : TreeObject) : String = {
    if (node.propertyExist(APRJNames.title))
      node.property(APRJNames.title).toString
    else
      ""
  }

  def resourceDescription(node : TreeObject) : String = {
    if (node.propertyExist(APRJNames.description))
      node.property(APRJNames.description).toString
    else
      ""
  }

  def resourceContainerPath(node : TreeObject) : String = {
    node.nodeType match {
      case ProjectTypes.PAGE => {
        ProjectManager.ProjectPathParentStore
          .append(ProjectTypes.APPDATA_CONTAINER)
          .append(ProjectTypes.PAGES)
          .toString
      }
      case ProjectTypes.IMAGE_FILTER
        | ProjectTypes.IMAGE_FILTER_GO => {
        ProjectManager.ProjectPathParentStore
          .append(ProjectTypes.APPDATA_CONTAINER)
          .append(ProjectTypes.FILTERS)
          .append(node.property(APRJNames.name).toString)
          .toString
      }
      case ProjectTypes.PAGES
        | ProjectTypes.IMAGES
        | ProjectTypes.SCRIPTS
        | ProjectTypes.LOCATIONS
        | ProjectTypes.PERSONS
        | ProjectTypes.SUMMARIES
        | ProjectTypes.NOTES
        | ProjectTypes.COMMENTS
        | ProjectTypes.TAGS => {
        if (node.parent.nodeType == ProjectTypes.PROJECT) {
          ProjectManager.ProjectPathParentStore
            .append(ProjectTypes.APPDATA_CONTAINER)
            .toString
        } else {
          ProjectManager.ProjectPathParentStore
            .append(ProjectTypes.APPDATA_CONTAINER)
            .append(ProjectTypes.PAGES)
            .append(parentPage(node).property(APRJNames.name).toString)
            .toString
        }
      }
      case ProjectTypes.IMAGE
        | ProjectTypes.SCRIPT
        | ProjectTypes.LOCATION
        | ProjectTypes.PERSON
        | ProjectTypes.SUMMARY
        | ProjectTypes.NOTE
        | ProjectTypes.COMMENT
        | ProjectTypes.TAG => {
        ProjectManager.ProjectPathParentStore
          .append(ProjectTypes.APPDATA_CONTAINER)
          .append(ProjectTypes.PAGES)
          .append(parentPage(node).property(APRJNames.name).toString)
          .append(node.parent.nodeType)
          .toString
      }
      case _ => ""
    }
  }

  def resourcePath(node : TreeObject, relative : Boolean = false) : String = {
    val parentStore = if (relative) new Path("") else ProjectManager.ProjectPathParentStore

    node.nodeType match {
      case ProjectTypes.PAGE => {
        parentStore
          .append(ProjectTypes.APPDATA_CONTAINER)
          .append(ProjectTypes.PAGES)
          .append(node.property(APRJNames.name).toString)
          .toString
      }
      case ProjectTypes.IMAGE_FILTER
        | ProjectTypes.IMAGE_FILTER_GO => {
        parentStore
          .append(ProjectTypes.APPDATA_CONTAINER)
          .append(ProjectTypes.FILTERS)
          .append(node.name)
          .toString
      }
      case ProjectTypes.PAGES
        | ProjectTypes.IMAGES
        | ProjectTypes.SCRIPTS
        | ProjectTypes.LOCATIONS
        | ProjectTypes.PERSONS
        | ProjectTypes.SUMMARIES
        | ProjectTypes.NOTES
        | ProjectTypes.COMMENTS
        | ProjectTypes.TAGS => {
        if (node.parent.nodeType == ProjectTypes.PROJECT) {
          node.nodeType match {
            case ProjectTypes.PAGES => {
              parentStore
                .append(ProjectTypes.APPDATA_CONTAINER)
                .append(node.property(APRJNames.name).toString)
                .toString
            }
            case _ => {
              parentStore
                .append(ProjectTypes.APPDATA_CONTAINER)
                .toString
            }
          }
        } else {
          parentStore
            .append(ProjectTypes.APPDATA_CONTAINER)
            .append(ProjectTypes.PAGES)
            .append(parentPage(node).property(APRJNames.name).toString)
            .append(node.property(APRJNames.name).toString)
            .toString
        }
      }
      case ProjectTypes.IMAGE
        | ProjectTypes.SCRIPT
        | ProjectTypes.LOCATION
        | ProjectTypes.PERSON
        | ProjectTypes.SUMMARY
        | ProjectTypes.NOTE
        | ProjectTypes.COMMENT
        | ProjectTypes.TAG => {
        parentStore
          .append(ProjectTypes.APPDATA_CONTAINER)
          .append(ProjectTypes.PAGES)
          .append(parentPage(node).property(APRJNames.name).toString)
          .append(node.parent.nodeType)
          .append(node.property(APRJNames.name).toString)
          .toString
      }
      case _ => ""
    }
  }

}
