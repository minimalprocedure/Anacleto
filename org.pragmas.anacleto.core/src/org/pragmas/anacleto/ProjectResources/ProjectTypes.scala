/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ProjectResources

import org.pragmas.anacleto.models.APRJNames

object ProjectTypes {
  val PROJECT_FILE_EXT_TURTLE = ".turtle"
  val PROJECT_FILE_EXT_XML_ABBREV = ".xml"
  val PROJECT_FILE_EXT_NTRIPLE = ".ntriple"
  val PROJECT_FILE_EXT_N3 = ".n3"
  val PROJECT_FILE_EXT_XML = ".xml"
  val PROJECT_FILE_EXT_RDF = ".rdf"
  //val PROJECT_FILE_NAME = APRJNames.SUFFIX + "." + PROJECT_FILE_EXT_RDF
  val PROJECT_FILE_NAME = "__dataproject" //APRJNames.SUFFIX
  //val APPDATA = "@"+ APRJNames.SUFFIX
  val APPDATA_CONTAINER = "__datacontainer__"
  val APPINDEX_CONTAINER = "__indexcontainer__"
  val MATERIALS_CONTAINER = "__materialscontainer__"
  val ARCHIVES_CONTAINER = "__archivecontainer__"
  val ARCHIVE_EXTENSION = ".zip"
  val NODE = "node"
  val ROOT = "root"
  val CONTAINER = "container"
  val PROJECT = APRJNames.Project
  val IMAGE = APRJNames.image

  val SCRIPT = APRJNames.script
  val LOCATION = APRJNames.location
  val PERSON = APRJNames.person
  val SUMMARY = APRJNames.summary
  val NOTE = APRJNames.note
  val PAGE = APRJNames.Page
  val COMMENT = APRJNames.comment
  val FILTER = APRJNames.filter
  val MACRO = APRJNames.macro

  val ACTIVE = APRJNames.active

  val TAG = APRJNames.tag

  val PAGES = "Pages"
  val IMAGES = "Images"
  val SCRIPTS = "Scripts"
  val LOCATIONS = "Locations"
  val PERSONS = "Persons"
  val SUMMARIES = "Summaries"
  val NOTES = "Notes"
  val COMMENTS = "Comments"
  val TAGS = "Tags"
  val MACROS = "Macros"
  val FILTERS = APRJNames.Filters
  //val ARCHIVES = "Archives"
  val BACKUPS = "Backups"

  val IMAGE_FILTER = IMAGE + "_filter"
  val IMAGE_FILTER_GO = IMAGE_FILTER + "_go"

  val SUBTYPE_REFERENCE = "reference"

  val SUBTYPE_NONE = "none"
  val SUBTYPE_BLUE = "blue"
  val SUBTYPE_GREEN = "green"
  val SUBTYPE_ORANGE = "orange"
  val SUBTYPE_PINK = "pink"
  val SUBTYPE_PURPLE = "purple"
  val SUBTYPE_RED = "red"
  val SUBTYPE_YELLOW = "yellow"

  val SUBTYPE_FILTER = "filter"
  val SUBTYPE_PNG = "png"
  val SUBTYPE_JPG = "jpg"
  val SUBTYPE_TIF = "tiff"

  def RESOLVE_COLORS : Array[String] = {
    Array(
      SUBTYPE_NONE,
      SUBTYPE_BLUE,
      SUBTYPE_GREEN,
      SUBTYPE_ORANGE,
      SUBTYPE_PINK,
      SUBTYPE_PURPLE,
      SUBTYPE_RED,
      SUBTYPE_YELLOW
    )
  }

  def RESOLVE_PARENT(child : String) : String = {
    child match {
      case PAGE     => PAGES
      case IMAGE    => IMAGES
      case SCRIPT   => SCRIPTS
      case LOCATION => LOCATIONS
      case PERSON   => PERSONS
      case SUMMARY  => SUMMARIES
      case NOTE     => NOTES
      case COMMENT  => COMMENTS
      case TAG      => TAGS
      case _        => PAGES
    }
  }

  def RESOLVE_CHILD(child : String) : String = {
    child match {
      case PAGES     => PAGE
      case IMAGES    => IMAGE
      case SCRIPTS   => SCRIPT
      case LOCATIONS => LOCATION
      case PERSONS   => PERSON
      case SUMMARIES => SUMMARY
      case NOTES     => NOTE
      case COMMENTS  => COMMENT
      case TAGS      => TAG
      case _         => PAGE
    }
  }

  def RESOURCES_TEXT_CONCAT : String = {
    String.format("%s,%s,%s,%s,%s", SCRIPT, SUMMARY, NOTE, COMMENT, TAG)
  }

  def RESOURCES_STRUCT_CONCAT : String = {
    String.format("%s,%s", LOCATION, PERSON)
  }

  val RESOURCES_FILTERS_CONCAT = "rb,js,py,scala"

  def RESOURCES_PERSON : String = PERSON

  def RESOURCES_LOCATION : String = LOCATION
}

