/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.ProjectResources

import java.util.ArrayList
import org.eclipse.jface.viewers.LabelProvider
import org.eclipse.ui.ISharedImages
//import org.eclipse.ui.PlatformUI
import org.eclipse.swt.graphics.Image

//import org.eclipse.jface.viewers.TreeViewer
//import org.eclipse.swt.widgets.Tree

import org.pragmas.anacleto.tools.ResourceManager

class ViewLabelProvider extends LabelProvider {

  //val treeViewer : TreeViewer = null

  override def getText(obj : Object) : String = {
    obj.toString
  }

  override def getImage(obj : Object) : Image = {
    //var imageKey = ISharedImages.IMG_OBJ_ELEMENT
    val nodeObj = obj.asInstanceOf[TreeObject]
    val imageKey =
      nodeObj.nodeType match {
        case ProjectTypes.APPDATA_CONTAINER => ISharedImages.IMG_OBJ_ELEMENT
        case ProjectTypes.NODE              => ISharedImages.IMG_OBJ_ELEMENT
        case ProjectTypes.ROOT              => ISharedImages.IMG_OBJ_ELEMENT
        case ProjectTypes.PROJECT           => "silk/book.png"
        case ProjectTypes.CONTAINER         => "silk/container.png"

        case ProjectTypes.IMAGE => {
          val st = nodeObj.subTypes
          if (st.contains(ProjectTypes.SUBTYPE_PNG)) "silk/image_png.png"
          else if (st.contains(ProjectTypes.SUBTYPE_JPG)) "silk/image_jpg.png"
          else if (st.contains(ProjectTypes.SUBTYPE_TIF)) "silk/image_tif.png"
          else if (st.contains(ProjectTypes.SUBTYPE_FILTER)) "silk/image_filter.png"
          else "silk/image.png"
        }

        case ProjectTypes.IMAGE_FILTER    => "silk/image_filter.png"
        case ProjectTypes.IMAGE_FILTER_GO => "silk/image_filter_go.png"
        case ProjectTypes.SCRIPT          => "silk/script.png"
        case ProjectTypes.LOCATION        => "silk/location.png"
        case ProjectTypes.PERSON          => "silk/person.png"
        case ProjectTypes.SUMMARY         => "silk/summary.png"
        case ProjectTypes.NOTE            => "silk/note.png"
        case ProjectTypes.COMMENT         => "silk/comment.png"

        case ProjectTypes.TAG => {
          val st = nodeObj.subTypes
          if (st.contains(ProjectTypes.SUBTYPE_BLUE)) "silk/tag_blue.png"
          else if (st.contains(ProjectTypes.SUBTYPE_GREEN)) "silk/tag_orange.png"
          else if (st.contains(ProjectTypes.SUBTYPE_ORANGE)) "silk/tag_orange.png"
          else if (st.contains(ProjectTypes.SUBTYPE_PINK)) "silk/tag_pink.png"
          else if (st.contains(ProjectTypes.SUBTYPE_PURPLE)) "silk/tag_purple.png"
          else if (st.contains(ProjectTypes.SUBTYPE_RED)) "silk/tag_red.png"
          else if (st.contains(ProjectTypes.SUBTYPE_YELLOW)) "silk/tag_yellow.png"
          else "silk/tag.png"
        }

        case ProjectTypes.PAGE      => "silk/page.png"
        case ProjectTypes.PAGES     => "silk/pages.png"
        case ProjectTypes.FILTERS   => "silk/image_filter.png"
        case ProjectTypes.IMAGES    => "silk/images.png"
        case ProjectTypes.SCRIPTS   => "silk/scripts.png"
        case ProjectTypes.PERSONS   => "silk/persons.png"
        case ProjectTypes.LOCATIONS => "silk/locations.png"
        case ProjectTypes.SUMMARIES => "silk/summaries.png"
        case ProjectTypes.NOTES     => "silk/notes.png"
        case ProjectTypes.COMMENTS  => "silk/comments.png"
        case ProjectTypes.TAGS      => "silk/tags.png"
        case _                      => ISharedImages.IMG_OBJ_ELEMENT
      }
    ResourceManager.getIcon(imageKey)
  }
}
