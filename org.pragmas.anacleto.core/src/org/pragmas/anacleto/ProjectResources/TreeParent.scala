/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.ProjectResources

import scala.collection.mutable.ListBuffer

class TreeParent(name : String) extends TreeObject(name) {

  var childrenIsDumpable : Boolean = true
  var children = new ListBuffer[TreeObject]
  this.nodeType = ProjectTypes.CONTAINER

  def this(name : String, nodeType : String) = {
    this(name)
    this.nodeType = nodeType
  }

  def this(name : String, nodeType : String, nodeSubType : String) = {
    this(name)
    this.nodeType = nodeType
    this.subTypes = nodeSubType
  }

  def referenceChild(child : TreeObject, addAsChild : Boolean = true) : TreeObject = {
    child.referenceBy(this)
    if (addAsChild) children += child
    child
  }

  def dereferenceChild(child : TreeObject, removeAsChild : Boolean = true) : TreeObject = {
    child.dereferenceBy(this)
    if (removeAsChild) children -= child
    child
  }

  def addChild(child : TreeObject) : TreeObject = {
    children += child
    child.parent = this
    child
  }

  def removeChild(child : TreeObject) : Unit = {
    if (children.contains(child)) {
      children -= child
      child.parent = null
    }
  }

  def removeChildren : Unit = {
    getChildren foreach { c => removeChild(c) }
  }

  def getChildren : Array[TreeObject] = {
    children.toArray.asInstanceOf[Array[TreeObject]]
  }

  def hasChild(child : TreeObject) : Boolean = {
    if (children.contains(child)) true else false
  }

  def hasChildren : Boolean = {
    children.size > 0
  }

}
