/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.models

object APRJLocationNames {
  val SUFFIX = "loc"
  val TYPE = "location"
  val NS = "http://pragmas.org/anacleto/1.0/project/%1$s#".format(TYPE)

  val Location = "Location"
  val Point = "Point"

  val uuid = "uuid"

  val name = "name"

  val title = "title"
  val subject = "subject"
  val description = "description"

  val lat = "lat"
  val long = "long"
  val lat_long = "lat_long"

  val country = "country"
  val state = "state"
  val foundationDate = "fundationDate"
  val abandonmentDate = "abandonmentDate"

  val relationTypes = "relationTypes"

 val referenceGradeList = List[String]("0", "1", "2", "3", "4", "5")

  val referenceTypesList = List[String](
    "simple",
    "personal",
    "birth",
    "death",
    "living",
    "other")
}
