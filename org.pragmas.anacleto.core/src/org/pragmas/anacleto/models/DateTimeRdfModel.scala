/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.models

import com.hp.hpl.jena.rdf.model._
import com.hp.hpl.jena.vocabulary._
import com.hp.hpl.jena.datatypes.xsd._
import com.hp.hpl.jena.datatypes._
import com.hp.hpl.jena.datatypes.xsd.impl._
import com.hp.hpl.jena.query.QueryFactory
import com.hp.hpl.jena.query.QueryExecutionFactory
import com.hp.hpl.jena.query.ResultSetFormatter
import com.hp.hpl.jena.query._
import com.hp.hpl.jena.rdf.model.impl.StatementImpl

import java.io.StringWriter
import java.util.UUID
import scala.collection.mutable.ListBuffer

class DateTimeRdfModel extends RDFModelTasks {

  override def NS : String = APRJDateTimeNames.NS

  model.setNsPrefix(APRJDateTimeNames.SUFFIX, APRJDateTimeNames.NS)
  model.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#")
  model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#")
  model.setNsPrefix("dc", "http://purl.org/dc/elements/1.1/")

  override def getRes(res : String) : Resource = {
    super.getRes(res)
  }

  override def getProp(prop : String) : Property = {
    prop match {
      case APRJDateTimeNames.uuid => PROP(APRJDateTimeNames.uuid, APRJDateTimeNames.NS)
      case APRJDateTimeNames.DateTime => PROP(APRJDateTimeNames.DateTime, APRJDateTimeNames.NS)
      case APRJDateTimeNames.year => PROP(APRJDateTimeNames.year, APRJDateTimeNames.NS)
      case APRJDateTimeNames.month => PROP(APRJDateTimeNames.month, APRJDateTimeNames.NS)
      case APRJDateTimeNames.day => PROP(APRJDateTimeNames.day, APRJDateTimeNames.NS)
      case APRJDateTimeNames.hour => PROP(APRJDateTimeNames.hour, APRJDateTimeNames.NS)
      case APRJDateTimeNames.minute => PROP(APRJDateTimeNames.minute, APRJDateTimeNames.NS)
      case APRJDateTimeNames.second => PROP(APRJDateTimeNames.second, APRJDateTimeNames.NS)
      case APRJDateTimeNames.date => PROP(APRJDateTimeNames.date, APRJDateTimeNames.NS)
      case APRJDateTimeNames.time => PROP(APRJDateTimeNames.time, APRJDateTimeNames.NS)
      case APRJDateTimeNames.datetime => PROP(APRJDateTimeNames.datetime, APRJDateTimeNames.NS)
      case _ => super.getProp(prop)
    }
  }

  def createDate(name : String, data : Map[String, Any] = null) : Resource = {
    val dataProp = checkForName(name, data)
    val date = addRes(getRes(APRJDateTimeNames.DateTime), name, null)
    addPropData(dataProp, date)
    date
  }

  def addDate(dateResource : Resource, year : Int, month : Int, day : Int,
              dateType : String = APRJDateTimeNames.typeGregorian, compact : Boolean = false) : Resource = {
    val resource = findResource(dateResource)
    val dayCorrect = if (day < 10) "0" + day.toString else day.toString
    val monthCorrect = if (month > 31 || month < 1) "1" else month.toString
    val yearCorrect = if (year <= 0) "0000" else year.toString
    if (compact) {
      val datetime = if (year >= 0 && month > 0 && day > 0) yearCorrect + "-" + monthCorrect + "-" + dayCorrect else if (month > 0 && day > 0) "0-" + monthCorrect + "-" + dayCorrect else if (day > 0) "0-0-" + dayCorrect else "0-0-0"
      addProp(APRJDateTimeNames.date, datetime, resource)
    } else {
      if (year >= 0) addProp(APRJDateTimeNames.year, yearCorrect, resource)
      if (month > 0) addProp(APRJDateTimeNames.month, monthCorrect, resource)
      if (day > 0) addProp(APRJDateTimeNames.day, dayCorrect, resource)
    }
    if (dateType != null) addProp(APRJDateTimeNames.dateType, dateType, resource)
    resource
  }

}
