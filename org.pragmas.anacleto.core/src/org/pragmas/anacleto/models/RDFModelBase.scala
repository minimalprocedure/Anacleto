/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.models

import com.hp.hpl.jena.rdf.model._
import com.hp.hpl.jena.vocabulary._
import com.hp.hpl.jena.datatypes.xsd._
import com.hp.hpl.jena.datatypes._
import com.hp.hpl.jena.datatypes.xsd.impl._
import com.hp.hpl.jena.query.QueryFactory
import com.hp.hpl.jena.query.QueryExecutionFactory
import com.hp.hpl.jena.query.ResultSetFormatter
import com.hp.hpl.jena.query._
import com.hp.hpl.jena.rdf.model.impl.StatementImpl

import java.io.StringWriter
import java.util.UUID
import scala.collection.mutable.ListBuffer

object RDFDumpingFormat {
  val TURTLE = "TURTLE"
  val XML_ABBREV = "RDF/XML-ABBREV"
  val NTRIPLE = "N-TRIPLE"
  val N3 = "N3"
  val XML = null
  val RDF = XML_ABBREV
  val DEFAULT = XML_ABBREV
}

trait RDFModelBase {

  def NS : String

  var model : Model = ModelFactory.createDefaultModel

  def BAG(bagName : String) : Bag = {
    model.createBag(NS + bagName)
  }

  def BAG(bagName : String, localUri : String) : Bag = {
    model.createBag(localUri + bagName)
  }

  def SEQ(bagName : String) : Seq = {
    model.createSeq(NS + bagName)
  }

  def SEQ(bagName : String, localUri : String) : Seq = {
    model.createSeq(localUri + bagName)
  }

  def ALT(bagName : String) : Alt = {
    model.createAlt(NS + bagName)
  }

  def ALT(bagName : String, localUri : String) : Alt = {
    model.createAlt(localUri + bagName)
  }

  def RES(resName : String, localUri : String) : Resource = {
    model.createResource(localUri + resName)
  }

  def RES(resName : String = null) : Resource = {
    if (resName == null)
      model.createResource
    else
      model.createResource(NS + resName)
  }

  def PROP(propName : Property) : Property = {
    model.createProperty(propName.getNameSpace, propName.getLocalName)
  }

  def PROP(propName : String) : Property = {
    model.createProperty(NS, propName)
  }

  def PROP(propName : String, localUri : String) : Property = {
    model.createProperty(localUri, propName)
  }

  def LIT(lex : String) : Literal = {
    model.createTypedLiteral(lex, XSDDatatype.XSDstring)
  }

}

trait RDFModelTasks extends RDFModelBase { //extends APRJ {

  protected def getCommonProp(prop : String) : Property = {
    prop match {
      case APRJNames.contributor => DC_11.contributor
      case APRJNames.coverage    => DC_11.coverage
      case APRJNames.creator     => DC_11.creator
      case APRJNames.date        => DC_11.date
      case APRJNames.description => DC_11.description
      case APRJNames.format      => DC_11.format
      case APRJNames.identifier  => DC_11.identifier
      case APRJNames.language    => DC_11.language
      case APRJNames.publisher   => DC_11.publisher
      case APRJNames.relation    => DC_11.relation
      case APRJNames.rights      => DC_11.rights
      case APRJNames.source      => DC_11.source
      case APRJNames.subject     => DC_11.subject
      case APRJNames.title       => DC_11.title
      case APRJNames.dctype      => DC_11.`type`
      case _                     => null
    }
  }

  private def correctNSforNewModel(prefixMap : java.util.Map[String, String], mTo : Model) : Model = {
    val keys = prefixMap.keySet.toArray
    for (k <- keys) {
      mTo.setNsPrefix(k.toString, prefixMap.get(k))
    }
    mTo
  }

  def getURI(propName : String) : String = {
    NS + propName
  }

  def getPropURI(propName : String) : String = {
    getProp(propName).getURI
  }

  def getResURI(resName : String) : String = {
    getRes(resName).getURI
  }

  def getRes(res : String) : Resource = {
    RES(res)
  }

  def getProp(prop : String) : Property = {
    val property = getCommonProp(prop)
    if (property == null) PROP(prop) else property
  }

  def addRes(resource : Resource, value : String, predicate : String) : Resource = {
    val res = findResource(resource)
    val pred = if (predicate != null) predicate else value.replaceAll(" ", "_")
    val res2 = model.createResource(NS + pred, res)
    //addProp(APRJNames.name, value, resource)
    res2
  }

  def addRDFModelToRes(m : RDFModelBase, resource : Resource, propType : String) : Resource = {
    val res = findResource(resource)
    val ress = m.model.listSubjects
    val element = PROP(propType)
    while (ress.hasNext) {
      val r = ress.next.asInstanceOf[Resource]
      res.addProperty(element, r)
    }
    model = union(m.model)
    res
  }

  def addProp(property : String, value : String, resource : Resource) : Resource = {
    val res = findResource(resource)
    val propString = filterField(property)
    if (propString != null) {
      val prop = getProp(propString)
      res.addProperty(prop, value)
    } else res
  }

  def addProp(property : String, value : Resource, resource : Resource) : Resource = {
    val res = findResource(resource)
    val propString = filterField(property)
    if (propString != null) {
      val prop = getProp(propString)
      res.addProperty(prop, value)
    } else res
  }

  def addLit(property : Property, value : Any, resource : Resource) : Resource = {
    val res = findResource(resource)
    res.addLiteral(property, value)
  }

  def addPropData(dcData : Map[String, Any], resource : Resource) : Resource = {
    val res = findResource(resource)
    for (dc <- dcData.keys) {
      /*TODO: Refactor for type */
      if (dcData(dc).isInstanceOf[List[Any]])
        addPropSubList(dc, APRJNames.listItem, dcData(dc).asInstanceOf[List[Any]], res)
      else if (dcData(dc).isInstanceOf[ListBuffer[Any]])
        addPropSubList(dc, APRJNames.listItem, dcData(dc).asInstanceOf[ListBuffer[Any]].toList, res)
      else {
        addProp(dc, dcData(dc).toString, res)
      }
    }
    res
  }

  def loadModelFromUri(uri : String, syntax : String) : Model = {
    model.read(uri, syntax)
  }

  def loadModelFromUri(uri : String) : Model = {
    model.read(uri, RDFDumpingFormat.XML_ABBREV)
  }

  def checkForName(name : String, data : Map[String, Any]) : Map[String, Any] = {
    if (data != null)
      if (!data.contains(APRJNames.name))
        data + (APRJNames.name -> name)
      else data
    else Map[String, Any]()
  }

  def filterField(fieldValue : String) : String = {
    fieldValue match {
      case APRJNames.predicate => null
      case "page_name"         => null
      case "page_type"         => null
      //case "dctype" => null
      case _                   => fieldValue
    }
  }

  def filterFields(map : Map[String, String]) : Map[String, String] = {
    map -
      (APRJNames.predicate) -
      ("page_name") -
      ("page_type")
  }

  def getPropertiesNameList(resource : Resource) : List[Any] = {
    val res = findResource(resource)
    var list = ListBuffer[Any]()
    val properties = res.listProperties
    while (properties.hasNext) {
      list += properties.next.getString
    }
    list.toList
  }

  def addPropSubList(propName : String, subPropName : String, listNames : List[Any], resource : Resource) : Resource = {
    val res = findResource(resource)
    if (listNames.size > 0) {
      val element = getProp(propName)
      val elementResource = RES(null)
      for (name <- listNames) {
        addProp(subPropName, name.toString, elementResource)
      }
      res.addProperty(element, elementResource)
    } else res
  }

  def union(m : Model) : Model = {
    val newModel = model.union(m)
    correctNSforNewModel(m.getNsPrefixMap, newModel)
    correctNSforNewModel(model.getNsPrefixMap, newModel)
  }

  //  def intersection(m : Model) : Model = {
  //    model.intersection(m)
  //  }
  //
  //  def difference(m : Model) : Model = {
  //    model.difference(m)
  //  }

  def dumpIn(syntax : String = RDFDumpingFormat.XML_ABBREV) : String = {
    val sw = new StringWriter
    model.write(sw, syntax)
    sw.toString
  }

  def dump : String = {
    dumpIn(RDFDumpingFormat.XML_ABBREV)
  }

  def dumpModelIn(m : Model, syntax : String = RDFDumpingFormat.XML_ABBREV) : String = {
    val sw = new StringWriter
    m.write(sw, syntax)
    sw.toString
  }

  def findResource(predicate : String) : Resource = {
    model.createResource(predicate)
  }

  def findResource(ns : String, name : String) : Resource = {
    model.createResource(ns + name)
  }

  def findResource(res : Resource) : Resource = {
    val uri = res.getURI
    if (uri == null) model.createResource(res.getId) else model.createResource(uri)
  }

  def findProperties(resName : String) : List[Property] = {
    val resource = findResource(getResURI(resName))
    val properties = resource.listProperties
    val accumulator = ListBuffer[Property]()
    while (properties.hasNext) {
      val p = properties.next
      accumulator += p.getPredicate
    }
    accumulator.toList
  }

  def findPropertiesValues(resName : String) : List[String] = {
    val resource = findResource(getResURI(resName))
    val properties = resource.listProperties
    val accumulator = ListBuffer[String]()
    while (properties.hasNext) {
      val p = properties.next
      accumulator += p.getPredicate.toString
    }
    accumulator.toList
  }

  def findProperty(resName : String, prop : String) : Property = {
    val propFinded = model.getProperty(getRes(resName), getProp(prop))
    if (propFinded != null) propFinded.getPredicate else null
  }

  def findPropertyValue(resName : String, prop : String) : String = {
    val propFinded = model.getProperty(getRes(resName), getProp(prop))
    if (propFinded != null) propFinded.getPredicate.toString else null
  }

  def resourceFrom(resource : Any) : Resource = {
    if (resource.isInstanceOf[String])
      findResource(getResURI(resource.asInstanceOf[String]))
    else if (resource.isInstanceOf[Resource])
      resource.asInstanceOf[Resource]
    else throw new IllegalArgumentException("Value must be Resource or String")
  }

  def findStatements(resName : Any) : List[Statement] = {
    val resource = resourceFrom(resName)
    val properties = resource.listProperties
    val accumulator = ListBuffer[Statement]()
    while (properties.hasNext) {
      //val p = properties.next
      accumulator += properties.next.asInstanceOf[Statement]
    }
    accumulator.toList
  }

  def findStatement(resName : String, prop : String) : Statement = {
    model.getProperty(getRes(resName), getProp(prop))
  }

  def findStatementValue(resName : Any, prop : String) : Any = {
    val resource =
      if (resName.isInstanceOf[String]) model.getProperty(getRes(resName.toString), getProp(prop))
      else resName.asInstanceOf[Resource].getProperty(getProp(prop))
    if (resource != null) {
      val value = resource.asInstanceOf[Statement].getObject
      if (value.isInstanceOf[com.hp.hpl.jena.rdf.model.impl.LiteralImpl]) value.toString
      else if (value.isInstanceOf[com.hp.hpl.jena.rdf.model.impl.ResourceImpl]) value.asInstanceOf[Resource]
      else value.getClass.toString
    } else null
  }

  def loadValue(resName : String, prop : String) : String = {
    val value = findStatementValue(resName, prop)
    if (value != null) value.toString else ""
  }

}
