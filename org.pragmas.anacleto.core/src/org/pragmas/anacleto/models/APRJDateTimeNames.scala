/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package  org.pragmas.anacleto.models

object APRJDateTimeNames {
  val SUFFIX = "dt"
  val TYPE = "datetime"
  val NS = "http://pragmas.org/anacleto/1.0/project/%1$s#".format(TYPE)

  val uuid = "uuid"

  val DateTime = "DateTime"
  val year = "year"
  val month = "month"
  val day = "day"
  val hour = "hour"
  val minute = "minute"
  val second = "second"

  val date = "date"
  val time = "time"
  val datetime = "datetime"

  val dateType = "dateType"
  val typeJulian = "Julian"
  val typeGregorian = "Gregorian"

  val status = "status"
  val statusNULL = ""
  val statusAlive = "alive"
}
