/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.models

object APRJNames {

  val SUFFIX = "aprj"
  val TYPE = "project"
  val NS = "http://pragmas.org/anacleto/1.0/%1$s#".format(TYPE)

  val predicate = "predicate"

  val Project = "Project"
  val Folder = "Folder"
  val Page = "Page"
  val Image = "Image"
  val Script = "Script"
  val Location = "Location"
  val Person = "Person"
  val Note = "Note"
  val Summary = "Summary"
  val Tag = "Tag"
  val Comment = "Comment"

  val Filters = "Filters"

  val uuid = "uuid"

  val name = "name"
  val folder = "folder"
  val page = "page"
  val image = "image"
  val person = "person"
  val script = "script"
  val location = "location"
  val note = "note"
  val tag = "tag"
  val macro = "macro"
  val comment = "comment"
  val summary = "summary"
  val filter = "filter"
  val filters = "filters"
  val listItem = "item"
  val active = "active"

  val noteliteral = "noteliteral"
  val scriptliteral = "scriptliteral"

  val resolution = "resolution"
  val size = "size"
  val height = "height"
  val width = "width"

  val parent = "parent"
  val localpath = "localpath"
  val localname = "localname"
  val localtype = "localtype"

  val contributor = "contributor"
  val coverage = "coverage"
  val creator = "creator"
  val date = "date"
  val description = "description"
  val format = "format"
  val identifier = "identifier"
  val language = "language"
  val publisher = "publisher"
  val relation = "relation"
  val rights = "rights"
  val source = "source"
  val subject = "subject"
  val title = "title"
  val dctype = "type"
  val subtypes = "subtypes"
}
