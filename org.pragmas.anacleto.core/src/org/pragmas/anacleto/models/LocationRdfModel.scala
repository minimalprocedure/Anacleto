/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.models

import com.hp.hpl.jena.rdf.model._
import com.hp.hpl.jena.vocabulary._
import com.hp.hpl.jena.datatypes.xsd._
import com.hp.hpl.jena.datatypes._
import com.hp.hpl.jena.datatypes.xsd.impl._
import com.hp.hpl.jena.query.QueryFactory
import com.hp.hpl.jena.query.QueryExecutionFactory
import com.hp.hpl.jena.query.ResultSetFormatter
import com.hp.hpl.jena.query._
import com.hp.hpl.jena.rdf.model.impl.StatementImpl

import java.io.StringWriter
import java.util.UUID
import scala.collection.mutable.ListBuffer

class LocationRdfModel extends RDFModelTasks {

  override def NS : String = APRJLocationNames.NS

  model.setNsPrefix(APRJLocationNames.SUFFIX, APRJLocationNames.NS)
  model.setNsPrefix(APRJNames.SUFFIX, APRJNames.NS)
  model.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#")
  model.setNsPrefix("geo", "http://www.w3.org/2003/01/geo/wgs84_pos#")
  model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#")
  model.setNsPrefix("dc", "http://purl.org/dc/elements/1.1/")

  override def getRes(res : String) : Resource = {
    super.getRes(res)
  }

  override def getProp(prop : String) : Property = {
    prop match {
      case APRJLocationNames.Location => PROP(APRJLocationNames.Location, APRJLocationNames.NS)
      case APRJLocationNames.uuid => PROP(APRJLocationNames.uuid, APRJLocationNames.NS)
      case APRJLocationNames.name => PROP(APRJLocationNames.name, APRJLocationNames.NS)
      case APRJLocationNames.Point => PROP(APRJLocationNames.Point, "http://www.w3.org/2003/01/geo/wgs84_pos#")
      case APRJLocationNames.lat => PROP(APRJLocationNames.lat, "http://www.w3.org/2003/01/geo/wgs84_pos#")
      case APRJLocationNames.long => PROP(APRJLocationNames.long, "http://www.w3.org/2003/01/geo/wgs84_pos#")
      case _ => super.getProp(prop)
    }
  }

  def createLocation(name : String, data : Map[String, Any]) : Resource = {
    val dataProp = checkForName(name, data)
    val location = addRes(getRes(APRJLocationNames.Location), name, null)
    addPropData(dataProp, location)
    location
  }

  def addGeoLocation(locationResource : Resource, lat : String, long : String, compact : Boolean = false) : Resource = {
    val location = findResource(locationResource)
    val element = getProp(APRJLocationNames.Point)
    val elementResource = RES(null)
    if (compact) {
      addProp(APRJLocationNames.lat_long, lat + "-" + long, elementResource)
    } else {
      addProp(APRJLocationNames.lat, lat, elementResource)
      addProp(APRJLocationNames.long, long, elementResource)
    }
    location.addProperty(element, elementResource)
    location
  }

  def addFoundationDate(locationResource : Resource, year : Int, month : Int, day : Int,
                        dateType : String = APRJDateTimeNames.typeGregorian,
                        dateStatus : String = null, compact : Boolean = false) : Resource = {
    val location = findResource(locationResource)
    val dateModel = new DateTimeRdfModel
    val resDate = dateModel.createDate(APRJLocationNames.foundationDate)
    if (dateStatus != null) dateModel.addProp(APRJDateTimeNames.status, dateStatus, resDate)
    val dateResource = dateModel.addDate(resDate, year, month, day, dateType, compact)
    addRDFModelToRes(dateModel, location, APRJLocationNames.foundationDate)
  }

  def addAbandonmentDate(locationResource : Resource, year : Int, month : Int, day : Int,
                         dateType : String = APRJDateTimeNames.typeGregorian,
                         dateStatus : String = null, compact : Boolean = false) : Resource = {
    val location = findResource(locationResource)
    val dateModel = new DateTimeRdfModel
    val resDate = dateModel.createDate(APRJLocationNames.abandonmentDate)
    if (dateStatus != null) dateModel.addProp(APRJDateTimeNames.status, dateStatus, resDate)
    val dateResource = dateModel.addDate(resDate, year, month, day, dateType, compact)
    addRDFModelToRes(dateModel, location, APRJLocationNames.abandonmentDate)
  }

  private def validPredicate(p : Property) : Boolean = {
    p.getLocalName match {
      case "type"
        | "subject"
        | "uuid"
        | "creator"
        | "person"
        | "title" => false
      case _ => true
    }
  }

  def getDate(resourceName : String, resourceType : String = APRJLocationNames.foundationDate) : Map[String, String] = {
    val resource = findStatement(resourceName, resourceType)
    var accumulator = Map[String, String]()
    if (resource != null) {
      val resourceObject = resource.getObject.asInstanceOf[Resource]
      val properties = resourceObject.listProperties
      while (properties.hasNext) {
        val p = properties.next
        if (validPredicate(p.getPredicate))
          accumulator ++= Map[String, String](p.getPredicate.getLocalName -> p.getObject.toString)
      }
    }
    accumulator
  }
}
