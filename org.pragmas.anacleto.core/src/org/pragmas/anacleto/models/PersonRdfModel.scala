/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.models

import com.hp.hpl.jena.rdf.model._
import com.hp.hpl.jena.vocabulary._
import com.hp.hpl.jena.datatypes.xsd._
import com.hp.hpl.jena.datatypes._
import com.hp.hpl.jena.datatypes.xsd.impl._
//import com.hp.hpl.jena.query.QueryFactory
//import com.hp.hpl.jena.query.QueryExecutionFactory
//import com.hp.hpl.jena.query.ResultSetFormatter
import com.hp.hpl.jena.query._
import com.hp.hpl.jena.rdf.model.impl.StatementImpl

import java.io.StringWriter
import java.util.UUID
import scala.collection.mutable.{ ListBuffer }

class PersonRdfModel extends RDFModelTasks {

  override def NS : String = APRJPersonNames.NS

  model.setNsPrefix(APRJPersonNames.SUFFIX, APRJPersonNames.NS)
  model.setNsPrefix(APRJNames.SUFFIX, APRJNames.NS)
  model.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#")
  //model.setNsPrefix("geo", "http://www.w3.org/2003/01/geo/wgs84_pos#")
  model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#")
  model.setNsPrefix("dc", "http://purl.org/dc/elements/1.1/")

  override def getRes(res : String) : Resource = {
    super.getRes(res)
  }

  override def getProp(prop : String) : Property = {
    prop match {
      case APRJPersonNames.Person => PROP(APRJPersonNames.Person, APRJPersonNames.NS)
      case APRJPersonNames.uuid => PROP(APRJPersonNames.uuid, APRJPersonNames.NS)
      case APRJPersonNames.name => PROP(APRJPersonNames.name, APRJPersonNames.NS)
      case _ => super.getProp(prop)
    }
  }

  def createPerson(name : String, data : Map[String, Any] = null) : Resource = {
    val dataProp = checkForName(name, data)
    val person = addRes(getRes(APRJPersonNames.Person), name, null)
    addPropData(dataProp, person)
    person
  }

  def addLocation(personResource : Resource, locationModel : LocationRdfModel, name : String = "location") : Resource = {
    val person = findResource(personResource)
    addRDFModelToRes(locationModel, person, name)
  }

  def addBirthDate(personResource : Resource, year : Int, month : Int, day : Int,
                   dateType : String = APRJDateTimeNames.typeGregorian,
                   dateStatus : String = null, compact : Boolean = false) : Resource = {
    val person = findResource(personResource)
    val dateModel = new DateTimeRdfModel
    val resDate = dateModel.createDate(APRJPersonNames.birthDate)
    if (dateStatus != null) dateModel.addProp(APRJDateTimeNames.status, dateStatus, resDate)
    val dateResource = dateModel.addDate(resDate, year, month, day, dateType, compact)
    addRDFModelToRes(dateModel, person, APRJPersonNames.birthDate)
  }

  def addDeathDate(personResource : Resource, year : Int, month : Int, day : Int,
                   dateType : String = APRJDateTimeNames.typeGregorian,
                   dateStatus : String = null, compact : Boolean = false) : Resource = {
    val person = findResource(personResource)
    val dateModel = new DateTimeRdfModel
    val resDate = dateModel.createDate(APRJPersonNames.deathDate)
    if (dateStatus != null) dateModel.addProp(APRJDateTimeNames.status, dateStatus, resDate)
    val dateResource = dateModel.addDate(resDate, year, month, day, dateType, compact)
    addRDFModelToRes(dateModel, person, APRJPersonNames.deathDate)
  }

  def addReferenceTo(personResource : Resource, reference : String, referenceType : String = APRJPersonNames.referenceTypesList(0)) : Resource = {
    //val person = relinkResource(personResource)
    //println("---------------------------------")
    val person = personResource
    val element = getProp(APRJPersonNames.ReferenceTo)
    val elementResource = RES(null)
    addProp(APRJPersonNames.reference, reference, elementResource)
    addProp(APRJPersonNames.referenceType, referenceType, elementResource)
    person.addProperty(element, elementResource)
    //println("---------------------------------")
    person
  }

  def addReferenceBy(personResource : Resource, reference : String, referenceType : String = APRJPersonNames.referenceTypesList(0)) : Resource = {
    val person = findResource(personResource)
    val element = getProp(APRJPersonNames.ReferenceBy)
    val elementResource = RES(null)
    addProp(APRJPersonNames.reference, reference, elementResource)
    addProp(APRJPersonNames.referenceType, referenceType, elementResource)
    person.addProperty(element, elementResource)
    person
  }

  def getGivenName(personResourceName : String) : String = {
    val statement = findStatementValue(personResourceName, APRJPersonNames.givenName)
    if (statement != null) statement.toString else ""
  }

  def getFamilyName(personResourceName : String) : String = {
    val statement = findStatementValue(personResourceName, APRJPersonNames.familyName)
    if (statement != null) statement.toString else ""
  }

  def getNickName(personResourceName : String) : String = {
    val statement = findStatementValue(personResourceName, APRJPersonNames.nick)
    if (statement != null) statement.toString else ""
  }

  def getBio(personResourceName : String) : String = {
    val statement = findStatementValue(personResourceName, APRJPersonNames.bio)
    if (statement != null) statement.toString else ""
  }

  private def validPredicate(p : Property) : Boolean = {
    p.getLocalName match {
      case "type"
        | "subject"
        | "uuid"
        | "creator"
        | "person"
        | "title" => false
      case _ => true
    }

  }

  def getDate(resourceName : String, resourceType : String = APRJPersonNames.birthDate) : Map[String, String] = {
    val resource = findStatement(resourceName, resourceType)
    var accumulator = Map[String, String]()
    if (resource != null) {
      val resourceObject = resource.getObject.asInstanceOf[Resource]
      val properties = resourceObject.listProperties
      while (properties.hasNext) {
        val p = properties.next
        //val name = p.getPredicate.getLocalName
        if (validPredicate(p.getPredicate))
          accumulator ++= Map[String, String](p.getPredicate.getLocalName -> p.getObject.toString)
      }
    }
    accumulator
  }

  def getLocation(resourceName : String, resourceType : String = APRJPersonNames.birthLocation) : Map[String, String] = {
    val resource = findStatement(resourceName, resourceType)
    var accumulator = Map[String, String]()
    if (resource != null) {
      val resourceObject = resource.getObject.asInstanceOf[Resource]
      val properties = resourceObject.listProperties

      while (properties.hasNext) {
        val p = properties.next
        //val name = p.getPredicate.getLocalName
        val predicate = p.getPredicate
        if (validPredicate(predicate))
          accumulator ++= Map[String, String](predicate.getLocalName -> p.getObject.toString)
      }
    }
    accumulator
  }

  def getReferences(resourceName : String, resourceType : String) : List[Map[String, String]] = {
    var accumulator = ListBuffer[Map[String, String]]()
    val resource = findStatement(resourceName, resourceType)
    if (resource != null) {
      val resourceObject = resource.getObject.asInstanceOf[Resource]
      var statements = findStatements(resourceObject)

      statements foreach { stat =>
        val resourceObject = stat.getObject.asInstanceOf[Resource]
        var statements = findStatements(resourceObject)
        var dataMap = Map[String, String]()
        statements foreach { statement =>
          val predicate = statement.getPredicate
          if (validPredicate(predicate))
            dataMap ++= Map[String, String](predicate.getLocalName -> statement.getObject.toString)
        }
        accumulator += dataMap
      }
    }
    accumulator.toList
  }

  def addReferences(resourceName : Resource, resourceType : String, references : List[Map[String, String]]) : Resource = {
    val resource = findResource(resourceName)
    val element = getProp(resourceType)
    val elementResource = RES(null)
    references foreach { ref : Map[String, String] =>
      val elementSub = resourceType match {
        case APRJPersonNames.Locations => getProp(APRJPersonNames.LocationsRef)
        case APRJPersonNames.Persons => getProp(APRJPersonNames.PersonsRef)
        case _ => getProp(APRJPersonNames.LocationsRef)
      }
      val elementSubResource = RES(null)
      addProp(APRJPersonNames.reference, ref(APRJPersonNames.reference), elementSubResource)
      addProp(APRJPersonNames.referenceType, ref(APRJPersonNames.referenceType), elementSubResource)
      addProp(APRJPersonNames.referenceGrade, ref(APRJPersonNames.referenceGrade), elementSubResource)
      elementResource.addProperty(elementSub, elementSubResource)
    }
    resource.addProperty(element, elementResource)
    resource
  }

}
