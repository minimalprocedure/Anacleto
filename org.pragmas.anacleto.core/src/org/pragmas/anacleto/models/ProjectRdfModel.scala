/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package  org.pragmas.anacleto.models

import com.hp.hpl.jena.rdf.model._
import com.hp.hpl.jena.vocabulary._
import com.hp.hpl.jena.datatypes.xsd._
import com.hp.hpl.jena.datatypes._
import com.hp.hpl.jena.datatypes.xsd.impl._
//import com.hp.hpl.jena.query.QueryFactory
//import com.hp.hpl.jena.query.QueryExecutionFactory
//import com.hp.hpl.jena.query.ResultSetFormatter
import com.hp.hpl.jena.query._
import com.hp.hpl.jena.rdf.model.impl.StatementImpl

import java.io.StringWriter
import java.util.UUID
import scala.collection.mutable.ListBuffer


class ProjectRdfModel extends RDFModelTasks {

  override def NS : String = APRJNames.NS

  model.setNsPrefix(APRJNames.SUFFIX, APRJNames.NS)
  model.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#")
  model.setNsPrefix("geo", "http://www.w3.org/2003/01/geo/wgs84_pos#")
  model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#")
  model.setNsPrefix("dc", "http://purl.org/dc/elements/1.1/")

  private val PRED_PLACEHOLDER = "@@PRED_PLACEHOLDER@@"

  private val QUERY_PREFIX = """
    PREFIX %1$s: <%2$s>
    PREFIX dc: <http://purl.org/dc/elements/1.1/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
        """.format(APRJNames.SUFFIX, APRJNames.NS)

  private def OPTIONAL_DC_FIELDS(predicate : String) = {"""

    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:title ?title}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:subject ?subject}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:contributor ?contributor}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:coverage ?coverage}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:creator ?creator}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:date ?date}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:description ?description}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:format ?format}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:identifier ?identifier}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:language ?language}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:publisher ?publisher}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:relation ?relation}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:rights ?rights}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:source ?source}
    OPTIONAL {?@@PRED_PLACEHOLDER@@ dc:type ?type}

        """.replaceAll(PRED_PLACEHOLDER, predicate)
  }

  private def OPTIONAL_GEO_FIELDS = {"""

    OPTIONAL {?geo geo:lat ?latitude}
    OPTIONAL {?geo geo:long ?longitude}

        """
  }

  private def OPTIONAL_IMAGE_FIELDS = {"""

    OPTIONAL {?image aprj:localpath ?localpath}
    OPTIONAL {?image aprj:height ?height}
    OPTIONAL {?image aprj:width ?width}
    OPTIONAL {?image aprj:resolution ?resolution}
    OPTIONAL {?image aprj:size ?size}
    OPTIONAL {?image aprj:Filters ?Filters}
    OPTIONAL {?image aprj:active ?active}
        """
  }

  override def getRes(res : String) : Resource = {
    super.getRes(res)
  }

  override def getProp(prop : String) : Property = {
    prop match {
      case _ => super.getProp(prop)
    }
  }

  def createProject(name : String, data : Map[String, Any]) : Resource = {
    val dataProp = checkForName(name, data)
    val project = addRes(getRes(APRJNames.Project), name, null)
    addPropData(dataProp, project)
    project
  }

  def createPage(name : String, data : Map[String, Any]) : Resource = {
    val dataProp = checkForName(name, data)
    val page = addRes(getRes(APRJNames.Page), name, null)
    addPropData(dataProp, page)
    page
  }

  def addImageToPage(name : String, data : Map[String, Any], page : Resource) : Resource = {
    addElementToPage(APRJNames.image, name, data, page)
  }

  def addScriptToPage(name : String, data : Map[String, Any], page : Resource) : Resource = {
    addElementToPage(APRJNames.script, name, data, page)
  }

  def addLocationToPage(name : String, data : Map[String, Any], page : Resource) : Resource = {
    addElementToPage(APRJNames.location, name, data, page)
  }

//  def addGeoLocation(lat: String, long : String, location : Resource) : Resource = {
//    val element = getProp(APRJNames.point)
//    val elementResource = RES(null)
//    addProp(APRJNames.lat, lat, elementResource)
//    addProp(APRJNames.long, long, elementResource)
//    location.addProperty(element, elementResource)
//  }

  def addPersonToPage(name : String, data : Map[String, Any], page : Resource) : Resource = {
    addElementToPage(APRJNames.person, name, data, page)
  }

  def addNoteToPage(name : String, data : Map[String, Any], page : Resource) : Resource = {
    addElementToPage(APRJNames.note, name, data, page)
  }

  def addSummaryToPage(name : String, data : Map[String, Any], page : Resource) : Resource = {
    addElementToPage(APRJNames.summary, name, data, page)
  }

  def addTagToPage(name : String, data : Map[String, Any], page : Resource) : Resource = {
    addElementToPage(APRJNames.tag, name, data, page)
  }

  def addCommentToPage(name : String, data : Map[String, Any], page : Resource) : Resource = {
    addElementToPage(APRJNames.comment, name, data, page)
  }

  def addElementToPage(elementName : String, name : String, data : Map[String, Any], pageResource : Resource) : Resource = {
    val page = findResource(pageResource)
    val element = getProp(elementName)
    val elementResource = RES(null)
    if(!data.contains(APRJNames.name)) addProp(APRJNames.name, name, elementResource)
    if(!data.contains(APRJNames.uuid)) addProp(APRJNames.uuid, UUID.randomUUID.toString, elementResource)
    addPropData(data, elementResource)
    page.addProperty(element, elementResource)
    elementResource
  }

  def addElementToPage(element : Property, name : String, data : Map[String, Any], pageResource : Resource) : Resource = {
    val page = findResource(pageResource)
    val elementResource = RES(null)
    if(!data.contains(APRJNames.name)) addProp(APRJNames.name, name, elementResource)
    if(!data.contains(APRJNames.uuid)) addProp(APRJNames.uuid, UUID.randomUUID.toString, elementResource)
    addPropData(data, elementResource)
    page.addProperty(element, elementResource)
    elementResource
  }

  private def simpleQueryString(dataType : String, limit : Int) : String = {
    """
      %s
      SELECT *
      WHERE {
        ?predicate rdf:type ?type .
        ?predicate aprj:name ?name .
        ?predicate aprj:uuid ?uuid .
        OPTIONAL {?predicate aprj:subtypes ?subtypes} .
        FILTER (?type = %s) .
        %s
    }
    %s
    """.format(QUERY_PREFIX, dataType, OPTIONAL_DC_FIELDS(APRJNames.predicate), if(limit > 0) "LIMIT " + limit.toString else "")
  }

  private def simpleQueryStringForElements(elementType : String, pageName : String, limit : Int) : String = {
    """
      %s
      SELECT *
      WHERE {
        {?predicate aprj:name ?page_name .
        ?predicate rdf:type ?page_type .
        FILTER (?page_type = aprj:Page) .
        FILTER (?page_name = "%s")}
        ?predicate aprj:@@ELEMENT_TYPE@@ ?@@ELEMENT_TYPE@@ .
        ?@@ELEMENT_TYPE@@ aprj:name ?name .
        ?@@ELEMENT_TYPE@@ aprj:uuid ?uuid .
        OPTIONAL {?@@ELEMENT_TYPE@@ aprj:subtypes ?subtypes} .
        %s
        %s
    }
    %s
    """
      .replaceAll("@@ELEMENT_TYPE@@", elementType)
      .format(QUERY_PREFIX, pageName,
              OPTIONAL_DC_FIELDS(elementType),
              if(elementType == APRJNames.image) OPTIONAL_IMAGE_FIELDS else "",
              if(limit > 0) "LIMIT " + limit.toString else "")
  }

  private def normalizeNameTitle(data : Map[String, Any]) : Map[String, Any] = {
    var result = data
    if(!result.contains(APRJNames.title))
      result += (APRJNames.title -> result(APRJNames.name))
    result
  }

  def getProjectData : Map[String, Any] = {
    var result : Map[String, String] = Map()
    val query = QueryFactory.create(simpleQueryString("aprj:Project", 1))
    val qe = QueryExecutionFactory.create(query, model)
    val results = qe.execSelect
    //qe.close
    //ResultSetFormatter.out(System.out, results, query)
    if (results.hasNext) {
      var projNode = results.next
      val fields = results.getResultVars.toArray(new Array[String](results.getResultVars.size))
      for (field <- fields){
        var fieldObject = projNode.get(field)
        if (fieldObject != null)
          result += (field -> fieldObject.toString)
      }
    }
    qe.close
    normalizeNameTitle(result)
  }

  def getPagesData : List[Map[String, Any]] = {
    var resultData : List[Map[String, Any]] = List()
    val query = QueryFactory.create(simpleQueryString("aprj:Page", 0))
    val qe = QueryExecutionFactory.create(query, model)
    val results = qe.execSelect
    //ResultSetFormatter.out(System.out, results, query)
    while (results.hasNext) {
      var page = Map[String, String]()
      val projNode = results.next
      val fields = results.getResultVars.toArray(new Array[String](results.getResultVars.size))
      for (field <- fields){
        var fieldObject = projNode.get(field)
        if (fieldObject != null)
          page += (field -> fieldObject.toString)
      }
      resultData = resultData ++ List[Map[String, Any]](normalizeNameTitle(page))
    }
    qe.close
    resultData
  }

  def getElementsfromPage(elementType : String, pageName : String) : List[Map[String, Any]] = {
    var resultData : List[Map[String, Any]] = List()
    val query = QueryFactory.create(simpleQueryStringForElements(elementType, pageName, 0))
    val qe = QueryExecutionFactory.create(query, model)
    val results = qe.execSelect
    //ResultSetFormatter.out(System.out, results, query)
    while (results.hasNext) {
      var page = Map[String, Any]()
      val projNode = results.next

      val fields = results.getResultVars.toArray(new Array[String](results.getResultVars.size))
      for (field <- fields){
          var fieldObject = projNode.get(field)
          if (fieldObject != null) {
            val value = filterField(fieldObject.toString)
            if(value != null) {
              if(elementType == APRJNames.image && field == APRJNames.Filters)
                page += (field -> getPropertiesNameList(fieldObject.asInstanceOf[Resource]))
              else
                page += (field -> value)
            }
          }
      }
      resultData = resultData ++ List[Map[String, Any]](normalizeNameTitle(page))
    }
    qe.close
    resultData
  }

  def dumpForTest : Unit = {
    model.write(System.out, "TURTLE")
    println("-------------------------------------------------")
    model.write(System.out, "RDF/XML-ABBREV")
    println("-------------------------------------------------")
    model.write(System.out,"N-TRIPLE")
    println("-------------------------------------------------")
    model.write(System.out,"N3")
    println("-------------------------------------------------")
    model.write(System.out)
  }

}
