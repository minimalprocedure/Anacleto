/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.models

object APRJPersonNames {

  val SUFFIX = "pers"
  val TYPE = "person"
  val NS = "http://pragmas.org/anacleto/1.0/project/%1$s#".format(TYPE)

  val Person = "Person"
  val uuid = "uuid"
  val name = "name"
  val nick = "nick"
  val title = "title"
  val surname = "surname"
  val familyName = "familyName"
  val givenName = "givenName"
  val age = "age"
  val birthDate = "birthDate"
  val deathDate = "deathDate"
  val birthLocation = "birthLocation"
  val deathLocation = "deathLocation"

  val bio = "bio"

  val Images = "Images"
  val Locations = "Locations"
  val LocationsRef = "LocationsRef"

  val Persons = "Persons"
  val PersonsRef = "PersonsRef"

  val ReferenceBy = "ReferenceBy"
  val ReferenceTo = "ReferenceTo"
  val referenceType = "referenceType"
  val referenceGrade = "referenceGrade"
  val reference = "reference"

  val listItem = "item"

  val referenceGradeList = List[String]("0", "1", "2", "3", "4", "5")

  val referenceTypesList = List[String](
    "simple",
    "personal",
    "friend",
    "father",
    "mother",
    "son",
    "daughter",
    "brother",
    "sister",
    "uncle",
    "aunt",
    "nephew",
    "niece",
    "cousin",
    "grandfather",
    "grandmother",
    "great-grandparent",
    "wife",
    "husband",
    "father-in-law",
    "mother-in-law",
    "lover",
    "other")

  /* FOAF Basics */
  //val Agent = "Agent"

  //val title = "title"
  //val homepage = "homepage"
  //val mbox = "mbox"
  //val mbox_sha1sum = "mbox_sha1sum"
  //val img = "img"

  //val depicts = "depicts"
  //val depiction = "depiction"

  /* FOAF Personal Info */
  //val weblog = "weblog"
  //val knows = "knows"
  //val interest = "interest"
  //val currentProject = "currentProject"
  //val pastProject = "pastProject"
  //val plan = "plan"
  //val based_near = "based_near"
  //val age = "age"
  //val workplaceHomepage = "workplaceHomepage"
  //val workInfoHomepage = "workInfoHomepage"
  //val schoolHomepage = "schoolHomepage"
  //val topic_interest = "topic_interest"
  //val publications = "publications"
  //val geekcode = "geekcode"
  //val myersBriggs = "myersBriggs"
  //val dnaChecksum = "dnaChecksum"

  /* FOAF Online Accounts */
  //val OnlineAccount = "OnlineAccount"
  //val OnlineChatAccount = "OnlineChatAccount"
  //val OnlineEcommerceAccount = "OnlineEcommerceAccount"
  //val OnlineGamingAccount = "OnlineGamingAccount"
  //val account = "account"
  //val accountServiceHomepage = "accountServiceHomepage"
  //val accountName = "accountName"
  //val icqChatID = "icqChatID"
  //val msnChatID = "msnChatID"
  //val aimChatID = "aimChatID"
  //val jabberID = "jabberID"
  //val yahooChatID = "yahooChatID"
  //val skypeID = "skypeID"

  /* FOAF Projects and Groups */
  //val Project = "Project"
  //val Organization = "Organization"
  //val Group = "Group"
  //val member = "member"
  //val membershipClass = "membershipClass"

  /* FOAF Documents and Images */
  //val Document = "Document"
  //val Image = "Image"
  //val PersonalProfileDocument = "PersonalProfileDocument"

  //val page = "page"
  //val topic = "topic"

  //val primaryTopicOf = "primaryTopicOf"
  //val primaryTopic = "primaryTopic"

  //val tipjar = "tipjar"
  //val sha1 = "sha1"

  //val maker = "maker"
  //val made = "made"

  //val thumbnail = "thumbnail"
  //val logo = "logo"

  /* PERSON Names */

}

