/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.storage

import java.io._
import java.net.URI
import org.eclipse.core.filesystem._
import org.eclipse.core.internal.filesystem._
import org.eclipse.core.runtime._
import org.eclipse.osgi.util.NLS

import org.eclipse.core.filesystem.provider.FileStore
import org.eclipse.core.filesystem.provider.FileInfo

class ProjectFileStore extends FileStore {
  override def toURI : URI = {
    new URI("")
  }

  override def childNames(options : Int, monitor : IProgressMonitor) : Array[String] = {
    new Array[String](2)
  }


  override def fetchInfo(options : Int, monitor : IProgressMonitor) : IFileInfo = {
    new FileInfo
  }

  override def getChild(name : String) : IFileStore = {
    this
  }

  override def getParent : IFileStore = {
    this
  }

  override def getName : String = {
    "aaa"
  }
  override def openInputStream(options : Int, monitor : IProgressMonitor) : InputStream = {
    new FileInputStream("nomefile")
  }
}
