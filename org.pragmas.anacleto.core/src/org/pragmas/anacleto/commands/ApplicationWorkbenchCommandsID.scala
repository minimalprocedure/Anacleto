/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.commands

import org.eclipse.ui.IPageLayout

object ActionCode {
  val DEFAULT = null
  val SAVE = "save"
  val NEW = "new"
  val CLOSE = "close"
  val DELETE = "delete"
  val OPEN = "open"
  val ADD = "add"
  val RENAME = "rename"
  val REMOVE = "delete"
  val ADD_GIT = "add_git"
  val SHOW = "show"
  val ARCHIVE = "archive"
  val FIND = "find"
}

object WorkbenchCommandsIDs {
  val PROJECT_ADD_ELEMENT = "org.pragmas.anacleto.project.add"
  val PROJECT_REMOVE_ELEMENT = "org.pragmas.anacleto.project.remove"
  val PROJECT_RENAME_ELEMENT = "org.pragmas.anacleto.project.rename"
  val PROJECT_MENU = "org.pragmas.anacleto.project"
  val PROJECT_ELEMENTS_SUBMENU = "org.pragmas.anacleto.project.sub.elements"

  val PROJECT_NEW = "org.pragmas.anacleto.project.new"
  val PROJECT_SAVE = "org.pragmas.anacleto.project.save"
  val PROJECT_OPEN = "org.pragmas.anacleto.project.open"
  val PROJECT_CLOSE = "org.pragmas.anacleto.project.close"
  val PROJECT_ARCHIVE = "org.pragmas.anacleto.project.archive"

  val PROJECT_ADD_GIT = "org.pragmas.anacleto.project.add_git"

  val WINDOW_SHOW_VIEW_SEARCH = "org.pragmas.anacleto.window.show.view.search"
  val WINDOW_SHOW_VIEW_PROJECTMANAGER = "org.pragmas.anacleto.window.show.view.projectmanager"
  val WINDOW_SHOW_VIEW_PROPERTIES = "org.pragmas.anacleto.window.show.view" + "." + IPageLayout.ID_PROP_SHEET
  val WINDOW_SHOW_VIEW_OUTLINE = "org.pragmas.anacleto.window.show.view" + "." + IPageLayout.ID_OUTLINE

  val NAVIGATION_FIND_MACROS = "org.pragmas.anacleto.navigation.find.macros"
}
