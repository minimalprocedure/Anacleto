/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.commands

import org.eclipse.jface.action.Action
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.dialogs.InputDialog
import org.eclipse.swt.widgets.FileDialog
import org.eclipse.swt.widgets.DirectoryDialog
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Shell
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.actions.ActionFactory
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction
import org.eclipse.ui.actions.ContributionItemFactory
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.ui.IWorkbenchActionConstants
import org.eclipse.jface.action.MenuManager
import org.eclipse.jface.action.IMenuListener
import org.eclipse.jface.action.IMenuManager
import org.eclipse.swt.widgets.MenuItem
import org.eclipse.jface.action.Separator
import org.eclipse.jface.action.ActionContributionItem
import org.eclipse.ui.application.ActionBarAdvisor
import org.eclipse.ui.ide.IDEActionFactory
import org.pragmas.anacleto.ui.ApplicationActionBarAdvisor
import org.pragmas.anacleto.ui.Perspective

class ApplicationFileMenuManager
    extends MenuManager("&File", IWorkbenchActionConstants.M_FILE) {

  val actionFileSave = ActionFactory.SAVE.create(Perspective.window)
  val actionFileSaveAs = ActionFactory.SAVE_AS.create(Perspective.window)
  val actionFileSaveAll = ActionFactory.SAVE_ALL.create(Perspective.window)
  val actionExit = ActionFactory.QUIT.create(Perspective.window)
  val actionShowEditor = ActionFactory.SHOW_EDITOR.create(Perspective.window)
  //val actionPrint = ActionFactory.PRINT.create(Perspective.window)


  //private val projectMenu = new ApplicationProjectMenuManager

  Perspective.actionBar.register(actionFileSave)
  Perspective.actionBar.register(actionFileSaveAs)
  Perspective.actionBar.register(actionFileSaveAll)
  Perspective.actionBar.register(actionExit)
  Perspective.actionBar.register(actionShowEditor)

  add(new Separator)
  //add(projectMenu)
  add(new Separator)
  add(actionFileSave)
  add(actionFileSaveAs)
  add(actionFileSaveAll)
  //add(new Separator)
  //add(actionPrint)
  add(new Separator)
  add(actionExit)
  add(actionShowEditor)

  Perspective.actionBar.menuBar.add(this)
}

class ApplicationEditMenuManager
    extends MenuManager("&Edit", IWorkbenchActionConstants.M_EDIT) {

  val actionEditUndo = ActionFactory.UNDO.create(Perspective.window)
  val actionEditRedo = ActionFactory.REDO.create(Perspective.window)
  val actionEditCut = ActionFactory.CUT.create(Perspective.window)
  val actionEditCopy = ActionFactory.COPY.create(Perspective.window)
  val actionEditPaste = ActionFactory.PASTE.create(Perspective.window)
  val actionEditDelete = ActionFactory.DELETE.create(Perspective.window)
  val actionEditSelectAll = ActionFactory.SELECT_ALL.create(Perspective.window)
  val actionEditFind = ActionFactory.FIND.create(Perspective.window)

  val actionEditPreferences = ActionFactory.PREFERENCES.create(Perspective.window)

  Perspective.actionBar.register(actionEditUndo)
  Perspective.actionBar.register(actionEditRedo)
  Perspective.actionBar.register(actionEditCut)
  Perspective.actionBar.register(actionEditCopy)
  Perspective.actionBar.register(actionEditPaste)
  Perspective.actionBar.register(actionEditDelete)
  Perspective.actionBar.register(actionEditSelectAll)
  Perspective.actionBar.register(actionEditFind)
  Perspective.actionBar.register(actionEditPreferences)

  add(new Separator)
  add(actionEditUndo)
  add(actionEditRedo)
  add(new Separator)
  add(actionEditCut)
  add(actionEditCopy)
  add(actionEditPaste)
  add(new Separator)
  add(actionEditDelete)
  add(actionEditSelectAll)
  add(new Separator)
  add(actionEditFind)
  add(new Separator)
  add(actionEditPreferences)

  Perspective.actionBar.menuBar.add(this)
}

class ApplicationWindowMenuManager
    extends MenuManager("&Window", IWorkbenchActionConstants.M_WINDOW) {

  val actionWindowOpenNewWindow = ActionFactory.OPEN_NEW_WINDOW.create(Perspective.window)
  private val actionWindowShowViews = ActionFactory.SHOW_VIEW_MENU.create(Perspective.window)
  val actionsOtherViewsList = ContributionItemFactory.VIEWS_SHORTLIST.create(Perspective.window)

  val viewMenu = new MenuManager("&Open view...", IWorkbenchActionConstants.M_WINDOW)
  val actionWindowShowViewSearch = new WindowShowViewSearch
  val actionWindowShowViewProjectManager = new WindowShowViewProjectManager
  val actionWindowShowViewProperties = new WindowShowViewProperties
  val actionWindowShowViewOutline = new WindowShowViewOutline

  val actionWindowToggleCoolbar = ActionFactory.TOGGLE_COOLBAR.create(Perspective.window)
  val actionWindowShowOpenEditors = ActionFactory.SHOW_OPEN_EDITORS.create(Perspective.window)
  val actionWindowShowNewEditor = ActionFactory.NEW_EDITOR.create(Perspective.window)

  viewMenu.add(actionWindowShowViewProjectManager)
  viewMenu.add(actionWindowShowViewSearch)
  viewMenu.add(actionWindowShowViewProperties)
  viewMenu.add(actionWindowShowViewOutline)
  viewMenu.add(actionsOtherViewsList)

  Perspective.actionBar.register(actionWindowOpenNewWindow)
  Perspective.actionBar.register(actionWindowShowViews)

  add(new Separator)
  add(actionWindowOpenNewWindow)
  add(actionWindowShowOpenEditors)
  add(actionWindowShowNewEditor)

  add(new Separator)
  add(actionWindowToggleCoolbar)
  add(new Separator)
  add(actionWindowShowViews)
  add(new Separator)
  add(viewMenu)

  Perspective.actionBar.menuBar.add(this)
}

class ApplicationAboutMenuManager
    extends MenuManager("&About", IWorkbenchActionConstants.M_HELP) {

  val actionAbout = ActionFactory.ABOUT.create(Perspective.window)
  val actionShowHelp = ActionFactory.HELP_CONTENTS.create(Perspective.window)
  val actionSearchHelp = ActionFactory.HELP_SEARCH.create(Perspective.window)
  val actionDynamicHelp = ActionFactory.DYNAMIC_HELP.create(Perspective.window)


  Perspective.actionBar.register(actionAbout)
  Perspective.actionBar.register(actionShowHelp)
  Perspective.actionBar.register(actionSearchHelp)
  Perspective.actionBar.register(actionDynamicHelp)

  add(new Separator)
  add(actionShowHelp)
  add(actionSearchHelp)
  add(actionDynamicHelp)
  add(new Separator)
  add(actionAbout)

  Perspective.actionBar.menuBar.add(this)
}

class ApplicationNavigateMenuManager
    extends MenuManager("&Navigate", IWorkbenchActionConstants.MENU_PREFIX + IWorkbenchActionConstants.BOOKMARK) {

  //val actionBookmark = IDEActionFactory.BOOKMARK.create(Perspective.window)
  //val actionAddTask = IDEActionFactory.ADD_TASK.create(Perspective.window)
  val actionFindMacro = new NavigationFindMacros

  //Perspective.actionBar.register(actionBookmark)

  add(new Separator)
  //add(actionBookmark)
  //add(actionAddTask)
  add(actionFindMacro)

  Perspective.actionBar.menuBar.add(this)
}

