/**
 * *****************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 * ****************************************************************************
 */
package org.pragmas.anacleto.commands

import org.eclipse.jface.action.Action
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.dialogs.IDialogConstants
import org.eclipse.jface.dialogs.InputDialog
import org.eclipse.swt.widgets.DirectoryDialog
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Shell
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction
import org.eclipse.ui.IWorkbenchWindow
import org.eclipse.jface.action.MenuManager
import org.eclipse.jface.action.IMenuListener
import org.eclipse.jface.action.IMenuManager
import org.eclipse.jface.action.IContributionItem
import org.eclipse.swt.widgets.MenuItem
import org.eclipse.jface.action.Separator
import org.eclipse.jface.action.ActionContributionItem
import org.eclipse.jface.wizard.WizardDialog
import org.pragmas.anacleto.ui.views.ProjectManager
import org.pragmas.anacleto.ui.ApplicationActionBarAdvisor
import org.pragmas.anacleto.ui.dialogs._
import org.pragmas.anacleto.ui.editors.ImageEditor
import org.pragmas.anacleto.tools.ResourceManager
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.ProjectResources.ProjectTypes
import org.pragmas.anacleto.ProjectResources.ProjectNodeTools
import org.pragmas.anacleto.extensions.ExtensionType

class ProjectMenuElementManagerListener extends IMenuListener {

  def menuAboutToShow(manager : IMenuManager) : Unit = {
    Perspective.actionBar.menuProject.reloadIconsForElements
  }

}

class ProjectMenuManagerListener extends IMenuListener {

  def menuAboutToShow(manager : IMenuManager) : Unit = {
    if (Perspective.projectManager.currentNode != null)
      Perspective.projectManager.currentNode.nodeType match {
        case ProjectTypes.PROJECT => Perspective.actionBar.menuProject.deactivateElementActions
        case _                    => Perspective.actionBar.menuProject.activateElementActions
      }
  }

}

class ApplicationProjectMenuManager extends MenuManager("&Project", WorkbenchCommandsIDs.PROJECT_MENU) {

  val menuElement = new MenuManager("&Elements", WorkbenchCommandsIDs.PROJECT_ELEMENTS_SUBMENU)

  var actionProjectAdd = new ProjectManagerAdd
  var actionProjectRename = new ProjectManagerRename
  var actionProjectRemove = new ProjectManagerRemove

  var actionProjectNew = new ProjectManagerNew
  var actionProjectOpen = new ProjectManagerOpen
  var actionProjectSave = new ProjectManagerSave
  var actionProjectClose = new ProjectManagerClose
  var actionProjectArchive = new ProjectManagerArchive

  //var actionProjectAddGit = new ProjectManagerAddGit

  add(new Separator)
  add(actionProjectNew)
  add(actionProjectOpen)
  add(actionProjectSave)
  add(new Separator)
  add(actionProjectClose)
  add(new Separator)
  add(actionProjectArchive)
  //add(new Separator)
  //add(actionProjectAddGit)
  add(new Separator)
  add(menuElement)
  menuElement.add(new Separator)
  menuElement.add(actionProjectAdd)
  menuElement.add(actionProjectRename)
  menuElement.add(new Separator)
  menuElement.add(actionProjectRemove)

  menuElement.addMenuListener(new ProjectMenuElementManagerListener)
  addMenuListener(new ProjectMenuManagerListener)

  def items : Array[MenuItem] = { this.getMenu.getItems }
  def menuElementMenuItems : Array[MenuItem] = {
    val menu = menuElement.getMenu
    if (menu != null)
      menuElement.getMenu.getItems
    else
      null
  }

  if (Perspective.actionBar != null) Perspective.actionBar.menuBar.add(this)

  private def reloadElementActionIcon(item : IContributionItem) : Unit = {
    if (Perspective.projectManager.currentNode != null && menuElement.isVisible) {
      val action : MenuItemNode = item.asInstanceOf[ActionContributionItem].getAction.asInstanceOf[MenuItemNode]
      val nodeType = ProjectTypes.RESOLVE_CHILD(Perspective.projectManager.currentNode.nodeType).toLowerCase
      var iconPath : String = null
      if (action.actionCode != null)
        iconPath = "silk/" + nodeType + "_" + action.actionCode + ".png"
      else
        iconPath = "silk/" + nodeType + ".png"
      action.setImageDescriptor(ResourceManager.getIconDescriptor(iconPath))
      item.update
    }
  }

  def deactivateElementActions : Unit = {
    val menu = menuElement.getMenu
    if (menu != null) menu.getParentItem.setEnabled(false)
  }

  def activateElementActions : Unit = {
    val menu = menuElement.getMenu
    if (menu != null) menu.getParentItem.setEnabled(true)
  }

  def reloadIconsForElements : Boolean = {
    if (Perspective.projectManager.currentNode != null) {
      val node = Perspective.projectManager.currentNode
      node.nodeType match {
        case ProjectTypes.PROJECT => deactivateElementActions
        case ProjectTypes.PAGE    => deactivateElementActions
        case _ => {
          if (node.subTypes.contains(ProjectTypes.SUBTYPE_REFERENCE))
            deactivateElementActions
          else
            activateElementActions
        }
      }
    } else deactivateElementActions
    val menu = menuElement.getMenu
    if (menu != null && menu.getParentItem.isEnabled) {
      val elementsItems = menuElement.getItems
      for (item <- elementsItems) {
        if (!item.isInstanceOf[Separator]) reloadElementActionIcon(item)
      }
      menu.getParentItem.isEnabled
    } else false
  }

}

class MenuItemNode extends Action with IWorkbenchAction {

  def setIcon(icon : String) = {
    setImageDescriptor(ResourceManager.getIconDescriptor(icon))
  }

  var actionCode : String = ActionCode.DEFAULT
  var normalText : String = null
  override def run : Unit = {}
  def dispose : Unit = {}

  def getWorkbenchWindow : IWorkbenchWindow = PlatformUI.getWorkbench.getActiveWorkbenchWindow

}

class ProjectManagerNew extends MenuItemNode {

  setId(WorkbenchCommandsIDs.PROJECT_NEW)
  normalText = "&New project"
  setText(normalText)
  setIcon("silk/book_new.png")

  actionCode = ActionCode.NEW

  override def run : Unit = {
    var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
    val dialog = new WizardDialog(shell, new NewProjectWizard)
    dialog.setTitle("New Project Wizard")
    dialog.open
  }

  override def dispose : Unit = {}

}

class ProjectManagerSave extends MenuItemNode {

  setId(WorkbenchCommandsIDs.PROJECT_SAVE)
  normalText = "&Save project"
  setText(normalText)
  setIcon("silk/book_save.png")

  actionCode = ActionCode.SAVE

  override def run : Unit = {
    Perspective.projectManager.saveProject
  }

  override def dispose : Unit = {}

}

class ProjectManagerOpen extends MenuItemNode {

  setId(WorkbenchCommandsIDs.PROJECT_OPEN)
  normalText = "&Open project"
  setText(normalText)
  setIcon("silk/book_open.png")

  actionCode = ActionCode.OPEN

  override def run : Unit = {
    var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
    val dialog = new DirectoryDialog(shell, SWT.OPEN)
    val fileSelected = dialog.open
    if (fileSelected != null)
      Perspective.projectManager.openProject(fileSelected)
  }

  override def dispose : Unit = {}

}

class ProjectManagerClose extends MenuItemNode {

  setId(WorkbenchCommandsIDs.PROJECT_CLOSE)
  normalText = "&Close project"
  setText(normalText)
  setIcon("silk/book_close.png")

  actionCode = ActionCode.CLOSE

  override def run : Unit = {

    if (Perspective.projectManager.isDirty) {
      var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
      val dialog = new MessageDialog(
        shell, "Save", null, "Project is unsaved, save?",
        MessageDialog.QUESTION_WITH_CANCEL,
        Array(IDialogConstants.YES_LABEL, IDialogConstants.NO_LABEL, IDialogConstants.CANCEL_LABEL), SWT.NONE)
      val result = dialog.open
      val force = if (result == 0) true else false
      if (result < 2)
        Perspective.projectManager.closeProject(if (result < 1) true else false)
    } else Perspective.projectManager.closeProject(false)
  }

  override def dispose : Unit = {}

}

class ProjectManagerArchive extends MenuItemNode {

  setId(WorkbenchCommandsIDs.PROJECT_ARCHIVE)
  normalText = "&Archive project"
  setText(normalText)
  setIcon("silk/compress.png")

  actionCode = ActionCode.ARCHIVE

  override def run : Unit = {
    Perspective.projectManager.archiveProject
  }

  override def dispose : Unit = {}

}

//class ProjectManagerAddGit extends MenuItemNode {
//
//  setId(WorkbenchCommandsIDs.PROJECT_ADD_GIT)
//  normalText = "&Add revision control"
//  setText(normalText)
//  setIcon("silk/revision_add.png")
//
//  actionCode = ActionCode.ADD_GIT
//
//  override def run : Unit = {
//    //    var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
//    //    val dialog = new DirectoryDialog(shell, SWT.OPEN)
//    //    val fileSelected = dialog.open
//    //    if (fileSelected != null)
//    //      worker("populateProjectManagerFromUri", "populateProjectManagerFromUri") { monitor : IProgressMonitor =>
//    //        Perspective.projectManager.populateProjectManagerFromUri(fileSelected)
//    //        Status.OK_STATUS
//    //      }
//    worker("addGitToProject", "addGitToProject") { monitor : IProgressMonitor =>
//      //val builder = new RepositoryBuilder
//      //println(ProjectManager.ProjectPathParentStore)
//      //val repository = builder.setGitDir(ProjectManager.ProjectPathParentStore.toFile)
//      //.setBare.readEnvironment.findGitDir.build
//      val initCommand = new InitCommand
//      initCommand.setDirectory(ProjectManager.ProjectPathParentStore.toFile)
//      initCommand.setBare(true)
//      initCommand.call
//
//      Status.OK_STATUS
//    }
//  }
//
//  override def dispose : Unit = {}
//
//}

class ProjectManagerAdd extends MenuItemNode {

  setId(WorkbenchCommandsIDs.PROJECT_ADD_ELEMENT)
  normalText = "&Add element"
  setText(normalText)

  actionCode = ActionCode.ADD

  override def run : Unit = {
    val projectManager = Perspective.projectManager
    val currentNode = projectManager.currentNode
    if (currentNode != null) {
      var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
      currentNode.nodeType match {
        case ProjectTypes.IMAGES => {
          val dialog = new WizardDialog(shell, new AddImageWizard)
          dialog.open
        }
        case ProjectTypes.PAGES => {
          val dialog = new WizardDialog(shell, new NewPageWizard)
          dialog.open
        }
        case ProjectTypes.SCRIPTS => {
          val dialog = new WizardDialog(shell, new AddResourceWizard(ProjectTypes.SCRIPT))
          dialog.open
        }
        case ProjectTypes.LOCATIONS => {
          val dialog = new WizardDialog(shell, new AddResourceWizard(ProjectTypes.LOCATION))
          dialog.open
        }
        case ProjectTypes.PERSONS => {
          val dialog = new WizardDialog(shell, new AddResourceWizard(ProjectTypes.PERSON))
          dialog.open
        }
        case ProjectTypes.SUMMARIES => {
          val dialog = new WizardDialog(shell, new AddResourceWizard(ProjectTypes.SUMMARY))
          dialog.open
        }
        case ProjectTypes.NOTES => {
          val dialog = new WizardDialog(shell, new AddResourceWizard(ProjectTypes.NOTE))
          dialog.open
        }
        case ProjectTypes.TAGS => {
          val dialog = new WizardDialog(shell, new AddResourceWizard(ProjectTypes.TAG))
          dialog.open
        }
        case ProjectTypes.COMMENTS => {
          val dialog = new WizardDialog(shell, new AddResourceWizard(ProjectTypes.COMMENT))
          dialog.open
        }
        case ProjectTypes.FILTERS => {
          val dialog = new WizardDialog(shell, new AddScriptletWizard(ExtensionType.IMAGE_MAGICK_MANIPULATOR))
          dialog.setTitle("Add Scriptlet Wizard")
          dialog.open
        }
        case _ => println(currentNode.nodeType)
      }
    }
  }

  override def dispose : Unit = {}

}

class ProjectManagerRename extends MenuItemNode {

  setId(WorkbenchCommandsIDs.PROJECT_RENAME_ELEMENT)
  normalText = "&Rename element"
  setText(normalText)

  actionCode = ActionCode.RENAME

  override def run : Unit = {
    val currentNode = Perspective.projectManager.currentNode
    if (ProjectNodeTools.renamableResource(currentNode)) {
      //if(Perspective.projectManager.renamableResource) {
      var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
      val inputDialog = new InputDialog(shell, "Rename", "Rename object", currentNode.name, null)
      inputDialog.open
      var text : String = inputDialog.getValue
      text = if (text != null) text.trim else ""
      if (!text.isEmpty) Perspective.projectManager.renameCurrentNode(text)
    }
  }

  override def dispose : Unit = {}

}

class ProjectManagerRemove extends MenuItemNode {

  setId(WorkbenchCommandsIDs.PROJECT_ADD_ELEMENT)
  normalText = "&Remove element"
  setText(normalText)

  actionCode = ActionCode.REMOVE

  override def run : Unit = {
    if (ProjectNodeTools.removableResource(Perspective.projectManager.currentNode)) {
      var shell = PlatformUI.getWorkbench.getActiveWorkbenchWindow.getShell
      //val result = MessageDialog.openConfirm(shell, "delete", "delete node")
      if (MessageDialog.openConfirm(shell, "delete", "delete node"))
        Perspective.projectManager.removeCurrentNode
    }
  }

  override def dispose : Unit = {}

}
