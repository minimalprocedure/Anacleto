/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.commands

//import org.eclipse.ui.PlatformUI
import org.pragmas.anacleto.ui.Perspective
//import org.pragmas.anacleto.ui.views.ProjectManager
//import org.pragmas.anacleto.search.ui.views.MacrosManagerView
//import org.eclipse.ui.IPageLayout

class NavigationFindMacros extends MenuItemNode {

  setId(WorkbenchCommandsIDs.NAVIGATION_FIND_MACROS)
  normalText = "&Find macros"
  setText(normalText)
  setIcon("silk/find_macros.png")

  actionCode = ActionCode.FIND

  override def run : Unit = {
    getWorkbenchWindow.getActivePage.showView(Perspective.MACROS_MANAGER_ID)
  }

  override def dispose : Unit = {}

}