/*******************************************************************************
 * Copyright (c) 2011 Massimo Maria Ghisalberti {http://pragmas.org}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
 ******************************************************************************/
package org.pragmas.anacleto.commands

import org.eclipse.ui.PlatformUI
import org.pragmas.anacleto.ui.Perspective
import org.pragmas.anacleto.ui.views.ProjectManager
import org.eclipse.ui.IPageLayout

class WindowShowViewProjectManager extends MenuItemNode {

  setId(WorkbenchCommandsIDs.WINDOW_SHOW_VIEW_PROJECTMANAGER)
  normalText = "&Project manager"
  setText(normalText)
  setIcon("silk/project-view.png")

  actionCode = ActionCode.SHOW

  override def run : Unit = {
    getWorkbenchWindow.getActivePage.showView(ProjectManager.ID)
  }

  override def dispose : Unit = {}

}

class WindowShowViewSearch extends MenuItemNode {

  setId(WorkbenchCommandsIDs.WINDOW_SHOW_VIEW_SEARCH)
  normalText = "&Search"
  setText(normalText)
  setIcon("silk/search.png")

  actionCode = ActionCode.SHOW

  override def run : Unit = {
    getWorkbenchWindow.getActivePage.showView(Perspective.ID_SEARCH_VIEW)
  }

  override def dispose : Unit = {}

}

class WindowShowViewProperties extends MenuItemNode {

  setId(WorkbenchCommandsIDs.WINDOW_SHOW_VIEW_PROPERTIES)
  normalText = "&Properties"
  setText(normalText)
  setIcon("silk/application_view_list.png")

  actionCode = ActionCode.SHOW

  override def run : Unit = {
    getWorkbenchWindow.getActivePage.showView(IPageLayout.ID_PROP_SHEET)
  }

  override def dispose : Unit = {}

}

class WindowShowViewOutline extends MenuItemNode {

  setId(WorkbenchCommandsIDs.WINDOW_SHOW_VIEW_OUTLINE)
  normalText = "&Outline"
  setText(normalText)
  setIcon("silk/outline.png")

  actionCode = ActionCode.SHOW

  override def run : Unit = {
    getWorkbenchWindow.getActivePage.showView(IPageLayout.ID_OUTLINE)
  }

  override def dispose : Unit = {}

}
